package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class TransitionLegs extends AbstractCompilerTester
{
    public TransitionLegs() throws IOException
    {
        super("TransitionLegs");
    }

    @Test
    public void test()
    {
        testAllNodes(0, 1);
    }
}
