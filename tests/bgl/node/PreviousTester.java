package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 06/05/2015.
 */
public class PreviousTester extends AbstractCompilerTester
{
    public PreviousTester() throws IOException
    {
        super("Previous");
    }

    @Test
    public void test()
    {
        testAllNodes(0, 1);
    }
}
