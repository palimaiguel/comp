package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class RouteTester extends AbstractCompilerTester
{
    public RouteTester() throws IOException
    {
        super("Route");
    }

    @Test
    public void test() throws IOException
    {
        testGrandChildrenNodes("Waypoint", 0, 1);
    }
}
