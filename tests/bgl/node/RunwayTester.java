package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class RunwayTester extends AbstractCompilerTester
{
    public RunwayTester() throws IOException
    {
        super("Runway");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(4, 1);
    }
}
