package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class TaxiwayPathTester extends AbstractCompilerTester
{
    public TaxiwayPathTester() throws IOException
    {
        super("TaxiwayPath");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(6, 1);
    }
}
