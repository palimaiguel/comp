package bgl.node;

import org.junit.Test;

import java.io.IOException;

public class StartTester extends AbstractCompilerTester
{
    public StartTester() throws IOException
    {
        super("Start");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(8, 1);
    }
}
