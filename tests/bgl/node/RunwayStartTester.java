package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class RunwayStartTester extends AbstractCompilerTester
{
    public RunwayStartTester() throws IOException
    {
        super("RunwayStart");
    }

    @Test
    public void test() throws IOException
    {
        testGrandChildrenNodes("Runway", 0, 1);
    }
}
