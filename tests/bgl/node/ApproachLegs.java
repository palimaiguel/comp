package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class ApproachLegs extends AbstractCompilerTester
{
    public ApproachLegs() throws IOException
    {
        super("ApproachLegs");
    }

    @Test
    public void test()
    {
        testGrandChildrenNodes("Approach", 0, 1);
    }
}
