package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class ComTester extends AbstractCompilerTester
{
    public ComTester() throws IOException
    {
        super("Com");
    }

    @Test
    public void test()
    {
        testAirportNodes(13, 1);
    }
}
