package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class TaxinameTester extends AbstractCompilerTester
{
    public TaxinameTester() throws IOException
    {
        super("TaxiName");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(42, 1);
    }
}
