package bgl.node;

import bgl.attribute.Attribute;
import compiler.Compiler;
import junit.framework.TestCase;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;
import java.util.function.BiConsumer;

public abstract class AbstractCompilerTester extends TestCase
{
    protected String name;

    public static final String FILENAME1 = "test_resources/input_template.txt";
    protected Compiler compiler1;
    protected AirportNode airportNode1;

    public static final String FILENAME2 = "test_resources/input_template2.txt";
    protected Compiler compiler2;
    protected AirportNode airportNode2;

    public AbstractCompilerTester(String name) throws IOException
    {
        this.name = name;

        this.compiler1 = new Compiler(FILENAME1, "lol.txt");
        this.airportNode1 = this.compiler1.getSemanticListener().getAirportList().get(0);

        this.compiler2 = new Compiler(FILENAME2, "lol.txt");
        this.airportNode2 = this.compiler2.getSemanticListener().getAirportList().get(0);
    }

    protected void testAirportNodes(int numberNodesFile1, int numberNodesFile2)
    {
        testAirportNodes(this.airportNode1, numberNodesFile1);
        testAirportNodes(this.airportNode2, numberNodesFile2);
    }

    private void testAirportNodes(AirportNode airportNode, int numberNodes)
    {
        List<Node> nodes = airportNode.getChildren(this.name);
        Assert.assertEquals(numberNodes, nodes.size());

        for (Node node : nodes)
        {
            // Check if mandatory attributes values are not null:
            node.getMandatoryAttributes().forEach(checkAttributeValueNotNull);
        }
    }

    protected void testGrandChildrenNodes(String parentName, int numberNodesFile1, int numberNodesFile2)
    {
        testGrandChildrenNodes(this.airportNode1, parentName, numberNodesFile1);
        testGrandChildrenNodes(this.airportNode2, parentName, numberNodesFile2);
    }

    private void testGrandChildrenNodes(AirportNode airportNode, String parentName, int numberNodes)
    {
        List<Node> nodes = airportNode.getChildren(parentName);

        int totalGrandChildren = 0;
        for (Node node : nodes)
        {
            List<Node> grandChildren = node.getChildren(this.name);
            totalGrandChildren += grandChildren.size();

            for (Node child : grandChildren)
            {
                child.getMandatoryAttributes().forEach(checkAttributeValueNotNull);
            }
        }
        Assert.assertEquals(numberNodes, totalGrandChildren);
    }

    protected void testAllNodes(int numberNodesFile1, int numberNodesFile2)
    {
        testAllNodes(this.airportNode1, numberNodesFile1);
        testAllNodes(this.airportNode2, numberNodesFile2);
    }

    private void testAllNodes(AirportNode airportNode, int numberNodes)
    {
        List<Node> children = airportNode.getChildren();

        int totalNodes = 0;
        for (Node child : children)
        {
            if(child.getName().equals(this.name))
            {
                child.getMandatoryAttributes().forEach(checkAttributeValueNotNull);
                totalNodes++;
            }

            List<Node> grandChildren = child.getChildren();
            for (Node grandChild : grandChildren)
            {
                if(grandChild.getName().equals(this.name))
                {
                    grandChild.getMandatoryAttributes().forEach(checkAttributeValueNotNull);
                    totalNodes++;
                }

                List<Node> grandGrandChildren = grandChild.getChildren();
                for (Node grandGrandChild : grandGrandChildren)
                {
                    if(grandGrandChild.getName().equals(this.name))
                    {
                        grandGrandChild.getMandatoryAttributes().forEach(checkAttributeValueNotNull);
                        totalNodes++;
                    }

                    List<Node> grandGrandGrandChildren = grandGrandChild.getChildren();
                    for (Node grandGrandGrandChild : grandGrandGrandChildren)
                    {
                        if(grandGrandGrandChild.getName().equals(this.name))
                        {
                            grandGrandGrandChild.getMandatoryAttributes().forEach(checkAttributeValueNotNull);
                            totalNodes++;
                        }
                    }
                }
            }
        }

        Assert.assertEquals(numberNodes, totalNodes);
    }

    private BiConsumer<String, Attribute> checkAttributeValueNotNull = (s, attribute) ->
            Assert.assertNotNull("Attribute not null: " + attribute.getName(), attribute.getValue());
}
