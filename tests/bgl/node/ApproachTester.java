package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class ApproachTester extends AbstractCompilerTester
{
    public ApproachTester() throws IOException
    {
        super("Approach");
    }

    @Test
    public void test()
    {
        testAirportNodes(0, 1);
    }
}
