package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class TransitionTester extends AbstractCompilerTester
{
    public TransitionTester() throws IOException
    {
        super("Transition");
    }

    @Test
    public void test() throws IOException
    {
        testGrandChildrenNodes("Approach", 0, 1);
    }
}
