package bgl.node;

import org.junit.Test;

import java.io.IOException;

public class HelipadTester extends AbstractCompilerTester
{
    public HelipadTester() throws IOException
    {
        super("Helipad");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(0, 1);
    }
}
