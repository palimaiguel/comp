package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class FuelTester extends AbstractCompilerTester
{
    public FuelTester() throws IOException
    {
        super("Fuel");
    }

    @Test
    public void test()
    {
        testGrandChildrenNodes("Services", 2, 1);
    }
}
