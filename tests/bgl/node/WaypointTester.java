package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class WaypointTester extends AbstractCompilerTester
{
    public WaypointTester() throws IOException
    {
        super("Waypoint");
    }

    @Test
    public void test() throws IOException
    {
        super.testAirportNodes(0, 1);
    }
}
