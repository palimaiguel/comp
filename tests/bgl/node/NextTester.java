package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class NextTester extends AbstractCompilerTester
{
    public NextTester() throws IOException
    {
        super("Next");
    }

    @Test
    public void test()
    {
        testAllNodes(0, 1);
    }
}
