package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class TowerTester extends AbstractCompilerTester
{
    public TowerTester() throws IOException
    {
        super("Tower");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(1, 1);
    }
}
