package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 07/05/2015.
 */
public class TaxiwayPointTester extends AbstractCompilerTester
{
    public TaxiwayPointTester() throws IOException
    {
        super("TaxiwayPoint");
    }

    @Test
    public void test() throws IOException
    {
        testAirportNodes(2, 1);
    }
}
