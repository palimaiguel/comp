package bgl.node;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by Jo�o on 06/05/2015.
 */
public class LegTester extends AbstractCompilerTester
{
    public LegTester() throws IOException
    {
        super("Leg");
    }

    @Test
    public void test()
    {
        testAllNodes(0, 3);
    }
}
