package compiler;

import org.junit.*;

public class RegexValidatorTest
{
    @Test
    public void latitude(){
        Assert.assertEquals(true, RegexValidator.latitude("N47 25.91"));
        Assert.assertEquals(true, RegexValidator.latitude("-80"));
        Assert.assertEquals(true, RegexValidator.latitude("50"));
        Assert.assertEquals(false, RegexValidator.latitude("-130"));
        Assert.assertEquals(false, RegexValidator.latitude("350"));
    }

    @Test
    public void longitude(){
        Assert.assertEquals(true, RegexValidator.longitude("W122 18.50"));
        Assert.assertEquals(true, RegexValidator.longitude("-180"));
        Assert.assertEquals(true, RegexValidator.longitude("-45"));
        Assert.assertEquals(true, RegexValidator.longitude("120"));
        Assert.assertEquals(true, RegexValidator.longitude("50"));
        Assert.assertEquals(false, RegexValidator.longitude("-200"));
        Assert.assertEquals(false, RegexValidator.longitude("350"));
    }

    @Test
    public void airportRegion()
    {
        Assert.assertEquals(true, RegexValidator.airportRegion("aa"));
        Assert.assertEquals(false, RegexValidator.airportRegion("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
    }

    @Test
    public void airportCountry()
    {
        Assert.assertEquals(true, RegexValidator.airportCountry("aa"));
        Assert.assertEquals(false, RegexValidator.airportCountry("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
    }

    @Test
    public void airportState()
    {
        Assert.assertEquals(true, RegexValidator.airportState("aa"));
        Assert.assertEquals(false, RegexValidator.airportState("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
    }

    @Test
    public void airportCity()
    {
        Assert.assertEquals(true, RegexValidator.airportCity("aa"));
        Assert.assertEquals(false, RegexValidator.airportCity("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
    }

    @Test
    public void airportName()
    {
        Assert.assertEquals(true, RegexValidator.airportName("aa"));
        Assert.assertEquals(false, RegexValidator.airportName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
    }

    @Test
    public void airportAltitude()
    {
        Assert.assertEquals(true, RegexValidator.airportAltitude("22.3M"));
        Assert.assertEquals(true, RegexValidator.airportAltitude("22.3F"));
        Assert.assertEquals(true, RegexValidator.airportAltitude("22.3"));
        Assert.assertEquals(false, RegexValidator.airportAltitude("22.3A"));
    }

    @Test
    public void airportMagvar()
    {
        Assert.assertEquals(true, RegexValidator.airportMagvar("-360.0"));
        Assert.assertEquals(true, RegexValidator.airportMagvar("360"));
        Assert.assertEquals(false, RegexValidator.airportMagvar("361"));
    }

    @Test
    public void airportIdent()
    {
        Assert.assertEquals(true, RegexValidator.airportIdent("AAAA"));
        Assert.assertEquals(false, RegexValidator.airportIdent("AAAAA"));
    }

    @Test
    public void airportAirportTestRadius()
    {
        Assert.assertEquals(true, RegexValidator.airportTestRadius("2.2F"));
        Assert.assertEquals(true, RegexValidator.airportTestRadius("2.2M"));
        Assert.assertEquals(true, RegexValidator.airportTestRadius("2.2N"));
        Assert.assertEquals(false, RegexValidator.airportTestRadius("pato"));
    }

    @Test
    public void airportAirportTrafficScalar()
    {
        Assert.assertEquals(true, RegexValidator.airportTrafficScalar("1.0"));
        Assert.assertEquals(true, RegexValidator.airportTrafficScalar("0.01"));
        Assert.assertEquals(false, RegexValidator.airportTrafficScalar("0.001"));
    }

    /********************
     *     TaxiwayPoint
     */
    @Test
    public void taxiwayPointType()
    {
        Assert.assertEquals(true, RegexValidator.taxiwayPointType("NORMAL"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointType("HOLD_SHORT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointType("ILS_HOLD_SHORT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointType("HOLD_SHORT_NO_DRAW"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointType("ILS_HOLD_SHORT_NO_DRAW"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointType("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointType("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointType("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointType("123.4"));
    }

    @Test
    public void taxiwayPointOrientation(){
        Assert.assertEquals(true, RegexValidator.taxiwayPointOrientation("FORWARD"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointOrientation("REVERSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointOrientation("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointOrientation("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointOrientation("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointOrientation("123.4"));
    }

    @Test
    public void taxiwayPointBiasX(){
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasX("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasX("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasX("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasX("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasX("-10"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasX("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointBiasX("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointBiasX("LOL"));
    }


    @Test
    public void taxiwayPointBiasZ(){
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasZ("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasZ("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasZ("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasZ("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasZ("-10"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointBiasZ("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointBiasZ("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointBiasZ("LOL"));
    }

    /********************
     *     TaxiwayParking
     */

    @Test
    public void taxiwayParkingIndex(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingIndex("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingIndex("255"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingIndex("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingIndex("3999"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingIndex("-1"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingIndex("200.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingIndex("4000"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingIndex("LOL"));
    }

    @Test
    public void taxiwayParkingBiasX(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasX("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasX("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasX("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasX("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasX("-10"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasX("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingBiasX("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingBiasX("LOL"));
    }

    @Test
    public void taxiwayParkingBiasZ(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasZ("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasZ("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasZ("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasZ("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasZ("-10"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingBiasZ("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingBiasZ("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingBiasZ("LOL"));
    }

    @Test
    public void taxiwayParkingHeading(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingHeading("360"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingHeading("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingHeading("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingHeading("0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingHeading("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingHeading("365.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingHeading("400"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingHeading("LOL"));
    }

    @Test
    public void taxiwayParkingRadius(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingRadius("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingRadius("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingRadius("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingRadius("0"));
        //Assert.assertEquals(true, RegexValidator.taxiwayParkingRadius("-10"));
        //Assert.assertEquals(true, RegexValidator.taxiwayParkingRadius("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingRadius("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingRadius("LOL"));
    }

    @Test
    public void taxiwayParkingType(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("NONE"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("DOCK_GA"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("FUEL"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("GATE_HEAVY"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("GATE_MEDIUM"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("GATE_SMALL"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_CARGO"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_GA"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_GA_LARGE"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_GA_MEDIUM"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_GA_SMALL"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_MIL_CARGO"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("RAMP_MIL_COMBAT"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingType("VEHICLE"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingType("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingType("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingType("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingType("123.4"));
    }

    @Test
    public void taxiwayParkingName(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("DOCK"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("GATE"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("GATE_A"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("GATE_B"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("GATE_D"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("GATE_Z"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingName("NONE"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("N_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("NE_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("NW_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("SE_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("S_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("SW_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("W_PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingName("E_PARKING"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingName("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingName("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingName("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingName("123.4"));
    }

    @Test
    public void taxiwayParkingNumber(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingNumber("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingNumber("255"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingNumber("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingNumber("3999"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingNumber("-1"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingNumber("200.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingNumber("4000"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingNumber("LOL"));
    }

    @Test
    public void taxiwayParkingAirlineCodes(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingAirlineCodes("UAL, AA, BA"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingAirlineCodes("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingAirlineCodes("ABCDE"));

    }

    @Test
    public void taxiwayParkingPushBack(){
        Assert.assertEquals(false, RegexValidator.taxiwayParkingPushBack("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingPushBack("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingPushBack("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingPushBack("123.4"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingPushBack("NONE"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingPushBack("BOTH"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingPushBack("LEFT"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingPushBack("RIGHT"));
    }

    @Test
    public void taxiwayParkingteeOffset1(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset1("0.1"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset1("50.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset1("50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset1("32.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("100.50"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("0.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("365.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("400"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset1("LOL"));
    }

    @Test
    public void taxiwayParkingteeOffset2() {
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset2("0.1"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset2("50.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset2("50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset2("32.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("100.50"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("0.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("365.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("400"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset2("LOL"));
    }

    @Test
    public void taxiwayParkingteeOffset3(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset3("0.1"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset3("50.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset3("50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset3("32.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("100.50"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("0.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("365.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("400"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset3("LOL"));
    }

    @Test
    public void taxiwayParkingteeOffset4(){
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset4("0.1"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset4("50.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset4("50"));
        Assert.assertEquals(true, RegexValidator.taxiwayParkingteeOffset4("32.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("100.50"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("0.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("0"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("365.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("400"));
        Assert.assertEquals(false, RegexValidator.taxiwayParkingteeOffset4("LOL"));
    }

    /********************
     *     Taxiname
     */

    @Test
    public void taxinameIndex(){
        Assert.assertEquals(true, RegexValidator.taxinameIndex("0"));
        Assert.assertEquals(true, RegexValidator.taxinameIndex("255"));
        Assert.assertEquals(true, RegexValidator.taxinameIndex("100"));
        Assert.assertEquals(true, RegexValidator.taxinameIndex("150"));
        Assert.assertEquals(false, RegexValidator.taxinameIndex("-1"));
        Assert.assertEquals(false, RegexValidator.taxinameIndex("200.5"));
        Assert.assertEquals(false, RegexValidator.taxinameIndex("4000"));
        Assert.assertEquals(false, RegexValidator.taxinameIndex("LOL"));
    }

    @Test
    public void taxinameName(){
        Assert.assertEquals(true, RegexValidator.taxinameName("K6"));
        Assert.assertEquals(true, RegexValidator.taxinameName("K9"));
        Assert.assertEquals(true, RegexValidator.taxinameName("A"));
        Assert.assertEquals(true, RegexValidator.taxinameName("LOL"));
        Assert.assertEquals(true, RegexValidator.taxinameName("1234"));
        Assert.assertEquals(true, RegexValidator.taxinameName("12.34"));
        Assert.assertEquals(false, RegexValidator.taxinameName("ABCDEFGIJK"));
        Assert.assertEquals(false, RegexValidator.taxinameName("12345678910222"));
    }

    /********************
     *     TaxiwayPath
     */

    @Test
    public void taxiwayPathType(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathType("RUNWAY"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathType("PARKING"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathType("TAXI"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathType("PATH"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathType("CLOSED"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathType("VEHICLE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathType("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathType("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathType("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathType("123.4"));
    }

    @Test
    public void taxiwayPathStart(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathStart("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathStart("255"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathStart("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathStart("3999"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathStart("-1"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathStart("200.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathStart("4000"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathStart("LOL"));
    }

    @Test
    public void taxiwayPathEnd(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathEnd("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathEnd("255"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathEnd("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathEnd("3999"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathEnd("-1"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathEnd("200.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathEnd("4000"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathEnd("LOL"));
    }

    @Test
    public void taxiwayPathWidth(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathWidth("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWidth("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWidth("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWidth("0"));
        //Assert.assertEquals(true, RegexValidator.taxiwayPathWidth("-10"));
        //Assert.assertEquals(true, RegexValidator.taxiwayPathWidth("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathWidth("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathWidth("LOL"));
    }

    @Test
    public void taxiwayPathWeightLimit(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathWeightLimit("1000"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWeightLimit("100.50"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWeightLimit("0.0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWeightLimit("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWeightLimit("-10"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathWeightLimit("-10.0"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathWeightLimit("abcd"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathWeightLimit("LOL"));
    }

    @Test
    public void taxiwayPathSurface(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("ASPHALT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("BITUMINOUS"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("BRICK"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("CLAY"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("CEMENT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("CONCRETE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("CORAL"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("DIRT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("GRASS"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("GRAVEL"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("ICE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("MACADAM"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("OIL_TREATED"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("PLANKS"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("SAND"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("SHALE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("SNOW"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("STEEL_MATS"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("TARMAC"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("UNKNOWN"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathSurface("WATER"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathSurface("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathSurface("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathSurface("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathSurface("123.4"));
    }

    @Test
    public void taxiwayPathDrawSurface(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathDrawSurface("TRUE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDrawSurface("FALSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawSurface("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawSurface("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawSurface("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawSurface("123.4"));
    }

    @Test
    public void taxiwayPathDrawDetail(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathDrawDetail("TRUE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDrawDetail("FALSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawDetail("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawDetail("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawDetail("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDrawDetail("123.4"));
    }

    @Test
    public void taxiwayPathCenterLine(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathCenterLine("TRUE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathCenterLine("FALSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLine("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLine("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLine("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLine("123.4"));
    }

    @Test
    public void taxiwayPathCenterLineLighted(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathCenterLineLighted("TRUE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathCenterLineLighted("FALSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLineLighted("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLineLighted("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLineLighted("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathCenterLineLighted("123.4"));
    }

    @Test
    public void taxiwayPathLeftEdge() {
        Assert.assertEquals(true, RegexValidator.taxiwayPathLeftEdge("NONE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathLeftEdge("SOLID"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathLeftEdge("DASHED"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathLeftEdge("SOLID_DASHED"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdge("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdge("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdge("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdge("123.4"));
    }

    @Test
    public void taxiwayPathLeftEdgeLighted() {
        Assert.assertEquals(true, RegexValidator.taxiwayPathLeftEdgeLighted("TRUE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathLeftEdgeLighted("FALSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdgeLighted("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdgeLighted("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdgeLighted("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathLeftEdgeLighted("123.4"));
    }

    @Test
    public void taxiwayPathRightEdge(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathRightEdge("NONE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathRightEdge("SOLID"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathRightEdge("DASHED"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathRightEdge("SOLID_DASHED"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdge("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdge("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdge("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdge("123.4"));
    }

    @Test
    public void taxiwayPathRightEdgeLighted(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathRightEdgeLighted("TRUE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathRightEdgeLighted("FALSE"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdgeLighted("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdgeLighted("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdgeLighted("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathRightEdgeLighted("123.4"));
    }

    @Test
    public void taxiwayPathNumber(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("00"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("05"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("09"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("25"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("36"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("EAST"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("NORTH"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("NORTHEAST"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("NORTHWEST"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("SOUTH"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("SOUTHEAST"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("SOUTHWEST"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathNumber("WEST"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathNumber("40"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathNumber("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathNumber("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathNumber("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathNumber("123.4"));
    }

    @Test
    public void taxiwayPathDesignator(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("NONE"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("C"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("CENTER"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("L"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("LEFT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("R"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("RIGHT"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("W"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("WATER"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("A"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathDesignator("B"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDesignator("LOL"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDesignator("lol"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDesignator("1234"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathDesignator("123.4"));
    }

    @Test
    public void taxiwayPathName(){
        Assert.assertEquals(true, RegexValidator.taxiwayPathName("0"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathName("255"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathName("100"));
        Assert.assertEquals(true, RegexValidator.taxiwayPathName("150"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathName("-1"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathName("200.5"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathName("4000"));
        Assert.assertEquals(false, RegexValidator.taxiwayPathName("LOL"));
    }


    /********************
     *     Tower
     */

    @Test
    public void towerAltitude(){
        Assert.assertEquals(true, RegexValidator.towerAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.towerAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.towerAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.towerAltitude("0"));
        Assert.assertEquals(false, RegexValidator.towerAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.towerAltitude("LOL"));
    }

    /********************
     *     Services
     */

    /********************
     *     Fuel
     */

    @Test
    public void fuelType() {
        Assert.assertEquals(true, RegexValidator.fuelType("73"));
        Assert.assertEquals(true, RegexValidator.fuelType("87"));
        Assert.assertEquals(true, RegexValidator.fuelType("100"));
        Assert.assertEquals(true, RegexValidator.fuelType("130"));
        Assert.assertEquals(true, RegexValidator.fuelType("145"));
        Assert.assertEquals(true, RegexValidator.fuelType("MOGAS"));
        Assert.assertEquals(true, RegexValidator.fuelType("JET"));
        Assert.assertEquals(true, RegexValidator.fuelType("JETA"));
        Assert.assertEquals(true, RegexValidator.fuelType("JETA1"));
        Assert.assertEquals(true, RegexValidator.fuelType("JETAP"));
        Assert.assertEquals(true, RegexValidator.fuelType("JETB"));
        Assert.assertEquals(true, RegexValidator.fuelType("JET4"));
        Assert.assertEquals(true, RegexValidator.fuelType("JET5"));
        Assert.assertEquals(true, RegexValidator.fuelType("UNKNOWN"));
        Assert.assertEquals(false, RegexValidator.fuelType("LOL"));
        Assert.assertEquals(false, RegexValidator.fuelType("lol"));
        Assert.assertEquals(false, RegexValidator.fuelType("1234"));
        Assert.assertEquals(false, RegexValidator.fuelType("123.4"));
    }

    @Test
    public void fuelAvailability(){
        Assert.assertEquals(true, RegexValidator.fuelAvailability("YES"));
        Assert.assertEquals(true, RegexValidator.fuelAvailability("NO"));
        Assert.assertEquals(true, RegexValidator.fuelAvailability("UNKNOWN"));
        Assert.assertEquals(true, RegexValidator.fuelAvailability("PRIOR_REQUEST"));
        Assert.assertEquals(false, RegexValidator.fuelAvailability("LOL"));
        Assert.assertEquals(false, RegexValidator.fuelAvailability("lol"));
        Assert.assertEquals(false, RegexValidator.fuelAvailability("1234"));
        Assert.assertEquals(false, RegexValidator.fuelAvailability("123.4"));
    }

    /********************
     *     Com
     */

    @Test
    public void comFrequency(){
        Assert.assertEquals(true, RegexValidator.comFrequency("108"));
        Assert.assertEquals(true, RegexValidator.comFrequency("108.0"));
        Assert.assertEquals(true, RegexValidator.comFrequency("136.992"));
        Assert.assertEquals(false, RegexValidator.comFrequency("100"));
        Assert.assertEquals(false, RegexValidator.comFrequency("-10"));
        Assert.assertEquals(false, RegexValidator.comFrequency("-10.0"));
        Assert.assertEquals(false, RegexValidator.comFrequency("90"));
        Assert.assertEquals(false, RegexValidator.comFrequency("150.0"));
        Assert.assertEquals(false, RegexValidator.comFrequency("abcd"));
        Assert.assertEquals(false, RegexValidator.comFrequency("LOL"));
    }

    @Test
    public void comType(){
        Assert.assertEquals(true, RegexValidator.comType("APPROACH"));
        Assert.assertEquals(true, RegexValidator.comType("ASOS"));
        Assert.assertEquals(true, RegexValidator.comType("ATIS"));
        Assert.assertEquals(true, RegexValidator.comType("AWOS"));
        Assert.assertEquals(true, RegexValidator.comType("CENTER"));
        Assert.assertEquals(true, RegexValidator.comType("CLEARANCE"));
        Assert.assertEquals(true, RegexValidator.comType("CLEARANCE_PRE_TAXI"));
        Assert.assertEquals(true, RegexValidator.comType("CTAF"));
        Assert.assertEquals(true, RegexValidator.comType("DEPARTURE"));
        Assert.assertEquals(true, RegexValidator.comType("FSS"));
        Assert.assertEquals(true, RegexValidator.comType("GROUND"));
        Assert.assertEquals(true, RegexValidator.comType("MULTICOM"));
        Assert.assertEquals(true, RegexValidator.comType("REMOTE_CLEARANCE_DELIVERY"));
        Assert.assertEquals(true, RegexValidator.comType("TOWER"));
        Assert.assertEquals(true, RegexValidator.comType("UNICOM"));

    }

    @Test
    public void comName() {
        Assert.assertEquals(true, RegexValidator.comName("LOL"));
        Assert.assertEquals(true, RegexValidator.comName("lol"));
        Assert.assertEquals(true, RegexValidator.comName("1234"));
        Assert.assertEquals(true, RegexValidator.comName("123.4"));
        Assert.assertEquals(false, RegexValidator.comName("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"));
    }

    /********************
     *     Runway
     */

    @Test
    public void runwayAltitude(){
        Assert.assertEquals(true, RegexValidator.runwayAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("100.50M"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("50M"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("50F"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("100.50F"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayAltitude("0"));
        Assert.assertEquals(false, RegexValidator.runwayAltitude("-10T"));
        Assert.assertEquals(false, RegexValidator.runwayAltitude("-10.0"));
        Assert.assertEquals(false, RegexValidator.runwayAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayAltitude("LOL"));
    }

    @Test
    public void runwaySurface(){
        Assert.assertEquals(true, RegexValidator.runwaySurface("ASPHALT"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("BITUMINOUS"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("BRICK"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("CLAY"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("CEMENT"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("CONCRETE"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("CORAL"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("DIRT"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("GRASS"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("GRAVEL"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("ICE"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("MACADAM"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("OIL_TREATED, PLANKS"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("SAND"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("SHALE"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("SNOW"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("STEEL_MATS"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("TARMAC"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("UNKNOWN"));
        Assert.assertEquals(true, RegexValidator.runwaySurface("WATER"));
        Assert.assertEquals(false, RegexValidator.runwaySurface("LOL"));
        Assert.assertEquals(false, RegexValidator.runwaySurface("lol"));
        Assert.assertEquals(false, RegexValidator.runwaySurface("1234"));
        Assert.assertEquals(false, RegexValidator.runwaySurface("123.4"));
    }

    @Test
    public void runwayHeading() {
        Assert.assertEquals(true, RegexValidator.runwayHeading("360"));
        Assert.assertEquals(true, RegexValidator.runwayHeading("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayHeading("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayHeading("0"));
        Assert.assertEquals(false, RegexValidator.runwayHeading("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayHeading("365.5"));
        Assert.assertEquals(false, RegexValidator.runwayHeading("400"));
        Assert.assertEquals(false, RegexValidator.runwayHeading("LOL"));
    }

    @Test
    public void runwayLength(){
        Assert.assertEquals(true, RegexValidator.runwayLength("1000"));
        Assert.assertEquals(true, RegexValidator.runwayLength("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayLength("100.50M"));
        Assert.assertEquals(true, RegexValidator.runwayLength("50M"));
        Assert.assertEquals(true, RegexValidator.runwayLength("50F"));
        Assert.assertEquals(true, RegexValidator.runwayLength("100.50F"));
        Assert.assertEquals(true, RegexValidator.runwayLength("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayLength("0"));
        Assert.assertEquals(false, RegexValidator.runwayLength("-10T"));
        Assert.assertEquals(false, RegexValidator.runwayLength("-10.0"));
        Assert.assertEquals(false, RegexValidator.runwayLength("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayLength("LOL"));
    }

    @Test
    public void runwayWidth(){
        Assert.assertEquals(true, RegexValidator.runwayWidth("1000"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("100.50M"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("50M"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("50F"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("100.50F"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayWidth("0"));
        Assert.assertEquals(false, RegexValidator.runwayWidth("-10T"));
        Assert.assertEquals(false, RegexValidator.runwayWidth("-10.0"));
        Assert.assertEquals(false, RegexValidator.runwayWidth("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayWidth("LOL"));
    }

    @Test
    public void runwayNumber(){
        Assert.assertEquals(true, RegexValidator.runwayNumber("00"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("05"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("09"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("0"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("25"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("36"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("EAST"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("NORTH"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("NORTHEAST"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("NORTHWEST"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("SOUTH"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("SOUTHEAST"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("SOUTHWEST"));
        Assert.assertEquals(true, RegexValidator.runwayNumber("WEST"));
        Assert.assertEquals(false, RegexValidator.runwayNumber("40"));
        Assert.assertEquals(false, RegexValidator.runwayNumber("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayNumber("lol"));
        Assert.assertEquals(false, RegexValidator.runwayNumber("1234"));
        Assert.assertEquals(false, RegexValidator.runwayNumber("123.4"));
    }

    @Test
    public void runwayDesignator(){
        Assert.assertEquals(true, RegexValidator.runwayDesignator("NONE"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("C"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("CENTER"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("L"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("LEFT"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("R"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("RIGHT"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("W"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("WATER"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("A"));
        Assert.assertEquals(true, RegexValidator.runwayDesignator("B"));
        Assert.assertEquals(false, RegexValidator.runwayDesignator("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayDesignator("lol"));
        Assert.assertEquals(false, RegexValidator.runwayDesignator("1234"));
        Assert.assertEquals(false, RegexValidator.runwayDesignator("123.4"));
    }

    @Test
    public void runwayPrimaryDesignator(){
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("NONE"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("C"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("CENTER"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("L"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("LEFT"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("R"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("RIGHT"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("W"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("WATER"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("A"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryDesignator("B"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryDesignator("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryDesignator("lol"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryDesignator("1234"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryDesignator("123.4"));
    }

    @Test
    public void runwaySecondaryDesignator(){
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("NONE"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("C"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("CENTER"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("L"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("LEFT"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("R"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("RIGHT"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("W"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("WATER"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("A"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryDesignator("B"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryDesignator("LOL"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryDesignator("lol"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryDesignator("1234"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryDesignator("123.4"));
    }

    @Test
    public void runwayPatternAltitude(){
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("100.50M"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("50M"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("50F"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("100.50F"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayPatternAltitude("0"));
        Assert.assertEquals(false, RegexValidator.runwayPatternAltitude("-10T"));
        Assert.assertEquals(false, RegexValidator.runwayPatternAltitude("-10.0"));
        Assert.assertEquals(false, RegexValidator.runwayPatternAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayPatternAltitude("LOL"));
    }

    @Test
    public void runwayPrimaryTakeoff(){
        Assert.assertEquals(true, RegexValidator.runwayPrimaryTakeoff("TRUE"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryTakeoff("FALSE"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryTakeoff("YES"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryTakeoff("NO"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryTakeoff("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryTakeoff("lol"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryTakeoff("1234"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryTakeoff("123.4"));
    }

    @Test
    public void runwayPrimaryLanding(){
        Assert.assertEquals(true, RegexValidator.runwayPrimaryLanding("TRUE"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryLanding("FALSE"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryLanding("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryLanding("lol"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryLanding("1234"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryLanding("123.4"));
    }

    @Test
    public void runwayPrimaryPattern(){
        Assert.assertEquals(true, RegexValidator.runwayPrimaryPattern("LEFT"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryPattern("RIGHT"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryPattern("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryPattern("lol"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryPattern("1234"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryPattern("123.4"));
    }

    @Test
    public void runwaySecondaryTakeoff(){
        Assert.assertEquals(true, RegexValidator.runwaySecondaryTakeoff("TRUE"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryTakeoff("FALSE"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryTakeoff("LOL"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryTakeoff("lol"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryTakeoff("1234"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryTakeoff("123.4"));
    }

    @Test
    public void runwaySecondaryLanding(){
        Assert.assertEquals(true, RegexValidator.runwaySecondaryLanding("TRUE"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryLanding("FALSE"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryLanding("LOL"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryLanding("lol"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryLanding("1234"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryLanding("123.4"));
    }

    @Test
    public void runwaySecondaryPattern(){
        Assert.assertEquals(true, RegexValidator.runwaySecondaryPattern("LEFT"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryPattern("RIGHT"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryPattern("LOL"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryPattern("lol"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryPattern("1234"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryPattern("123.4"));
    }

    @Test
    public void runwayPrimaryMarkingBias(){
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("1000"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("10N"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("5.5N"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("100.50M"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("50M"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("50F"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("100.50F"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayPrimaryMarkingBias("0"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryMarkingBias("-10"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryMarkingBias("-10.0"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryMarkingBias("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayPrimaryMarkingBias("LOL"));
    }

    @Test
    public void runwaySecondaryMarkingBias(){
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("1000"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("100.50"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("10N"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("5.5N"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("100.50M"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("50M"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("50F"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("100.50F"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("0.0"));
        Assert.assertEquals(true, RegexValidator.runwaySecondaryMarkingBias("0"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryMarkingBias("-10"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryMarkingBias("-10.0"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryMarkingBias("abcd"));
        Assert.assertEquals(false, RegexValidator.runwaySecondaryMarkingBias("LOL"));
    }

    /********************
     *     RunwayStart
     */

    @Test
    public void runwayStartType(){
        Assert.assertEquals(true, RegexValidator.runwayStartType("RUNWAY"));
        Assert.assertEquals(false, RegexValidator.runwayStartType("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayStartType("lol"));
        Assert.assertEquals(false, RegexValidator.runwayStartType("1234"));
        Assert.assertEquals(false, RegexValidator.runwayStartType("123.4"));
    }

    @Test
    public void runwayStartLatitude() {
        Assert.assertEquals(true, RegexValidator.runwayStartLatitude("N47 25.91"));
        Assert.assertEquals(true, RegexValidator.runwayStartLatitude("-80"));
        Assert.assertEquals(true, RegexValidator.runwayStartLatitude("50"));
        Assert.assertEquals(false, RegexValidator.runwayStartLatitude("-130"));
        Assert.assertEquals(false, RegexValidator.runwayStartLatitude("350"));
    }

    @Test
    public void runwayStartLongitude(){
        Assert.assertEquals(true, RegexValidator.runwayStartLongitude("W122 18.50"));
        Assert.assertEquals(true, RegexValidator.runwayStartLongitude("-180"));
        Assert.assertEquals(true, RegexValidator.runwayStartLongitude("-45"));
        Assert.assertEquals(true, RegexValidator.runwayStartLongitude("120" ));
        Assert.assertEquals(true, RegexValidator.runwayStartLongitude("50"));
        Assert.assertEquals(false, RegexValidator.runwayStartLongitude("-200"));
        Assert.assertEquals(false, RegexValidator.runwayStartLongitude("350"));
        Assert.assertEquals(false, RegexValidator.runwayStartLongitude("lol"));
    }

    @Test
    public void runwayStartAltitude(){
        Assert.assertEquals(true, RegexValidator.runwayStartAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.runwayStartAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayStartAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayStartAltitude("0"));
        Assert.assertEquals(false, RegexValidator.runwayStartAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayStartAltitude("LOL"));
    }

    @Test
    public void runwayStartHeading(){
        Assert.assertEquals(true, RegexValidator.runwayStartHeading("360"));
        Assert.assertEquals(true, RegexValidator.runwayStartHeading("100.50"));
        Assert.assertEquals(true, RegexValidator.runwayStartHeading("0.0"));
        Assert.assertEquals(true, RegexValidator.runwayStartHeading("0"));
        Assert.assertEquals(false, RegexValidator.runwayStartHeading("abcd"));
        Assert.assertEquals(false, RegexValidator.runwayStartHeading("365.5"));
        Assert.assertEquals(false, RegexValidator.runwayStartHeading("400"));
        Assert.assertEquals(false, RegexValidator.runwayStartHeading("LOL"));
    }

    @Test
    public void runwayStartEnd(){
        Assert.assertEquals(true, RegexValidator.runwayStartEnd("PRIMARY"));
        Assert.assertEquals(true, RegexValidator.runwayStartEnd("SECONDARY"));
        Assert.assertEquals(false, RegexValidator.runwayStartEnd("LOL"));
        Assert.assertEquals(false, RegexValidator.runwayStartEnd("lol"));
        Assert.assertEquals(false, RegexValidator.runwayStartEnd("1234"));
        Assert.assertEquals(false, RegexValidator.runwayStartEnd("123.4"));
    }

    @Test
    public void taxiwayPointIndex()
    {
        Assert.assertEquals(true, RegexValidator.taxiwayPointIndex("3999"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointIndex("coixa"));
        Assert.assertEquals(false, RegexValidator.taxiwayPointIndex("-1"));
        Assert.assertEquals(true, RegexValidator.taxiwayPointIndex("5"));
    }

    @Test
    public void waypointType()
    {
        Assert.assertEquals(true, RegexValidator.waypointType("NAMED"));
        Assert.assertEquals(true, RegexValidator.waypointType("UNNAMED"));
        Assert.assertEquals(true, RegexValidator.waypointType("VOR"));
        Assert.assertEquals(true, RegexValidator.waypointType("NDB"));
        Assert.assertEquals(true, RegexValidator.waypointType("OFF_ROUTE"));
        Assert.assertEquals(true, RegexValidator.waypointType("IAF"));
        Assert.assertEquals(true, RegexValidator.waypointType("FAF"));
        Assert.assertEquals(false, RegexValidator.waypointType("LOL"));
        Assert.assertEquals(false, RegexValidator.waypointType("lol"));
        Assert.assertEquals(false, RegexValidator.waypointType("1234"));
        Assert.assertEquals(false, RegexValidator.waypointType("123.4"));
    }

    @Test
    public void waypointMagvar()
    {
        Assert.assertEquals(true, RegexValidator.waypointMagvar("-300"));
        Assert.assertEquals(true, RegexValidator.waypointMagvar("-235.23"));
        Assert.assertEquals(true, RegexValidator.waypointMagvar("0"));
        Assert.assertEquals(true, RegexValidator.waypointMagvar("0.0"));
        Assert.assertEquals(true, RegexValidator.waypointMagvar("360.0"));
        Assert.assertEquals(true, RegexValidator.waypointMagvar("90"));
        Assert.assertEquals(false, RegexValidator.waypointMagvar("LOL"));
        Assert.assertEquals(false, RegexValidator.waypointMagvar("lol"));
        Assert.assertEquals(false, RegexValidator.waypointMagvar("1234"));
    }

    @Test
    public void waypointRegion()
    {
        Assert.assertEquals(true, RegexValidator.waypointRegion("K6"));
        Assert.assertEquals(true, RegexValidator.waypointRegion("K9"));
        Assert.assertEquals(false, RegexValidator.waypointRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.waypointRegion("1234"));
        Assert.assertEquals(false, RegexValidator.waypointRegion("12.34"));
    }

    @Test
    public void waypointIdent()
    {
        Assert.assertEquals(true, RegexValidator.waypointIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.waypointIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.waypointIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.waypointIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.waypointIdent("LOLOLOL"));
    }

    @Test
    public void routeType()
    {
        Assert.assertEquals(true, RegexValidator.routeType("VICTOR"));
        Assert.assertEquals(true, RegexValidator.routeType("JET"));
        Assert.assertEquals(true, RegexValidator.routeType("BOTH"));
        Assert.assertEquals(false, RegexValidator.routeType("LOL"));
        Assert.assertEquals(false, RegexValidator.routeType("123456"));
    }

    @Test
    public void routeName()
    {
        Assert.assertEquals(true, RegexValidator.routeName("name"));
        Assert.assertEquals(true, RegexValidator.routeName("SomeName"));
        Assert.assertEquals(false, RegexValidator.routeName("LOLOLOLOLOL"));
    }

    @Test
    public void previousType()
    {
        Assert.assertEquals(true, RegexValidator.previousType("NAMED"));
        Assert.assertEquals(true, RegexValidator.previousType("UNNAMED"));
        Assert.assertEquals(true, RegexValidator.previousType("VOR"));
        Assert.assertEquals(true, RegexValidator.previousType("NDB"));
        Assert.assertEquals(true, RegexValidator.previousType("OFF_ROUTE"));
        Assert.assertEquals(true, RegexValidator.previousType("IAF"));
        Assert.assertEquals(true, RegexValidator.previousType("FAF"));
        Assert.assertEquals(false, RegexValidator.previousType("LOL"));
        Assert.assertEquals(false, RegexValidator.previousType("lol"));
        Assert.assertEquals(false, RegexValidator.previousType("1234"));
        Assert.assertEquals(false, RegexValidator.previousType("123.4"));
    }

    @Test
    public void previousRegion()
    {
        Assert.assertEquals(true, RegexValidator.previousRegion("K6"));
        Assert.assertEquals(true, RegexValidator.previousRegion("K9"));
        Assert.assertEquals(false, RegexValidator.previousRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.previousRegion("1234"));
        Assert.assertEquals(false, RegexValidator.previousRegion("12.34"));
    }

    @Test
    public void previousIdent()
    {
        Assert.assertEquals(true, RegexValidator.previousIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.previousIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.previousIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.previousIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.previousIdent("LOLOLOL"));
    }

    @Test
    public void previousAltitudeMinimum()
    {
        Assert.assertEquals(true, RegexValidator.previousAltitudeMinimum("1000"));
        Assert.assertEquals(true, RegexValidator.previousAltitudeMinimum("100.50"));
        Assert.assertEquals(true, RegexValidator.previousAltitudeMinimum("0.0"));
        Assert.assertEquals(true, RegexValidator.previousAltitudeMinimum("0"));
        Assert.assertEquals(false, RegexValidator.previousAltitudeMinimum("abcd"));
        Assert.assertEquals(false, RegexValidator.previousAltitudeMinimum("LOL"));
    }

    @Test
    public void nextType()
    {
        Assert.assertEquals(true, RegexValidator.nextType("NAMED"));
        Assert.assertEquals(true, RegexValidator.nextType("UNNAMED"));
        Assert.assertEquals(true, RegexValidator.nextType("VOR"));
        Assert.assertEquals(true, RegexValidator.nextType("NDB"));
        Assert.assertEquals(true, RegexValidator.nextType("OFF_ROUTE"));
        Assert.assertEquals(true, RegexValidator.nextType("IAF"));
        Assert.assertEquals(true, RegexValidator.nextType("FAF"));
        Assert.assertEquals(false, RegexValidator.nextType("LOL"));
        Assert.assertEquals(false, RegexValidator.nextType("lol"));
        Assert.assertEquals(false, RegexValidator.nextType("1234"));
        Assert.assertEquals(false, RegexValidator.nextType("123.4"));
    }

    @Test
    public void nextRegion()
    {
        Assert.assertEquals(true, RegexValidator.nextRegion("K6"));
        Assert.assertEquals(true, RegexValidator.nextRegion("K9"));
        Assert.assertEquals(false, RegexValidator.nextRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.nextRegion("1234"));
        Assert.assertEquals(false, RegexValidator.nextRegion("12.34"));
    }

    @Test
    public void nextIdent()
    {
        Assert.assertEquals(true, RegexValidator.nextIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.nextIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.nextIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.nextIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.nextIdent("LOLOLOL"));
    }

    @Test
    public void nextAltitudeMinimum()
    {
        Assert.assertEquals(true, RegexValidator.nextAltitudeMinimum("1000"));
        Assert.assertEquals(true, RegexValidator.nextAltitudeMinimum("100.50"));
        Assert.assertEquals(true, RegexValidator.nextAltitudeMinimum("0.0"));
        Assert.assertEquals(true, RegexValidator.nextAltitudeMinimum("0"));
        Assert.assertEquals(false, RegexValidator.nextAltitudeMinimum("abcd"));
        Assert.assertEquals(false, RegexValidator.nextAltitudeMinimum("LOL"));
    }

    @Test
    public void approachType()
    {
        Assert.assertEquals(true, RegexValidator.approachType("GPS"));
        Assert.assertEquals(true, RegexValidator.approachType("ILS"));
        Assert.assertEquals(true, RegexValidator.approachType("LDA"));
        Assert.assertEquals(true, RegexValidator.approachType("LOCALIZER"));
        Assert.assertEquals(true, RegexValidator.approachType("LOCALIZER_BACKCOURSE"));
        Assert.assertEquals(true, RegexValidator.approachType("NDB"));
        Assert.assertEquals(true, RegexValidator.approachType("NDBDME"));
        Assert.assertEquals(true, RegexValidator.approachType("RNAV"));
        Assert.assertEquals(true, RegexValidator.approachType("SDF"));
        Assert.assertEquals(true, RegexValidator.approachType("VOR"));
        Assert.assertEquals(true, RegexValidator.approachType("VORDME"));
        Assert.assertEquals(false, RegexValidator.approachType("LOL"));
        Assert.assertEquals(false, RegexValidator.approachType("lol"));
        Assert.assertEquals(false, RegexValidator.approachType("1234"));
        Assert.assertEquals(false, RegexValidator.approachType("123.4"));
    }

    @Test
    public void approachRunway()
    {
        Assert.assertEquals(true, RegexValidator.approachRunway("00"));
        Assert.assertEquals(true, RegexValidator.approachRunway("05"));
        Assert.assertEquals(true, RegexValidator.approachRunway("09"));
        Assert.assertEquals(true, RegexValidator.approachRunway("0"));
        Assert.assertEquals(true, RegexValidator.approachRunway("25"));
        Assert.assertEquals(true, RegexValidator.approachRunway("36"));
        Assert.assertEquals(true, RegexValidator.approachRunway("EAST"));
        Assert.assertEquals(true, RegexValidator.approachRunway("NORTH"));
        Assert.assertEquals(true, RegexValidator.approachRunway("NORTHEAST"));
        Assert.assertEquals(true, RegexValidator.approachRunway("NORTHWEST"));
        Assert.assertEquals(true, RegexValidator.approachRunway("SOUTH"));
        Assert.assertEquals(true, RegexValidator.approachRunway("SOUTHEAST"));
        Assert.assertEquals(true, RegexValidator.approachRunway("SOUTHWEST"));
        Assert.assertEquals(true, RegexValidator.approachRunway("WEST"));
        Assert.assertEquals(false, RegexValidator.approachRunway("40"));
        Assert.assertEquals(false, RegexValidator.approachRunway("LOL"));
        Assert.assertEquals(false, RegexValidator.approachRunway("lol"));
        Assert.assertEquals(false, RegexValidator.approachRunway("1234"));
        Assert.assertEquals(false, RegexValidator.approachRunway("123.4"));
    }

    @Test
    public void approachDesignator()
    {
        Assert.assertEquals(true, RegexValidator.approachDesignator("NONE"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("C"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("CENTER"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("L"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("LEFT"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("R"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("RIGHT"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("W"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("WATER"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("A"));
        Assert.assertEquals(true, RegexValidator.approachDesignator("B"));
        Assert.assertEquals(false, RegexValidator.approachDesignator("LOL"));
        Assert.assertEquals(false, RegexValidator.approachDesignator("lol"));
        Assert.assertEquals(false, RegexValidator.approachDesignator("1234"));
        Assert.assertEquals(false, RegexValidator.approachDesignator("123.4"));
    }

    @Test
    public void approachSuffix()
    {
        Assert.assertEquals(true, RegexValidator.approachSuffix("A"));
        Assert.assertEquals(true, RegexValidator.approachSuffix("4"));
        Assert.assertEquals(true, RegexValidator.approachSuffix("B"));
        Assert.assertEquals(false, RegexValidator.approachSuffix("LOL"));
        Assert.assertEquals(false, RegexValidator.approachSuffix("lol"));
        Assert.assertEquals(false, RegexValidator.approachSuffix("1234"));
        Assert.assertEquals(false, RegexValidator.approachSuffix("123.4"));
    }

    @Test
    public void approachGPSOverlay()
    {
        Assert.assertEquals(true, RegexValidator.approachGPSOverlay("TRUE"));
        Assert.assertEquals(true, RegexValidator.approachGPSOverlay("FALSE"));
        Assert.assertEquals(false, RegexValidator.approachGPSOverlay("LOL"));
        Assert.assertEquals(false, RegexValidator.approachGPSOverlay("lol"));
        Assert.assertEquals(false, RegexValidator.approachGPSOverlay("1234"));
        Assert.assertEquals(false, RegexValidator.approachGPSOverlay("123.4"));
    }

    @Test
    public void approachFixType()
    {
        Assert.assertEquals(true, RegexValidator.approachFixType("NDB"));
        Assert.assertEquals(true, RegexValidator.approachFixType("TERMINAL_NDB"));
        Assert.assertEquals(true, RegexValidator.approachFixType("TERMINAL_WAYPOINT"));
        Assert.assertEquals(true, RegexValidator.approachFixType("VOR"));
        Assert.assertEquals(true, RegexValidator.approachFixType("WAYPOINT"));
        Assert.assertEquals(false, RegexValidator.approachFixType("LOL"));
        Assert.assertEquals(false, RegexValidator.approachFixType("lol"));
        Assert.assertEquals(false, RegexValidator.approachFixType("1234"));
        Assert.assertEquals(false, RegexValidator.approachFixType("123.4"));
    }

    @Test
    public void approachFixRegion()
    {
        Assert.assertEquals(true, RegexValidator.approachFixRegion("K6"));
        Assert.assertEquals(true, RegexValidator.approachFixRegion("K9"));
        Assert.assertEquals(false, RegexValidator.approachFixRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.approachFixRegion("1234"));
        Assert.assertEquals(false, RegexValidator.approachFixRegion("12.34"));
    }

    @Test
    public void approachFixIdent()
    {
        Assert.assertEquals(true, RegexValidator.approachFixIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.approachFixIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.approachFixIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.approachFixIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.approachFixIdent("LOLOLOL"));
    }

    @Test
    public void approachAltitude()
    {
        Assert.assertEquals(true, RegexValidator.approachAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.approachAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.approachAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.approachAltitude("0"));
        Assert.assertEquals(false, RegexValidator.approachAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.approachAltitude("LOL"));
    }

    @Test
    public void approachHeading()
    {
        Assert.assertEquals(true, RegexValidator.approachHeading("360"));
        Assert.assertEquals(true, RegexValidator.approachHeading("100.50"));
        Assert.assertEquals(true, RegexValidator.approachHeading("0.0"));
        Assert.assertEquals(true, RegexValidator.approachHeading("0"));
        Assert.assertEquals(false, RegexValidator.approachHeading("abcd"));
        Assert.assertEquals(false, RegexValidator.approachHeading("365.5"));
        Assert.assertEquals(false, RegexValidator.approachHeading("400"));
        Assert.assertEquals(false, RegexValidator.approachHeading("LOL"));
    }

    @Test
    public void approachMissedAltitude()
    {
        Assert.assertEquals(true, RegexValidator.approachMissedAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.approachMissedAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.approachMissedAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.approachMissedAltitude("0"));
        Assert.assertEquals(false, RegexValidator.approachMissedAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.approachMissedAltitude("LOL"));
    }

    @Test
    public void legType()
    {
        Assert.assertEquals(true, RegexValidator.legType("AF"));
        Assert.assertEquals(true, RegexValidator.legType("CA"));
        Assert.assertEquals(true, RegexValidator.legType("CD"));
        Assert.assertEquals(true, RegexValidator.legType("CF"));
        Assert.assertEquals(true, RegexValidator.legType("CI"));
        Assert.assertEquals(true, RegexValidator.legType("CR"));
        Assert.assertEquals(true, RegexValidator.legType("DF"));
        Assert.assertEquals(true, RegexValidator.legType("FA"));
        Assert.assertEquals(true, RegexValidator.legType("FC"));
        Assert.assertEquals(true, RegexValidator.legType("FD"));
        Assert.assertEquals(true, RegexValidator.legType("FM"));
        Assert.assertEquals(true, RegexValidator.legType("HA"));
        Assert.assertEquals(true, RegexValidator.legType("HF"));
        Assert.assertEquals(true, RegexValidator.legType("HM"));
        Assert.assertEquals(true, RegexValidator.legType("IF"));
        Assert.assertEquals(true, RegexValidator.legType("PI"));
        Assert.assertEquals(true, RegexValidator.legType("RF"));
        Assert.assertEquals(true, RegexValidator.legType("TF"));
        Assert.assertEquals(true, RegexValidator.legType("VA"));
        Assert.assertEquals(true, RegexValidator.legType("VD"));
        Assert.assertEquals(true, RegexValidator.legType("VI"));
        Assert.assertEquals(true, RegexValidator.legType("VM"));
        Assert.assertEquals(true, RegexValidator.legType("VR"));
        Assert.assertEquals(false, RegexValidator.legType("LOL"));
        Assert.assertEquals(false, RegexValidator.legType("lol"));
        Assert.assertEquals(false, RegexValidator.legType("1234"));
        Assert.assertEquals(false, RegexValidator.legType("123.4"));
    }

    @Test
    public void legFixType()
    {
        Assert.assertEquals(true, RegexValidator.legFixType("NDB"));
        Assert.assertEquals(true, RegexValidator.legFixType("TERMINAL_NDB"));
        Assert.assertEquals(true, RegexValidator.legFixType("TERMINAL_WAYPOINT"));
        Assert.assertEquals(true, RegexValidator.legFixType("VOR"));
        Assert.assertEquals(true, RegexValidator.legFixType("WAYPOINT"));
        Assert.assertEquals(false, RegexValidator.legFixType("LOL"));
        Assert.assertEquals(false, RegexValidator.legFixType("lol"));
        Assert.assertEquals(false, RegexValidator.legFixType("1234"));
        Assert.assertEquals(false, RegexValidator.legFixType("123.4"));
    }

    @Test
    public void legFixRegion()
    {
        Assert.assertEquals(true, RegexValidator.legFixRegion("K6"));
        Assert.assertEquals(true, RegexValidator.legFixRegion("K9"));
        Assert.assertEquals(false, RegexValidator.legFixRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.legFixRegion("1234"));
        Assert.assertEquals(false, RegexValidator.legFixRegion("12.34"));
    }

    @Test
    public void legFixIdent()
    {
        Assert.assertEquals(true, RegexValidator.legFixIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.legFixIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.legFixIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.legFixIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.legFixIdent("LOLOLOL"));
    }

    @Test
    public void legFlyOver()
    {
        Assert.assertEquals(true, RegexValidator.legFlyOver("TRUE"));
        Assert.assertEquals(true, RegexValidator.legFlyOver("FALSE"));
        Assert.assertEquals(false, RegexValidator.legFlyOver("LOL"));
        Assert.assertEquals(false, RegexValidator.legFlyOver("lol"));
        Assert.assertEquals(false, RegexValidator.legFlyOver("1234"));
        Assert.assertEquals(false, RegexValidator.legFlyOver("123.4"));
    }

    @Test
    public void legTurnDirection()
    {
        Assert.assertEquals(true, RegexValidator.legTurnDirection("L"));
        Assert.assertEquals(true, RegexValidator.legTurnDirection("R"));
        Assert.assertEquals(true, RegexValidator.legTurnDirection("E"));
        Assert.assertEquals(false, RegexValidator.legTurnDirection("LOL"));
        Assert.assertEquals(false, RegexValidator.legTurnDirection("lol"));
        Assert.assertEquals(false, RegexValidator.legTurnDirection("1234"));
        Assert.assertEquals(false, RegexValidator.legTurnDirection("123.4"));
    }

    @Test
    public void legRecommendedType()
    {
        Assert.assertEquals(true, RegexValidator.legRecommendedType("NDB"));
        Assert.assertEquals(true, RegexValidator.legRecommendedType("TERMINAL_NDB"));
        Assert.assertEquals(true, RegexValidator.legRecommendedType("TERMINAL_WAYPOINT"));
        Assert.assertEquals(true, RegexValidator.legRecommendedType("VOR"));
        Assert.assertEquals(true, RegexValidator.legRecommendedType("WAYPOINT"));
        Assert.assertEquals(true, RegexValidator.legRecommendedType("RUNWAY"));
        Assert.assertEquals(true, RegexValidator.legRecommendedType("LOCALIZER"));
        Assert.assertEquals(false, RegexValidator.legRecommendedType("LOL"));
        Assert.assertEquals(false, RegexValidator.legRecommendedType("lol"));
        Assert.assertEquals(false, RegexValidator.legRecommendedType("1234"));
        Assert.assertEquals(false, RegexValidator.legRecommendedType("123.4"));
    }

    @Test
    public void legRecommendedRegion()
    {
        Assert.assertEquals(true, RegexValidator.legRecommendedRegion("K6"));
        Assert.assertEquals(true, RegexValidator.legRecommendedRegion("K9"));
        Assert.assertEquals(false, RegexValidator.legRecommendedRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.legRecommendedRegion("1234"));
        Assert.assertEquals(false, RegexValidator.legRecommendedRegion("12.34"));
    }

    @Test
    public void legRecommendedIdent()
    {
        Assert.assertEquals(true, RegexValidator.legRecommendedIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.legRecommendedIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.legRecommendedIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.legRecommendedIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.legRecommendedIdent("LOLOLOL"));
    }

    @Test
    public void legTheta()
    {
        Assert.assertEquals(true, RegexValidator.legTheta("360"));
        Assert.assertEquals(true, RegexValidator.legTheta("100.50"));
        Assert.assertEquals(true, RegexValidator.legTheta("0.0"));
        Assert.assertEquals(true, RegexValidator.legTheta("0"));
        Assert.assertEquals(false, RegexValidator.legTheta("abcd"));
        Assert.assertEquals(false, RegexValidator.legTheta("365.5"));
        Assert.assertEquals(false, RegexValidator.legTheta("400"));
        Assert.assertEquals(false, RegexValidator.legTheta("LOL"));
    }

    @Test
    public void legRho()
    {
        Assert.assertEquals(true, RegexValidator.legRho("1000"));
        Assert.assertEquals(true, RegexValidator.legRho("100.50"));
        Assert.assertEquals(true, RegexValidator.legRho("10N"));
        Assert.assertEquals(true, RegexValidator.legRho("5.5N"));
        Assert.assertEquals(true, RegexValidator.legRho("0.0"));
        Assert.assertEquals(true, RegexValidator.legRho("0"));
        Assert.assertEquals(false, RegexValidator.legRho("-10"));
        Assert.assertEquals(false, RegexValidator.legRho("-10.0"));
        Assert.assertEquals(false, RegexValidator.legRho("abcd"));
        Assert.assertEquals(false, RegexValidator.legRho("LOL"));
    }

    @Test
    public void legTrueCourse()
    {
        Assert.assertEquals(true, RegexValidator.legTrueCourse("360"));
        Assert.assertEquals(true, RegexValidator.legTrueCourse("100.50"));
        Assert.assertEquals(true, RegexValidator.legTrueCourse("0.0"));
        Assert.assertEquals(true, RegexValidator.legTrueCourse("0"));
        Assert.assertEquals(false, RegexValidator.legTrueCourse("abcd"));
        Assert.assertEquals(false, RegexValidator.legTrueCourse("365.5"));
        Assert.assertEquals(false, RegexValidator.legTrueCourse("400"));
        Assert.assertEquals(false, RegexValidator.legTrueCourse("LOL"));
    }

    @Test
    public void legMagneticCourse()
    {
        Assert.assertEquals(true, RegexValidator.legMagneticCourse("360"));
        Assert.assertEquals(true, RegexValidator.legMagneticCourse("100.50"));
        Assert.assertEquals(true, RegexValidator.legMagneticCourse("0.0"));
        Assert.assertEquals(true, RegexValidator.legMagneticCourse("0"));
        Assert.assertEquals(false, RegexValidator.legMagneticCourse("abcd"));
        Assert.assertEquals(false, RegexValidator.legMagneticCourse("365.5"));
        Assert.assertEquals(false, RegexValidator.legMagneticCourse("400"));
        Assert.assertEquals(false, RegexValidator.legMagneticCourse("LOL"));
    }

    @Test
    public void legDistance()
    {
        Assert.assertEquals(true, RegexValidator.legDistance("1000"));
        Assert.assertEquals(true, RegexValidator.legDistance("100.50"));
        Assert.assertEquals(true, RegexValidator.legDistance("10N"));
        Assert.assertEquals(true, RegexValidator.legDistance("5.5N"));
        Assert.assertEquals(true, RegexValidator.legDistance("0.0"));
        Assert.assertEquals(true, RegexValidator.legDistance("0"));
        Assert.assertEquals(false, RegexValidator.legDistance("-10"));
        Assert.assertEquals(false, RegexValidator.legDistance("-10.0"));
        Assert.assertEquals(false, RegexValidator.legDistance("abcd"));
        Assert.assertEquals(false, RegexValidator.legDistance("LOL"));
    }

    @Test
    public void legTime()
    {
        Assert.assertEquals(true, RegexValidator.legTime("1000"));
        Assert.assertEquals(true, RegexValidator.legTime("100.50"));
        Assert.assertEquals(true, RegexValidator.legTime("0.0"));
        Assert.assertEquals(true, RegexValidator.legTime("0"));
        Assert.assertEquals(false, RegexValidator.legTime("-10"));
        Assert.assertEquals(false, RegexValidator.legTime("-10.0"));
        Assert.assertEquals(false, RegexValidator.legTime("abcd"));
        Assert.assertEquals(false, RegexValidator.legTime("LOL"));
    }

    @Test
    public void legAltitudeDescriptor()
    {
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("+"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("-"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor(" "));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("A"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("B"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("C"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("G"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("H"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("I"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("J"));
        Assert.assertEquals(true, RegexValidator.legAltitudeDescriptor("V"));
        Assert.assertEquals(false, RegexValidator.legAltitudeDescriptor("LOL"));
        Assert.assertEquals(false, RegexValidator.legAltitudeDescriptor("lol"));
        Assert.assertEquals(false, RegexValidator.legAltitudeDescriptor("1234"));
        Assert.assertEquals(false, RegexValidator.legAltitudeDescriptor("123.4"));
    }

    @Test
    public void legAltitude1()
    {
        Assert.assertEquals(true, RegexValidator.legAltitude1("1000"));
        Assert.assertEquals(true, RegexValidator.legAltitude1("100.50"));
        Assert.assertEquals(true, RegexValidator.legAltitude1("0.0"));
        Assert.assertEquals(true, RegexValidator.legAltitude1("0"));
        Assert.assertEquals(false, RegexValidator.legAltitude1("-10"));
        Assert.assertEquals(false, RegexValidator.legAltitude1("-10.0"));
        Assert.assertEquals(false, RegexValidator.legAltitude1("abcd"));
        Assert.assertEquals(false, RegexValidator.legAltitude1("LOL"));
    }

    @Test
    public void legAltitude2()
    {
        Assert.assertEquals(true, RegexValidator.legAltitude2("1000"));
        Assert.assertEquals(true, RegexValidator.legAltitude2("100.50"));
        Assert.assertEquals(true, RegexValidator.legAltitude2("0.0"));
        Assert.assertEquals(true, RegexValidator.legAltitude2("0"));
        Assert.assertEquals(false, RegexValidator.legAltitude2("-10"));
        Assert.assertEquals(false, RegexValidator.legAltitude2("-10.0"));
        Assert.assertEquals(false, RegexValidator.legAltitude2("abcd"));
        Assert.assertEquals(false, RegexValidator.legAltitude2("LOL"));
    }

    @Test
    public void transitionType()
    {
        Assert.assertEquals(true, RegexValidator.transitionType("FULL"));
        Assert.assertEquals(true, RegexValidator.transitionType("DME"));
        Assert.assertEquals(false, RegexValidator.transitionType("LOL"));
        Assert.assertEquals(false, RegexValidator.transitionType("lol"));
        Assert.assertEquals(false, RegexValidator.transitionType("1234"));
        Assert.assertEquals(false, RegexValidator.transitionType("123.4"));
    }

    @Test
    public void transitionFixType()
    {
        Assert.assertEquals(true, RegexValidator.transitionFixType("NDB"));
        Assert.assertEquals(true, RegexValidator.transitionFixType("TERMINAL_NDB"));
        Assert.assertEquals(true, RegexValidator.transitionFixType("TERMINAL_WAYPOINT"));
        Assert.assertEquals(true, RegexValidator.transitionFixType("VOR"));
        Assert.assertEquals(true, RegexValidator.transitionFixType("WAYPOINT"));
        Assert.assertEquals(false, RegexValidator.transitionFixType("LOL"));
        Assert.assertEquals(false, RegexValidator.transitionFixType("lol"));
        Assert.assertEquals(false, RegexValidator.transitionFixType("1234"));
        Assert.assertEquals(false, RegexValidator.transitionFixType("123.4"));
    }

    @Test
    public void transitionFixRegion()
    {
        Assert.assertEquals(true, RegexValidator.transitionFixRegion("K6"));
        Assert.assertEquals(true, RegexValidator.transitionFixRegion("K9"));
        Assert.assertEquals(false, RegexValidator.transitionFixRegion("LOL"));
        Assert.assertEquals(false, RegexValidator.transitionFixRegion("1234"));
        Assert.assertEquals(false, RegexValidator.transitionFixRegion("12.34"));
    }

    @Test
    public void transitionFixIdent()
    {
        Assert.assertEquals(true, RegexValidator.transitionFixIdent("IDENT"));
        Assert.assertEquals(true, RegexValidator.transitionFixIdent("ORD"));
        Assert.assertEquals(true, RegexValidator.transitionFixIdent("ABDY"));
        Assert.assertEquals(false, RegexValidator.transitionFixIdent("1234567"));
        Assert.assertEquals(false, RegexValidator.transitionFixIdent("LOLOLOL"));
    }

    @Test
    public void transitionAltitude()
    {
        Assert.assertEquals(true, RegexValidator.transitionAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.transitionAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.transitionAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.transitionAltitude("0"));
        Assert.assertEquals(false, RegexValidator.transitionAltitude("-10"));
        Assert.assertEquals(false, RegexValidator.transitionAltitude("-10.0"));
        Assert.assertEquals(false, RegexValidator.transitionAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.transitionAltitude("LOL"));
    }

    @Test
    public void helipadAltitude()
    {
        Assert.assertEquals(true, RegexValidator.helipadAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("100.50M"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("50M"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("50F"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("100.50F"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.helipadAltitude("0"));
        Assert.assertEquals(false, RegexValidator.helipadAltitude("-10T"));
        Assert.assertEquals(false, RegexValidator.helipadAltitude("-10.0"));
        Assert.assertEquals(false, RegexValidator.helipadAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.helipadAltitude("LOL"));
    }


    @Test
    public void helipadSurface()
    {
        Assert.assertEquals(true, RegexValidator.helipadSurface("ASPHALT"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("BITUMINOUS"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("BRICK"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("CLAY"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("CEMENT"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("CONCRETE"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("CORAL"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("DIRT"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("GRASS"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("GRAVEL"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("ICE"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("MACADAM"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("OIL_TREATED, PLANKS"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("SAND"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("SHALE"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("SNOW"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("STEEL_MATS"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("TARMAC"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("UNKNOWN"));
        Assert.assertEquals(true, RegexValidator.helipadSurface("WATER"));
    }

    @Test
    public void helipadHeading()
    {
        Assert.assertEquals(true, RegexValidator.helipadHeading("360"));
        Assert.assertEquals(true, RegexValidator.helipadHeading("100.50"));
        Assert.assertEquals(true, RegexValidator.helipadHeading("0.0"));
        Assert.assertEquals(true, RegexValidator.helipadHeading("0"));
        Assert.assertEquals(false, RegexValidator.helipadHeading("abcd"));
        Assert.assertEquals(false, RegexValidator.helipadHeading("365.5"));
        Assert.assertEquals(false, RegexValidator.helipadHeading("400"));
        Assert.assertEquals(false, RegexValidator.helipadHeading("LOL"));
    }

    @Test
    public void helipadLength()
    {
        Assert.assertEquals(true, RegexValidator.helipadLength("1000"));
        Assert.assertEquals(true, RegexValidator.helipadLength("100.50"));
        Assert.assertEquals(true, RegexValidator.helipadLength("100.50M"));
        Assert.assertEquals(true, RegexValidator.helipadLength("50M"));
        Assert.assertEquals(true, RegexValidator.helipadLength("50F"));
        Assert.assertEquals(true, RegexValidator.helipadLength("100.50F"));
        Assert.assertEquals(true, RegexValidator.helipadLength("0.0"));
        Assert.assertEquals(true, RegexValidator.helipadLength("0"));
        Assert.assertEquals(false, RegexValidator.helipadLength("-10T"));
        Assert.assertEquals(false, RegexValidator.helipadLength("-10.0"));
        Assert.assertEquals(false, RegexValidator.helipadLength("abcd"));
        Assert.assertEquals(false, RegexValidator.helipadLength("LOL"));
    }

    @Test
    public void helipadWidth()
    {
        Assert.assertEquals(true, RegexValidator.helipadWidth("1000"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("100.50"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("100.50M"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("50M"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("50F"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("100.50F"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("0.0"));
        Assert.assertEquals(true, RegexValidator.helipadWidth("0"));
        Assert.assertEquals(false, RegexValidator.helipadWidth("-10T"));
        Assert.assertEquals(false, RegexValidator.helipadWidth("-10.0"));
        Assert.assertEquals(false, RegexValidator.helipadWidth("abcd"));
        Assert.assertEquals(false, RegexValidator.helipadWidth("LOL"));
    }

    @Test
    public void helipadType()
    {
        Assert.assertEquals(true, RegexValidator.helipadType("NONE"));
        Assert.assertEquals(true, RegexValidator.helipadType("CIRCLE"));
        Assert.assertEquals(true, RegexValidator.helipadType("H"));
        Assert.assertEquals(true, RegexValidator.helipadType("MEDICAL"));
        Assert.assertEquals(true, RegexValidator.helipadType("SQUARE"));
        Assert.assertEquals(false, RegexValidator.helipadType("LOL"));
        Assert.assertEquals(false, RegexValidator.helipadType("lol"));
        Assert.assertEquals(false, RegexValidator.helipadType("1234"));
        Assert.assertEquals(false, RegexValidator.helipadType("123.4"));
    }

    @Test
    public void helipadClosed()
    {
        Assert.assertEquals(true, RegexValidator.helipadClosed("TRUE"));
        Assert.assertEquals(true, RegexValidator.helipadClosed("FALSE"));
        Assert.assertEquals(false, RegexValidator.helipadClosed("LOL"));
        Assert.assertEquals(false, RegexValidator.helipadClosed("lol"));
        Assert.assertEquals(false, RegexValidator.helipadClosed("1234"));
        Assert.assertEquals(false, RegexValidator.helipadClosed("123.4"));
    }

    @Test
    public void helipadTransparent()
    {
        Assert.assertEquals(true, RegexValidator.helipadTransparent("TRUE"));
        Assert.assertEquals(true, RegexValidator.helipadTransparent("FALSE"));
        Assert.assertEquals(false, RegexValidator.helipadTransparent("LOL"));
        Assert.assertEquals(false, RegexValidator.helipadTransparent("lol"));
        Assert.assertEquals(false, RegexValidator.helipadTransparent("1234"));
        Assert.assertEquals(false, RegexValidator.helipadTransparent("123.4"));
    }

    @Test
    public void helipadRed()
    {
        Assert.assertEquals(true, RegexValidator.helipadRed("0"));
        Assert.assertEquals(true, RegexValidator.helipadRed("255"));
        Assert.assertEquals(true, RegexValidator.helipadRed("100"));
        Assert.assertEquals(true, RegexValidator.helipadRed("150"));
        Assert.assertEquals(false, RegexValidator.helipadRed("-1"));
        Assert.assertEquals(false, RegexValidator.helipadRed("200.5"));
        Assert.assertEquals(false, RegexValidator.helipadRed("4000"));
        Assert.assertEquals(false, RegexValidator.helipadRed("LOL"));
    }

    @Test
    public void helipadGreen()
    {
        Assert.assertEquals(true, RegexValidator.helipadGreen("0"));
        Assert.assertEquals(true, RegexValidator.helipadGreen("255"));
        Assert.assertEquals(true, RegexValidator.helipadGreen("100"));
        Assert.assertEquals(true, RegexValidator.helipadGreen("150"));
        Assert.assertEquals(false, RegexValidator.helipadGreen("-1"));
        Assert.assertEquals(false, RegexValidator.helipadGreen("200.5"));
        Assert.assertEquals(false, RegexValidator.helipadGreen("4000"));
        Assert.assertEquals(false, RegexValidator.helipadGreen("LOL"));
    }

    @Test
    public void helipadBlue()
    {
        Assert.assertEquals(true, RegexValidator.helipadBlue("0"));
        Assert.assertEquals(true, RegexValidator.helipadBlue("255"));
        Assert.assertEquals(true, RegexValidator.helipadBlue("100"));
        Assert.assertEquals(true, RegexValidator.helipadBlue("150"));
        Assert.assertEquals(false, RegexValidator.helipadBlue("-1"));
        Assert.assertEquals(false, RegexValidator.helipadBlue("200.5"));
        Assert.assertEquals(false, RegexValidator.helipadBlue("4000"));
        Assert.assertEquals(false, RegexValidator.helipadBlue("LOL"));
    }

    @Test
    public void startType()
    {
        Assert.assertEquals(true, RegexValidator.startType("RUNWAY"));
        Assert.assertEquals(false, RegexValidator.startType("LOL"));
        Assert.assertEquals(false, RegexValidator.startType("lol"));
        Assert.assertEquals(false, RegexValidator.startType("1234"));
        Assert.assertEquals(false, RegexValidator.startType("123.4"));
    }

    @Test
    public void startLatitude()
    {
        Assert.assertEquals(true, RegexValidator.startLatitude("N47 25.91"));
        Assert.assertEquals(true, RegexValidator.startLatitude("-80"));
        Assert.assertEquals(true, RegexValidator.startLatitude("50"));
        Assert.assertEquals(false, RegexValidator.startLatitude("-130"));
        Assert.assertEquals(false, RegexValidator.startLatitude("350"));
        Assert.assertEquals(false, RegexValidator.startLatitude("lol"));
    }

    @Test
    public void startLongitude()
    {
        Assert.assertEquals(true, RegexValidator.startLongitude("W47 25.91"));
        Assert.assertEquals(true, RegexValidator.startLongitude("-80"));
        Assert.assertEquals(true, RegexValidator.startLongitude("50"));
        Assert.assertEquals(true, RegexValidator.startLongitude("-130"));
        Assert.assertEquals(false, RegexValidator.startLongitude("350"));
        Assert.assertEquals(false, RegexValidator.startLongitude("lol"));
    }

    @Test
    public void startAltitude()
    {
        Assert.assertEquals(true, RegexValidator.startAltitude("1000"));
        Assert.assertEquals(true, RegexValidator.startAltitude("100.50"));
        Assert.assertEquals(true, RegexValidator.startAltitude("100.50M"));
        Assert.assertEquals(true, RegexValidator.startAltitude("50M"));
        Assert.assertEquals(true, RegexValidator.startAltitude("50F"));
        Assert.assertEquals(true, RegexValidator.startAltitude("100.50F"));
        Assert.assertEquals(true, RegexValidator.startAltitude("0.0"));
        Assert.assertEquals(true, RegexValidator.startAltitude("0"));
        Assert.assertEquals(false, RegexValidator.startAltitude("-10T"));
        Assert.assertEquals(false, RegexValidator.startAltitude("-10.0"));
        Assert.assertEquals(false, RegexValidator.startAltitude("abcd"));
        Assert.assertEquals(false, RegexValidator.startAltitude("LOL"));
    }

    @Test
    public void startHeading()
    {
        Assert.assertEquals(true, RegexValidator.startHeading("360"));
        Assert.assertEquals(true, RegexValidator.startHeading("100.50"));
        Assert.assertEquals(true, RegexValidator.startHeading("0.0"));
        Assert.assertEquals(true, RegexValidator.startHeading("0"));
        Assert.assertEquals(false, RegexValidator.startHeading("abcd"));
        Assert.assertEquals(false, RegexValidator.startHeading("365.5"));
        Assert.assertEquals(false, RegexValidator.startHeading("400"));
        Assert.assertEquals(false, RegexValidator.startHeading("LOL"));
    }

    @Test
    public void startNumber()
    {
        Assert.assertEquals(true, RegexValidator.startNumber("00"));
        Assert.assertEquals(true, RegexValidator.startNumber("05"));
        Assert.assertEquals(true, RegexValidator.startNumber("09"));
        Assert.assertEquals(true, RegexValidator.startNumber("0"));
        Assert.assertEquals(true, RegexValidator.startNumber("25"));
        Assert.assertEquals(true, RegexValidator.startNumber("36"));
        Assert.assertEquals(true, RegexValidator.startNumber("EAST"));
        Assert.assertEquals(true, RegexValidator.startNumber("NORTH"));
        Assert.assertEquals(true, RegexValidator.startNumber("NORTHEAST"));
        Assert.assertEquals(true, RegexValidator.startNumber("NORTHWEST"));
        Assert.assertEquals(true, RegexValidator.startNumber("SOUTH"));
        Assert.assertEquals(true, RegexValidator.startNumber("SOUTHEAST"));
        Assert.assertEquals(true, RegexValidator.startNumber("SOUTHWEST"));
        Assert.assertEquals(true, RegexValidator.startNumber("WEST"));
        Assert.assertEquals(false, RegexValidator.startNumber("40"));
        Assert.assertEquals(false, RegexValidator.startNumber("LOL"));
        Assert.assertEquals(false, RegexValidator.startNumber("lol"));
        Assert.assertEquals(false, RegexValidator.startNumber("1234"));
        Assert.assertEquals(false, RegexValidator.startNumber("123.4"));

    }

    @Test
    public void startDesignator()
    {
        Assert.assertEquals(true, RegexValidator.startDesignator("NONE"));
        Assert.assertEquals(true, RegexValidator.startDesignator("C"));
        Assert.assertEquals(true, RegexValidator.startDesignator("CENTER"));
        Assert.assertEquals(true, RegexValidator.startDesignator("L"));
        Assert.assertEquals(true, RegexValidator.startDesignator("LEFT"));
        Assert.assertEquals(true, RegexValidator.startDesignator("R"));
        Assert.assertEquals(true, RegexValidator.startDesignator("RIGHT"));
        Assert.assertEquals(true, RegexValidator.startDesignator("W"));
        Assert.assertEquals(true, RegexValidator.startDesignator("WATER"));
        Assert.assertEquals(true, RegexValidator.startDesignator("A"));
        Assert.assertEquals(true, RegexValidator.startDesignator("B"));
        Assert.assertEquals(false, RegexValidator.startDesignator("LOL"));
        Assert.assertEquals(false, RegexValidator.startDesignator("lol"));
        Assert.assertEquals(false, RegexValidator.startDesignator("1234"));
        Assert.assertEquals(false, RegexValidator.startDesignator("123.4"));
    }
}
