lexer grammar Tags;

TAG_FACILITY_DATA: 'FSData';
TAG_AIRPORT: 'Airport';

// ** Taxi tags **:
TAG_TAXIWAY_POINT: 'TaxiwayPoint';
TAG_TAXIWAY_PARKING: 'TaxiwayParking';
TAG_TAXINAME: 'TaxiName';
TAG_TAXIWAY_PATH: 'TaxiwayPath';

// ** Other tags **:
TAG_TOWER: 'Tower';

TAG_SERVICES: 'Services';
    TAG_FUEL: 'Fuel';

TAG_COM: 'Com';

TAG_RUNWAY: 'Runway';
	TAG_MARKINGS: 'Markings';
	TAG_LIGHTS: 'Lights';
	TAG_OFFSET_THRESHOLD: 'OffsetThreshold';
	TAG_BLAST_PAD: 'BlastPad';
	TAG_OVERRUN: 'Overrun';
	TAG_APPROACH_LIGHTS: 'ApproachLights';
	TAG_VASI: 'Vasi';
	TAG_ILS: 'Ils';
		TAG_GLIDE_SLOPE: 'GlideSlope';
		TAG_DME: 'Dme';
		TAG_VISUAL_MODEL: 'VisualModel';
			TAG_BIAS_XYZ: 'BiasXYZ';
	TAG_RUNWAY_START: 'RunwayStart';

TAG_RUNWAY_ALIAS: 'RunwayAlias';

TAG_APRONS: 'Aprons';
    TAG_APRON: 'Apron';
        TAG_VERTEX: 'Vertex';

TAG_APRON_EDGE_LIGHTS: 'ApronEdgeLights';
    TAG_EDGE_LIGHTS: 'EdgeLights';

TAG_TAXIWAY_SIGN: 'TaxiwaySign';

TAG_WAYPOINT: 'Waypoint';
    TAG_ROUTE: 'Route';
		TAG_PREVIOUS: 'Previous';
		TAG_NEXT: 'Next';

TAG_APPROACH: 'Approach';
	TAG_APPROACH_LEGS: 'ApproachLegs';
		TAG_LEG: 'Leg';
	TAG_MISSED_APROACH_LEGS: 'MissedApproachLegs';
	TAG_TRANSITION: 'Transition';
		TAG_DME_ARC: 'DmeArc';
		TAG_TRANSITION_LEGS: 'TransitionLegs';

TAG_NDB: 'Ndb';
TAG_HELIPAD: 'Helipad';
TAG_START: 'Start';

TAG_JETWAY: 'Jetway';
	TAG_SCENERY_OBJECT: 'SceneryObject';
		TAG_BEACON: 'Beacon';
		TAG_EFFECT: 'Effect';
		TAG_GENERIC_BUILDING: 'GenericBuilding';
			TAG_RECTANGULAR_BUILDING: 'RectangularBuilding';
			TAG_PYRAMIDAL_BUILDING: 'PyramidalBuilding';
			TAG_MULTI_SIDED_BUILDING: 'MultiSidedBuilding';
		TAG_LIBRARY_OBJECT: 'LibraryObject';
		TAG_TRIGGER: 'Trigger';
			TAG_TRIGGER_WEATHER_DATA: 'TriggerWeatherData';
		TAG_WINDSOCK: 'Windsock';
			TAG_POLE_COLOR: 'PoleColor';
			TAG_SOCK_COLOR: 'SockColor';
		TAG_ATTACHED_OBJECT: 'AttachedObject';
			TAG_RANDOM_ATTACH: 'RandomAttach';
		TAG_NO_AUTOGEN_SUPPRESSION: 'NoAutogenSuppression';
		TAG_NO_CRASH: 'NoCrash';
		TAG_NO_FOG: 'NoFog';
		TAG_NO_SHADOW: 'NoShadow';
		TAG_NO_Z_WRITE: 'NoZWrite';
		TAG_NO_NO_Z_TEST: 'NoZTest';

TAG_BLAST_FENCE: 'BlastFence';
TAG_BOUNDARY_FENCE: 'BoundaryFence';
TAG_DELETE_AIRPORT: 'DeleteAirport';