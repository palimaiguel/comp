package compiler;

import bgl.node.AirportNode;
import org.antlr.v4.runtime.ParserRuleContext;

import java.util.HashMap;

/**
 * Created by Miguel on 05-05-2015.
 */
public class SemanticAnalyzer
{
    public void analyzeSemantics(SemanticListener listener)
    {
        for (AirportNode airport: listener.getAirportList())
        {
            // Check taxi name references:
            checkIdentifiers(airport.getTaxiNameReferencedIdentifiers(), airport.getTaxiNameIdentifiers(), "name", "TaxiName");

            // Check taxiway point references:
            checkIdentifiers(airport.getTaxiwayPointReferencedIdentifiers(), airport.getTaxiwayPointIdentifiers(), "start/end", "TaxiwayPoint");

            // Check taxiway parking references:
            checkIdentifiers(airport.getTaxiwayParkingReferencedIdentifiers(), airport.getTaxiwayParkingIdentifiers(), "end", "TaxiwayParking");
        }
    }

    private void checkIdentifiers(HashMap<Integer, ParserRuleContext> referencedIdentifiersTable, HashMap<Integer, ParserRuleContext> identifiersTable, String attributeName, String referencedIndexNode)
    {
        referencedIdentifiersTable.forEach(
                (referencedIdentifier, ctx) ->
                {
                    if(!identifiersTable.containsKey(referencedIdentifier))
                        ErrorPrinter.printInvalidReferenceToIdentifier(ctx, attributeName, referencedIdentifier, referencedIndexNode);
                }
        );
    }
}
