package compiler;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * Created by Jo�o on 08/05/2015.
 */
public class ErrorPrinter
{
    public enum Type
    {
        NONE,
        IDENTIFIED_REPEATED
    }

    public static boolean s_error = false;

    public static boolean getError()
    {
        return s_error;
    }

    public static void printNullMandatoryAttribute(@NotNull ParserRuleContext ctx, String attributeName)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getChild(1).getText();

        printErrorMessage(line, nodeName, "Missing mandatory attribute <" + attributeName + ">");
    }

    public static void printAttributeValueNotValid(@NotNull ParserRuleContext ctx, String attributeName, String attributeValue)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getParent().getChild(1).getText();

        printErrorMessage(line, nodeName, "Attribute <" + attributeName + "> with value <" + attributeValue + "> is not valid");
    }

    public static void printAttributeRepeated(@NotNull ParserRuleContext ctx, String attributeName, String attributeValue)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getParent().getChild(1).getText();

        printErrorMessage(line, nodeName, "Attribute <" + attributeName + "> is already defined. Redefining with value <" + attributeValue + ">");
    }

    public static void printIdentifierRepeated(@NotNull ParserRuleContext ctx, String nodeName, Integer index)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();

        printErrorMessage(line, nodeName, "Index <" + index + "> already exists");
    }

    public static void printInvalidReferenceToIdentifier(@NotNull ParserRuleContext ctx, String attributeName, Integer index, String referencedIndexNode)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getChild(1).getText();

        StringBuilder builder = new StringBuilder(256);
        builder.append("Attribute <");
        builder.append(attributeName);
        builder.append("> references invalid index <");
        builder.append(index);
        builder.append("> of node <");
        builder.append(referencedIndexNode);
        builder.append(">");

        printErrorMessage(line, nodeName, builder.toString());
    }

    public static void printInvalidTaxiwayLocation(@NotNull ParserRuleContext ctx)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getChild(1).getText();

        printErrorMessage(line, nodeName, "There must be only one way to specify the position (using biasX & biasZ or lat & lon)");
    }

    public static void printTaxiwayPathError(@NotNull ParserRuleContext ctx, boolean runwayType, String attributeName, boolean mustBePresent)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getChild(1).getText();

        StringBuilder builder = new StringBuilder(512);
        builder.append("Since type is ");
        if(runwayType)
            builder.append("<RUNWAY>");
        else
            builder.append("not <RUNWAY>");

        builder.append(" then attribute <");
        builder.append(attributeName);
        builder.append("> ");

        if(mustBePresent)
            builder.append("is mandatory");
        else
            builder.append("must not be present");

        printErrorMessage(line, nodeName, builder.toString());
    }

    // Error message for opposite designator:
    public static void printRunwayDesignatorError(@NotNull ParserRuleContext ctx, String message)
    {
        s_error = true;

        Token start = ctx.getStart();
        int line = start.getLine();
        String nodeName = ctx.getChild(1).getText();

        printErrorMessage(line, nodeName, message);
    }

    private static void printErrorMessage(int line, String nodeName, String message)
    {
        s_error = true;

        System.err.print("Line ");
        System.err.print(line);
        System.err.print(": Node ");
        System.err.print(nodeName);
        System.err.print(":: ");
        System.err.println(message);
    }
}
