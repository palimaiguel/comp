package compiler;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.BitSet;

public class LexicalSyntacticErrorListener implements ANTLRErrorListener
{
    private boolean m_error = false;

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingToken, int line, int charPosition, String s, RecognitionException e)
    {
        m_error = true;

        //String message =
        //        "Syntax m_error in line " + line + " at position " + charPosition + " - Token is: " +  offendingToken.toString();

        //System.out.println(message);

        if(offendingToken != null)
        {
            CommonToken token = (CommonToken) offendingToken;

            String message =
                    "~ Simplified message by Palimaiguel: Line " + line + ":" + charPosition + " unexpected token \"" + token.getText() + "\"";

            System.err.println(message);
        }
    }

    @Override
    public void reportAmbiguity(Parser parser, DFA dfa, int i, int i1, boolean b, BitSet bitSet, ATNConfigSet atnConfigSet)
    {
    }

    @Override
    public void reportAttemptingFullContext(Parser parser, DFA dfa, int i, int i1, BitSet bitSet, ATNConfigSet atnConfigSet)
    {
    }

    @Override
    public void reportContextSensitivity(Parser parser, DFA dfa, int i, int i1, int i2, ATNConfigSet atnConfigSet)
    {
    }

    public boolean getError()
    {
        return m_error;
    }
}
