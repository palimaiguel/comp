package compiler;

import bgl.attribute.Attribute;
import bgl.node.*;
import gen.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * Created by Miguel on 20-04-2015.
 */
public class SemanticListener implements AirportListener
{
    private Stack<Node> nodes = new Stack<>();
    private List<AirportNode> airports = new ArrayList<>();

    private void addTaxiwayPointIdentifier(@NotNull ParserRuleContext ctx, Node node)
    {
        addIdentifier(ctx, node, "index", true, airports.get(airports.size() - 1).getTaxiwayPointIdentifiers(), ErrorPrinter.Type.IDENTIFIED_REPEATED);
    }

    private void addTaxiwayParkingIdentifier(@NotNull ParserRuleContext ctx, Node node)
    {
        addIdentifier(ctx, node, "index", true, airports.get(airports.size() - 1).getTaxiwayParkingIdentifiers(), ErrorPrinter.Type.IDENTIFIED_REPEATED);
    }

    private void addTaxiNameIdentifier(@NotNull ParserRuleContext ctx, Node node)
    {
        addIdentifier(ctx, node, "index", true, airports.get(airports.size() - 1).getTaxiNameIdentifiers(), ErrorPrinter.Type.IDENTIFIED_REPEATED);
    }

    private void addTaxiwayPathReferencedIdentifiers(@NotNull ParserRuleContext ctx, Node node)
    {
        // If the taxiwayparking type is "PARKING", then end references a parking ID, if not references a point ID
        addIdentifier(ctx, node, "start", true, airports.get(airports.size() - 1).getTaxiwayPointReferencedIdentifiers(), ErrorPrinter.Type.NONE);

        if (nodes.peek().getMandatoryAttributes().get("type").getValue().equals("PARKING"))
            addIdentifier(ctx, node, "end", true, airports.get(airports.size() - 1).getTaxiwayParkingReferencedIdentifiers(), ErrorPrinter.Type.NONE);
        else
            addIdentifier(ctx, node, "end", true, airports.get(airports.size() - 1).getTaxiwayPointReferencedIdentifiers(), ErrorPrinter.Type.NONE);

        addIdentifier(ctx, node, "name", false, airports.get(airports.size() - 1).getTaxiNameReferencedIdentifiers(), ErrorPrinter.Type.NONE);
    }

    private void addIdentifier(@NotNull ParserRuleContext ctx, Node node, String attributeName, boolean mandatoryAttribute, HashMap<Integer, ParserRuleContext> table, ErrorPrinter.Type errorType)
    {
        Attribute indexAttribute;
        if(mandatoryAttribute)
            indexAttribute = node.getMandatoryAttributes().get(attributeName);
        else
            indexAttribute = node.getOptionalAttributes().get(attributeName);

        if(indexAttribute != null)
        {
            String indexAttributeValue = indexAttribute.getValue();
            if(indexAttributeValue != null)
            {
                Integer index = Integer.parseInt(indexAttributeValue);
                if(!table.containsKey(index))
                    table.put(index, ctx);
                else if(errorType == ErrorPrinter.Type.IDENTIFIED_REPEATED)
                    ErrorPrinter.printIdentifierRepeated(ctx, node.getName(), index);
            }
        }
    }

    private void enterNode(@NotNull ParserRuleContext ctx, Node node)
    {
        // Add node to stack:
        this.nodes.push(node);
    }

    private void exitNode(@NotNull ParserRuleContext ctx)
    {
        // Remove start from stack:
        Node node = this.nodes.pop();

        // Add node to parent node:
        if(this.nodes.size() != 0)
        {
            Node parent = this.nodes.peek();
            parent.addChild(node);
        }

        node.validateAttributes(ctx);
    }

    private void enterAttribute(@NotNull ParserRuleContext ctx)
    {
        Node node = this.nodes.peek();

        // Adding attribute to node:
        node.addAttribute(ctx);
    }

    private void exitAttribute(@NotNull ParserRuleContext ctx)
    {
    }

    public List<AirportNode> getAirportList()
    {
        return this.airports;
    }


    // ------------------- S -------------------

    @Override
    public void enterS(@NotNull AirportParser.SContext ctx)
    {
    }

    @Override
    public void exitS(@NotNull AirportParser.SContext ctx)
    {

    }

    @Override
    public void enterFacilityData(@NotNull AirportParser.FacilityDataContext ctx)
    {

    }

    @Override
    public void exitFacilityData(@NotNull AirportParser.FacilityDataContext ctx)
    {

    }

    @Override
    public void enterFacilityData_attr(@NotNull AirportParser.FacilityData_attrContext ctx)
    {

    }

    @Override
    public void exitFacilityData_attr(@NotNull AirportParser.FacilityData_attrContext ctx)
    {

    }

    @Override
    public void enterFacilityData_descendents(@NotNull AirportParser.FacilityData_descendentsContext ctx)
    {

    }

    @Override
    public void exitFacilityData_descendents(@NotNull AirportParser.FacilityData_descendentsContext ctx)
    {

    }


    // ------------------- AIRPORT -------------------

    @Override
    public void enterAirport(@NotNull AirportParser.AirportContext ctx)
    {
        AirportNode newAirport = new AirportNode();
        this.airports.add(newAirport);
        enterNode(ctx, newAirport);
    }

    @Override
    public void exitAirport(@NotNull AirportParser.AirportContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterAirport_attr(@NotNull AirportParser.Airport_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitAirport_attr(@NotNull AirportParser.Airport_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterAirport_descendents(@NotNull AirportParser.Airport_descendentsContext ctx)
    {
    }

    @Override
    public void exitAirport_descendents(@NotNull AirportParser.Airport_descendentsContext ctx)
    {
    }

    @Override
    public void enterAirport_taxi_info(@NotNull AirportParser.Airport_taxi_infoContext ctx)
    {
    }

    @Override
    public void exitAirport_taxi_info(@NotNull AirportParser.Airport_taxi_infoContext ctx)
    {
    }


    // ------------------- TAXIWAY POINT -------------------

    @Override
    public void enterTaxiway_point(@NotNull AirportParser.Taxiway_pointContext ctx)
    {
        enterNode(ctx, new TaxiwaypointNode());
    }

    @Override
    public void exitTaxiway_point(@NotNull AirportParser.Taxiway_pointContext ctx)
    {
        addTaxiwayPointIdentifier(ctx, this.nodes.peek());

        exitNode(ctx);
    }

    @Override
    public void enterTaxiway_point_attr(@NotNull AirportParser.Taxiway_point_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitTaxiway_point_attr(@NotNull AirportParser.Taxiway_point_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- TAXIWAY PARKING -------------------

    @Override
    public void enterTaxiway_parking(@NotNull AirportParser.Taxiway_parkingContext ctx)
    {
        enterNode(ctx, new TaxiwayparkingNode());
    }

    @Override
    public void exitTaxiway_parking(@NotNull AirportParser.Taxiway_parkingContext ctx)
    {
        addTaxiwayParkingIdentifier(ctx, this.nodes.peek());

        exitNode(ctx);
    }

    @Override
    public void enterTaxiway_parking_attr(@NotNull AirportParser.Taxiway_parking_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitTaxiway_parking_attr(@NotNull AirportParser.Taxiway_parking_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- TAXI NAME -------------------

    @Override
    public void enterTaxi_name(@NotNull AirportParser.Taxi_nameContext ctx)
    {
        enterNode(ctx, new TaxinameNode());
    }

    @Override
    public void exitTaxi_name(@NotNull AirportParser.Taxi_nameContext ctx)
    {
        addTaxiNameIdentifier(ctx, this.nodes.peek());

        exitNode(ctx);
    }

    @Override
    public void enterTaxi_name_attr(@NotNull AirportParser.Taxi_name_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitTaxi_name_attr(@NotNull AirportParser.Taxi_name_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- TAXIWAY PATH -------------------

    @Override
    public void enterTaxiway_path(@NotNull AirportParser.Taxiway_pathContext ctx)
    {
        enterNode(ctx, new TaxiwaypathNode());
    }
    @Override
    public void exitTaxiway_path(@NotNull AirportParser.Taxiway_pathContext ctx)
    {
        addTaxiwayPathReferencedIdentifiers(ctx, this.nodes.peek());

        exitNode(ctx);
    }

    @Override
    public void enterTaxiway_path_attr(@NotNull AirportParser.Taxiway_path_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitTaxiway_path_attr(@NotNull AirportParser.Taxiway_path_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- TAXIWAY SIGN -------------------

    @Override
    public void enterTaxiway_sign(@NotNull AirportParser.Taxiway_signContext ctx)
    {

    }

    @Override
    public void exitTaxiway_sign(@NotNull AirportParser.Taxiway_signContext ctx)
    {

    }

    @Override
    public void enterTaxiway_sign_attr(@NotNull AirportParser.Taxiway_sign_attrContext ctx)
    {

    }

    @Override
    public void exitTaxiway_sign_attr(@NotNull AirportParser.Taxiway_sign_attrContext ctx)
    {

    }


    // ------------------- TOWER -------------------

    @Override
    public void enterTower(@NotNull AirportParser.TowerContext ctx)
    {
        enterNode(ctx, new TowerNode());
    }

    @Override
    public void exitTower(@NotNull AirportParser.TowerContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterTower_attr(@NotNull AirportParser.Tower_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitTower_attr(@NotNull AirportParser.Tower_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterTower_descendants(@NotNull AirportParser.Tower_descendantsContext ctx)
    {
    }

    @Override
    public void exitTower_descendants(@NotNull AirportParser.Tower_descendantsContext ctx)
    {
    }


    // ------------------- SERVICES -------------------

    @Override
    public void enterServices(@NotNull AirportParser.ServicesContext ctx)
    {
        enterNode(ctx, new ServicesNode());
    }

    @Override
    public void exitServices(@NotNull AirportParser.ServicesContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterServices_descendant(@NotNull AirportParser.Services_descendantContext ctx)
    {
    }

    @Override
    public void exitServices_descendant(@NotNull AirportParser.Services_descendantContext ctx)
    {
    }


    // ------------------- FUEL -------------------

    @Override
    public void enterFuel(@NotNull AirportParser.FuelContext ctx)
    {
        enterNode(ctx, new FuelNode());
    }

    @Override
    public void exitFuel(@NotNull AirportParser.FuelContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterFuel_attr(@NotNull AirportParser.Fuel_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitFuel_attr(@NotNull AirportParser.Fuel_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- COM -------------------

    @Override
    public void enterCom(@NotNull AirportParser.ComContext ctx)
    {
        enterNode(ctx, new ComNode());
    }

    @Override
    public void exitCom(@NotNull AirportParser.ComContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterCom_attr(@NotNull AirportParser.Com_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitCom_attr(@NotNull AirportParser.Com_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- RUNWAY -------------------

    @Override
    public void enterRunway(@NotNull AirportParser.RunwayContext ctx)
    {
        enterNode(ctx, new RunwayNode());
    }

    @Override
    public void exitRunway(@NotNull AirportParser.RunwayContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterRunway_attr(@NotNull AirportParser.Runway_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitRunway_attr(@NotNull AirportParser.Runway_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterRunway_descendant(@NotNull AirportParser.Runway_descendantContext ctx)
    {

    }

    @Override
    public void exitRunway_descendant(@NotNull AirportParser.Runway_descendantContext ctx)
    {

    }


    // ------------------- RUNWAY START -------------------

    @Override
    public void enterRunway_start(@NotNull AirportParser.Runway_startContext ctx)
    {
        enterNode(ctx, new RunwayStartNode());
    }

    @Override
    public void exitRunway_start(@NotNull AirportParser.Runway_startContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterRunway_start_attr(@NotNull AirportParser.Runway_start_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitRunway_start_attr(@NotNull AirportParser.Runway_start_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- WAYPOINT -------------------

    @Override
    public void enterWaypoint(@NotNull AirportParser.WaypointContext ctx)
    {
        enterNode(ctx, new WaypointNode());
    }

    @Override
    public void exitWaypoint(@NotNull AirportParser.WaypointContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterWaypoint_attr(@NotNull AirportParser.Waypoint_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitWaypoint_attr(@NotNull AirportParser.Waypoint_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterWaypoint_descendant(@NotNull AirportParser.Waypoint_descendantContext ctx)
    {

    }

    @Override
    public void exitWaypoint_descendant(@NotNull AirportParser.Waypoint_descendantContext ctx)
    {

    }


    // ------------------- ROUTE -------------------

    @Override
    public void enterRoute(@NotNull AirportParser.RouteContext ctx)
    {
        enterNode(ctx, new RouteNode());
    }

    @Override
    public void exitRoute(@NotNull AirportParser.RouteContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterRoute_attr(@NotNull AirportParser.Route_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitRoute_attr(@NotNull AirportParser.Route_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterRoute_descendant(@NotNull AirportParser.Route_descendantContext ctx)
    {

    }

    @Override
    public void exitRoute_descendant(@NotNull AirportParser.Route_descendantContext ctx)
    {

    }


    // ------------------- PREVIOUS -------------------

    @Override
    public void enterPrevious(@NotNull AirportParser.PreviousContext ctx)
    {
        enterNode(ctx, new PreviousNode());
    }

    @Override
    public void exitPrevious(@NotNull AirportParser.PreviousContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterPrevious_attr(@NotNull AirportParser.Previous_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitPrevious_attr(@NotNull AirportParser.Previous_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- NEXT -------------------

    @Override
    public void enterNext(@NotNull AirportParser.NextContext ctx)
    {
        enterNode(ctx, new NextNode());
    }

    @Override
    public void exitNext(@NotNull AirportParser.NextContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterNext_attr(@NotNull AirportParser.Next_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitNext_attr(@NotNull AirportParser.Next_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- APPROACH -------------------

    @Override
    public void enterApproach(@NotNull AirportParser.ApproachContext ctx)
    {
        enterNode(ctx, new ApproachNode());
    }

    @Override
    public void exitApproach(@NotNull AirportParser.ApproachContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterApproach_attr(@NotNull AirportParser.Approach_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitApproach_attr(@NotNull AirportParser.Approach_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterApproach_descendant(@NotNull AirportParser.Approach_descendantContext ctx)
    {

    }

    @Override
    public void exitApproach_descendant(@NotNull AirportParser.Approach_descendantContext ctx)
    {

    }


    // ------------------- APPROACH LEGS -------------------

    @Override
    public void enterApproach_legs(@NotNull AirportParser.Approach_legsContext ctx)
    {
        enterNode(ctx, new ApproachlegsNode());
    }

    @Override
    public void exitApproach_legs(@NotNull AirportParser.Approach_legsContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterApproach_legs_descendant(@NotNull AirportParser.Approach_legs_descendantContext ctx)
    {
    }

    @Override
    public void exitApproach_legs_descendant(@NotNull AirportParser.Approach_legs_descendantContext ctx)
    {
    }


    // ------------------- LEG -------------------

    @Override
    public void enterLeg(@NotNull AirportParser.LegContext ctx)
    {
        enterNode(ctx, new LegNode());
    }

    @Override
    public void exitLeg(@NotNull AirportParser.LegContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterLeg_attr(@NotNull AirportParser.Leg_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitLeg_attr(@NotNull AirportParser.Leg_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- MISSED APPROACH LEGS -------------------

    @Override
    public void enterMissed_approach_legs(@NotNull AirportParser.Missed_approach_legsContext ctx)
    {
        enterNode(ctx, new MissedapproachlegsNode());
    }

    @Override
    public void exitMissed_approach_legs(@NotNull AirportParser.Missed_approach_legsContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterMissed_approach_legs_descendant(@NotNull AirportParser.Missed_approach_legs_descendantContext ctx)
    {
    }

    @Override
    public void exitMissed_approach_legs_descendant(@NotNull AirportParser.Missed_approach_legs_descendantContext ctx)
    {
    }


    // ------------------- TRANSITION -------------------

    @Override
    public void enterTransition(@NotNull AirportParser.TransitionContext ctx)
    {
        enterNode(ctx, new TransitionNode());
    }

    @Override
    public void exitTransition(@NotNull AirportParser.TransitionContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterTransition_attr(@NotNull AirportParser.Transition_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitTransition_attr(@NotNull AirportParser.Transition_attrContext ctx)
    {
        exitAttribute(ctx);
    }

    @Override
    public void enterTransition_descendant(@NotNull AirportParser.Transition_descendantContext ctx)
    {

    }

    @Override
    public void exitTransition_descendant(@NotNull AirportParser.Transition_descendantContext ctx)
    {

    }


    // ------------------- TRANSITION LEGS -------------------

    @Override
    public void enterTransition_legs(@NotNull AirportParser.Transition_legsContext ctx)
    {
        enterNode(ctx, new TransitionlegsNode());
    }

    @Override
    public void exitTransition_legs(@NotNull AirportParser.Transition_legsContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterTransition_legs_descendant(@NotNull AirportParser.Transition_legs_descendantContext ctx)
    {
    }

    @Override
    public void exitTransition_legs_descendant(@NotNull AirportParser.Transition_legs_descendantContext ctx)
    {
    }


    // ------------------- HELIPAD -------------------

    @Override
    public void enterHelipad(@NotNull AirportParser.HelipadContext ctx)
    {
        enterNode(ctx, new HelipadNode());
    }

    @Override
    public void exitHelipad(@NotNull AirportParser.HelipadContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterHelipad_attr(@NotNull AirportParser.Helipad_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitHelipad_attr(@NotNull AirportParser.Helipad_attrContext ctx)
    {
        exitAttribute(ctx);
    }


    // ------------------- START -------------------

    @Override
    public void enterStart(@NotNull AirportParser.StartContext ctx)
    {
        StartNode node = new StartNode();

        enterNode(ctx, node);
    }

    @Override
    public void exitStart(@NotNull AirportParser.StartContext ctx)
    {
        exitNode(ctx);
    }

    @Override
    public void enterStart_attr(@NotNull AirportParser.Start_attrContext ctx)
    {
        enterAttribute(ctx);
    }

    @Override
    public void exitStart_attr(@NotNull AirportParser.Start_attrContext ctx)
    {
        exitAttribute(ctx);
    }



    // ------------------- SEMANTIC SPECIFIC -------------------

    @Override
    public void visitTerminal(TerminalNode terminalNode)
    {

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode)
    {

    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext)
    {

    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext)
    {

    }


    // ------------------- UNUSED -------------------

    @Override
    public void enterAirport_other_info(@NotNull AirportParser.Airport_other_infoContext ctx)
    {

    }

    @Override
    public void exitAirport_other_info(@NotNull AirportParser.Airport_other_infoContext ctx)
    {
    }

    @Override
    public void enterOther(@NotNull AirportParser.OtherContext ctx)
    {

    }

    @Override
    public void exitOther(@NotNull AirportParser.OtherContext ctx)
    {

    }

    @Override
    public void enterMarkings(@NotNull AirportParser.MarkingsContext ctx)
    {

    }

    @Override
    public void exitMarkings(@NotNull AirportParser.MarkingsContext ctx)
    {

    }

    @Override
    public void enterMarkings_attr(@NotNull AirportParser.Markings_attrContext ctx)
    {

    }

    @Override
    public void exitMarkings_attr(@NotNull AirportParser.Markings_attrContext ctx)
    {

    }

    @Override
    public void enterLights(@NotNull AirportParser.LightsContext ctx)
    {

    }

    @Override
    public void exitLights(@NotNull AirportParser.LightsContext ctx)
    {

    }

    @Override
    public void enterLights_attr(@NotNull AirportParser.Lights_attrContext ctx)
    {

    }

    @Override
    public void exitLights_attr(@NotNull AirportParser.Lights_attrContext ctx)
    {

    }

    @Override
    public void enterOffset_threshold(@NotNull AirportParser.Offset_thresholdContext ctx)
    {

    }

    @Override
    public void exitOffset_threshold(@NotNull AirportParser.Offset_thresholdContext ctx)
    {

    }

    @Override
    public void enterOffset_threshold_attr(@NotNull AirportParser.Offset_threshold_attrContext ctx)
    {

    }

    @Override
    public void exitOffset_threshold_attr(@NotNull AirportParser.Offset_threshold_attrContext ctx)
    {

    }

    @Override
    public void enterBlast_pad(@NotNull AirportParser.Blast_padContext ctx)
    {

    }

    @Override
    public void exitBlast_pad(@NotNull AirportParser.Blast_padContext ctx)
    {

    }

    @Override
    public void enterBlast_pad_attr(@NotNull AirportParser.Blast_pad_attrContext ctx)
    {

    }

    @Override
    public void exitBlast_pad_attr(@NotNull AirportParser.Blast_pad_attrContext ctx)
    {

    }

    @Override
    public void enterOverrun(@NotNull AirportParser.OverrunContext ctx)
    {

    }

    @Override
    public void exitOverrun(@NotNull AirportParser.OverrunContext ctx)
    {

    }

    @Override
    public void enterOverrun_attr(@NotNull AirportParser.Overrun_attrContext ctx)
    {

    }

    @Override
    public void exitOverrun_attr(@NotNull AirportParser.Overrun_attrContext ctx)
    {

    }

    @Override
    public void enterApproach_lights(@NotNull AirportParser.Approach_lightsContext ctx)
    {

    }

    @Override
    public void exitApproach_lights(@NotNull AirportParser.Approach_lightsContext ctx)
    {

    }

    @Override
    public void enterApproach_lights_attr(@NotNull AirportParser.Approach_lights_attrContext ctx)
    {

    }

    @Override
    public void exitApproach_lights_attr(@NotNull AirportParser.Approach_lights_attrContext ctx)
    {

    }

    @Override
    public void enterVasi(@NotNull AirportParser.VasiContext ctx)
    {

    }

    @Override
    public void exitVasi(@NotNull AirportParser.VasiContext ctx)
    {

    }

    @Override
    public void enterVasi_attr(@NotNull AirportParser.Vasi_attrContext ctx)
    {

    }

    @Override
    public void exitVasi_attr(@NotNull AirportParser.Vasi_attrContext ctx)
    {

    }

    @Override
    public void enterIls(@NotNull AirportParser.IlsContext ctx)
    {

    }

    @Override
    public void exitIls(@NotNull AirportParser.IlsContext ctx)
    {

    }

    @Override
    public void enterIls_attr(@NotNull AirportParser.Ils_attrContext ctx)
    {

    }

    @Override
    public void exitIls_attr(@NotNull AirportParser.Ils_attrContext ctx)
    {

    }

    @Override
    public void enterIls_descendant(@NotNull AirportParser.Ils_descendantContext ctx)
    {

    }

    @Override
    public void exitIls_descendant(@NotNull AirportParser.Ils_descendantContext ctx)
    {

    }

    @Override
    public void enterGlide_slope(@NotNull AirportParser.Glide_slopeContext ctx)
    {

    }

    @Override
    public void exitGlide_slope(@NotNull AirportParser.Glide_slopeContext ctx)
    {

    }

    @Override
    public void enterGlide_slope_attr(@NotNull AirportParser.Glide_slope_attrContext ctx)
    {

    }

    @Override
    public void exitGlide_slope_attr(@NotNull AirportParser.Glide_slope_attrContext ctx)
    {

    }

    @Override
    public void enterRunway_alias(@NotNull AirportParser.Runway_aliasContext ctx)
    {

    }

    @Override
    public void exitRunway_alias(@NotNull AirportParser.Runway_aliasContext ctx)
    {

    }

    @Override
    public void enterRunway_alias_attr(@NotNull AirportParser.Runway_alias_attrContext ctx)
    {

    }

    @Override
    public void exitRunway_alias_attr(@NotNull AirportParser.Runway_alias_attrContext ctx)
    {

    }

    @Override
    public void enterAprons(@NotNull AirportParser.ApronsContext ctx)
    {

    }

    @Override
    public void exitAprons(@NotNull AirportParser.ApronsContext ctx)
    {

    }

    @Override
    public void enterApron(@NotNull AirportParser.ApronContext ctx)
    {

    }

    @Override
    public void exitApron(@NotNull AirportParser.ApronContext ctx)
    {

    }

    @Override
    public void enterApron_attr(@NotNull AirportParser.Apron_attrContext ctx)
    {

    }

    @Override
    public void exitApron_attr(@NotNull AirportParser.Apron_attrContext ctx)
    {

    }

    @Override
    public void enterApron_descendant(@NotNull AirportParser.Apron_descendantContext ctx)
    {

    }

    @Override
    public void exitApron_descendant(@NotNull AirportParser.Apron_descendantContext ctx)
    {

    }

    @Override
    public void enterVertex(@NotNull AirportParser.VertexContext ctx)
    {

    }

    @Override
    public void exitVertex(@NotNull AirportParser.VertexContext ctx)
    {

    }

    @Override
    public void enterVertex_attr(@NotNull AirportParser.Vertex_attrContext ctx)
    {

    }

    @Override
    public void exitVertex_attr(@NotNull AirportParser.Vertex_attrContext ctx)
    {

    }

    @Override
    public void enterAprons_edge_lights(@NotNull AirportParser.Aprons_edge_lightsContext ctx)
    {

    }

    @Override
    public void exitAprons_edge_lights(@NotNull AirportParser.Aprons_edge_lightsContext ctx)
    {

    }

    @Override
    public void enterAprons_edge_lights_descendant(@NotNull AirportParser.Aprons_edge_lights_descendantContext ctx)
    {

    }

    @Override
    public void exitAprons_edge_lights_descendant(@NotNull AirportParser.Aprons_edge_lights_descendantContext ctx)
    {

    }

    @Override
    public void enterEdge_lights(@NotNull AirportParser.Edge_lightsContext ctx)
    {

    }

    @Override
    public void exitEdge_lights(@NotNull AirportParser.Edge_lightsContext ctx)
    {

    }

    @Override
    public void enterDme_arc(@NotNull AirportParser.Dme_arcContext ctx)
    {

    }

    @Override
    public void exitDme_arc(@NotNull AirportParser.Dme_arcContext ctx)
    {

    }

    @Override
    public void enterDme_arc_attr(@NotNull AirportParser.Dme_arc_attrContext ctx)
    {

    }

    @Override
    public void exitDme_arc_attr(@NotNull AirportParser.Dme_arc_attrContext ctx)
    {

    }

    @Override
    public void enterNdb(@NotNull AirportParser.NdbContext ctx)
    {

    }

    @Override
    public void exitNdb(@NotNull AirportParser.NdbContext ctx)
    {

    }

    @Override
    public void enterNdb_attr(@NotNull AirportParser.Ndb_attrContext ctx)
    {

    }

    @Override
    public void exitNdb_attr(@NotNull AirportParser.Ndb_attrContext ctx)
    {

    }

    @Override
    public void enterNdb_descendant(@NotNull AirportParser.Ndb_descendantContext ctx)
    {

    }

    @Override
    public void exitNdb_descendant(@NotNull AirportParser.Ndb_descendantContext ctx)
    {

    }

    @Override
    public void enterVisual_model(@NotNull AirportParser.Visual_modelContext ctx)
    {

    }

    @Override
    public void exitVisual_model(@NotNull AirportParser.Visual_modelContext ctx)
    {

    }

    @Override
    public void enterVisual_model_attr(@NotNull AirportParser.Visual_model_attrContext ctx)
    {

    }

    @Override
    public void exitVisual_model_attr(@NotNull AirportParser.Visual_model_attrContext ctx)
    {

    }

    @Override
    public void enterVisual_model_descendant(@NotNull AirportParser.Visual_model_descendantContext ctx)
    {

    }

    @Override
    public void exitVisual_model_descendant(@NotNull AirportParser.Visual_model_descendantContext ctx)
    {

    }

    @Override
    public void enterDme(@NotNull AirportParser.DmeContext ctx)
    {

    }

    @Override
    public void exitDme(@NotNull AirportParser.DmeContext ctx)
    {

    }

    @Override
    public void enterDme_attr(@NotNull AirportParser.Dme_attrContext ctx)
    {

    }

    @Override
    public void exitDme_attr(@NotNull AirportParser.Dme_attrContext ctx)
    {

    }

    @Override
    public void enterJetway(@NotNull AirportParser.JetwayContext ctx)
    {

    }

    @Override
    public void exitJetway(@NotNull AirportParser.JetwayContext ctx)
    {

    }

    @Override
    public void enterJetway_attr(@NotNull AirportParser.Jetway_attrContext ctx)
    {

    }

    @Override
    public void exitJetway_attr(@NotNull AirportParser.Jetway_attrContext ctx)
    {

    }

    @Override
    public void enterJetway_descendant(@NotNull AirportParser.Jetway_descendantContext ctx)
    {

    }

    @Override
    public void exitJetway_descendant(@NotNull AirportParser.Jetway_descendantContext ctx)
    {

    }

    @Override
    public void enterScenery_object(@NotNull AirportParser.Scenery_objectContext ctx)
    {

    }

    @Override
    public void exitScenery_object(@NotNull AirportParser.Scenery_objectContext ctx)
    {

    }

    @Override
    public void enterScenery_object_attr(@NotNull AirportParser.Scenery_object_attrContext ctx)
    {

    }

    @Override
    public void exitScenery_object_attr(@NotNull AirportParser.Scenery_object_attrContext ctx)
    {

    }

    @Override
    public void enterScenery_object_descendant(@NotNull AirportParser.Scenery_object_descendantContext ctx)
    {

    }

    @Override
    public void exitScenery_object_descendant(@NotNull AirportParser.Scenery_object_descendantContext ctx)
    {

    }

    @Override
    public void enterBias_xyz(@NotNull AirportParser.Bias_xyzContext ctx)
    {

    }

    @Override
    public void exitBias_xyz(@NotNull AirportParser.Bias_xyzContext ctx)
    {

    }

    @Override
    public void enterBias_xyz_attr(@NotNull AirportParser.Bias_xyz_attrContext ctx)
    {

    }

    @Override
    public void exitBias_xyz_attr(@NotNull AirportParser.Bias_xyz_attrContext ctx)
    {

    }

    @Override
    public void enterBeacon(@NotNull AirportParser.BeaconContext ctx)
    {

    }

    @Override
    public void exitBeacon(@NotNull AirportParser.BeaconContext ctx)
    {

    }

    @Override
    public void enterBeacon_attr(@NotNull AirportParser.Beacon_attrContext ctx)
    {

    }

    @Override
    public void exitBeacon_attr(@NotNull AirportParser.Beacon_attrContext ctx)
    {

    }

    @Override
    public void enterEffect(@NotNull AirportParser.EffectContext ctx)
    {

    }

    @Override
    public void exitEffect(@NotNull AirportParser.EffectContext ctx)
    {

    }

    @Override
    public void enterEffect_attr(@NotNull AirportParser.Effect_attrContext ctx)
    {

    }

    @Override
    public void exitEffect_attr(@NotNull AirportParser.Effect_attrContext ctx)
    {

    }

    @Override
    public void enterGeneric_building(@NotNull AirportParser.Generic_buildingContext ctx)
    {

    }

    @Override
    public void exitGeneric_building(@NotNull AirportParser.Generic_buildingContext ctx)
    {

    }

    @Override
    public void enterGeneric_building_attr(@NotNull AirportParser.Generic_building_attrContext ctx)
    {

    }

    @Override
    public void exitGeneric_building_attr(@NotNull AirportParser.Generic_building_attrContext ctx)
    {

    }

    @Override
    public void enterGeneric_building_descendant(@NotNull AirportParser.Generic_building_descendantContext ctx)
    {

    }

    @Override
    public void exitGeneric_building_descendant(@NotNull AirportParser.Generic_building_descendantContext ctx)
    {

    }

    @Override
    public void enterRectangular_building(@NotNull AirportParser.Rectangular_buildingContext ctx)
    {

    }

    @Override
    public void exitRectangular_building(@NotNull AirportParser.Rectangular_buildingContext ctx)
    {

    }

    @Override
    public void enterRectangular_building_attr(@NotNull AirportParser.Rectangular_building_attrContext ctx)
    {

    }

    @Override
    public void exitRectangular_building_attr(@NotNull AirportParser.Rectangular_building_attrContext ctx)
    {

    }

    @Override
    public void enterPyramidal_building(@NotNull AirportParser.Pyramidal_buildingContext ctx)
    {

    }

    @Override
    public void exitPyramidal_building(@NotNull AirportParser.Pyramidal_buildingContext ctx)
    {

    }

    @Override
    public void enterPyramidal_building_attr(@NotNull AirportParser.Pyramidal_building_attrContext ctx)
    {

    }

    @Override
    public void exitPyramidal_building_attr(@NotNull AirportParser.Pyramidal_building_attrContext ctx)
    {

    }

    @Override
    public void enterMulti_sided_building(@NotNull AirportParser.Multi_sided_buildingContext ctx)
    {

    }

    @Override
    public void exitMulti_sided_building(@NotNull AirportParser.Multi_sided_buildingContext ctx)
    {

    }

    @Override
    public void enterMulti_sided_building_attr(@NotNull AirportParser.Multi_sided_building_attrContext ctx)
    {

    }

    @Override
    public void exitMulti_sided_building_attr(@NotNull AirportParser.Multi_sided_building_attrContext ctx)
    {

    }

    @Override
    public void enterLibrary_object(@NotNull AirportParser.Library_objectContext ctx)
    {

    }

    @Override
    public void exitLibrary_object(@NotNull AirportParser.Library_objectContext ctx)
    {

    }

    @Override
    public void enterLibrary_object_attr(@NotNull AirportParser.Library_object_attrContext ctx)
    {

    }

    @Override
    public void exitLibrary_object_attr(@NotNull AirportParser.Library_object_attrContext ctx)
    {

    }

    @Override
    public void enterTrigger(@NotNull AirportParser.TriggerContext ctx)
    {

    }

    @Override
    public void exitTrigger(@NotNull AirportParser.TriggerContext ctx)
    {

    }

    @Override
    public void enterTrigger_attr(@NotNull AirportParser.Trigger_attrContext ctx)
    {

    }

    @Override
    public void exitTrigger_attr(@NotNull AirportParser.Trigger_attrContext ctx)
    {

    }

    @Override
    public void enterTrigger_descendant(@NotNull AirportParser.Trigger_descendantContext ctx)
    {

    }

    @Override
    public void exitTrigger_descendant(@NotNull AirportParser.Trigger_descendantContext ctx)
    {

    }

    @Override
    public void enterTrigger_weather_data(@NotNull AirportParser.Trigger_weather_dataContext ctx)
    {

    }

    @Override
    public void exitTrigger_weather_data(@NotNull AirportParser.Trigger_weather_dataContext ctx)
    {

    }

    @Override
    public void enterTrigger_weather_data_attr(@NotNull AirportParser.Trigger_weather_data_attrContext ctx)
    {

    }

    @Override
    public void exitTrigger_weather_data_attr(@NotNull AirportParser.Trigger_weather_data_attrContext ctx)
    {

    }

    @Override
    public void enterWindsock(@NotNull AirportParser.WindsockContext ctx)
    {

    }

    @Override
    public void exitWindsock(@NotNull AirportParser.WindsockContext ctx)
    {

    }

    @Override
    public void enterWindsock_attr(@NotNull AirportParser.Windsock_attrContext ctx)
    {

    }

    @Override
    public void exitWindsock_attr(@NotNull AirportParser.Windsock_attrContext ctx)
    {

    }

    @Override
    public void enterWindsock_descendant(@NotNull AirportParser.Windsock_descendantContext ctx)
    {

    }

    @Override
    public void exitWindsock_descendant(@NotNull AirportParser.Windsock_descendantContext ctx)
    {

    }

    @Override
    public void enterPole_color(@NotNull AirportParser.Pole_colorContext ctx)
    {

    }

    @Override
    public void exitPole_color(@NotNull AirportParser.Pole_colorContext ctx)
    {

    }

    @Override
    public void enterPole_color_attr(@NotNull AirportParser.Pole_color_attrContext ctx)
    {

    }

    @Override
    public void exitPole_color_attr(@NotNull AirportParser.Pole_color_attrContext ctx)
    {

    }

    @Override
    public void enterSock_color(@NotNull AirportParser.Sock_colorContext ctx)
    {

    }

    @Override
    public void exitSock_color(@NotNull AirportParser.Sock_colorContext ctx)
    {

    }

    @Override
    public void enterSock_color_attr(@NotNull AirportParser.Sock_color_attrContext ctx)
    {

    }

    @Override
    public void exitSock_color_attr(@NotNull AirportParser.Sock_color_attrContext ctx)
    {

    }

    @Override
    public void enterAttached_object(@NotNull AirportParser.Attached_objectContext ctx)
    {

    }

    @Override
    public void exitAttached_object(@NotNull AirportParser.Attached_objectContext ctx)
    {

    }

    @Override
    public void enterAttached_object_attr(@NotNull AirportParser.Attached_object_attrContext ctx)
    {

    }

    @Override
    public void exitAttached_object_attr(@NotNull AirportParser.Attached_object_attrContext ctx)
    {

    }

    @Override
    public void enterAttached_object_descendant(@NotNull AirportParser.Attached_object_descendantContext ctx)
    {

    }

    @Override
    public void exitAttached_object_descendant(@NotNull AirportParser.Attached_object_descendantContext ctx)
    {

    }

    @Override
    public void enterRandom_attach(@NotNull AirportParser.Random_attachContext ctx)
    {

    }

    @Override
    public void exitRandom_attach(@NotNull AirportParser.Random_attachContext ctx)
    {


    }

    @Override
    public void enterRandom_attach_attr(@NotNull AirportParser.Random_attach_attrContext ctx)
    {

    }

    @Override
    public void exitRandom_attach_attr(@NotNull AirportParser.Random_attach_attrContext ctx)
    {

    }

    @Override
    public void enterBlast_fence(@NotNull AirportParser.Blast_fenceContext ctx)
    {

    }

    @Override
    public void exitBlast_fence(@NotNull AirportParser.Blast_fenceContext ctx)
    {

    }

    @Override
    public void enterBlast_fence_attr(@NotNull AirportParser.Blast_fence_attrContext ctx)
    {

    }

    @Override
    public void exitBlast_fence_attr(@NotNull AirportParser.Blast_fence_attrContext ctx)
    {

    }

    @Override
    public void enterBlast_fence_descendant(@NotNull AirportParser.Blast_fence_descendantContext ctx)
    {

    }

    @Override
    public void exitBlast_fence_descendant(@NotNull AirportParser.Blast_fence_descendantContext ctx)
    {

    }

    @Override
    public void enterBoundary_fence(@NotNull AirportParser.Boundary_fenceContext ctx)
    {

    }

    @Override
    public void exitBoundary_fence(@NotNull AirportParser.Boundary_fenceContext ctx)
    {

    }

    @Override
    public void enterBoundary_fence_attr(@NotNull AirportParser.Boundary_fence_attrContext ctx)
    {

    }

    @Override
    public void exitBoundary_fence_attr(@NotNull AirportParser.Boundary_fence_attrContext ctx)
    {

    }

    @Override
    public void enterBoundary_fence_descendant(@NotNull AirportParser.Boundary_fence_descendantContext ctx)
    {

    }

    @Override
    public void exitBoundary_fence_descendant(@NotNull AirportParser.Boundary_fence_descendantContext ctx)
    {

    }

    @Override
    public void enterDelete_airport(@NotNull AirportParser.Delete_airportContext ctx)
    {

    }

    @Override
    public void exitDelete_airport(@NotNull AirportParser.Delete_airportContext ctx)
    {

    }

    @Override
    public void enterDelete_airport_attr(@NotNull AirportParser.Delete_airport_attrContext ctx)
    {

    }

    @Override
    public void exitDelete_airport_attr(@NotNull AirportParser.Delete_airport_attrContext ctx)
    {

    }
}
