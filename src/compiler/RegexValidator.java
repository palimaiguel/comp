package compiler;//import gen.*;

/**
 * LATITUDES IGUAL
 * LONGITUDE IGUAL
 * SURFACE IGUAL
 */
public class RegexValidator
{
    static public boolean latitude(String s){
        Double d = 0.0;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            // accept N47 25.91 , for example
            return s.matches("^N[-]?[0-9]*(\\.[0-9]*)? [-]?[0-9]*(\\.[0-9]*)?$");
        }
        return d >= -90 && d <= 90;
    }

    static public boolean longitude(String s){
        Double d = 0.0;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            //accept W122 18.50, for example
            return s.matches("^W[-]?[0-9]*(\\.[0-9]*)? [-]?[0-9]*(\\.[0-9]*)?$");
        }
        return d >= -180 && d <= 180;
    }

    /********************
     *     Airport
     */
    static public boolean airportRegion(String s)
    {
        return s.length() <= 48;
    }

    static public boolean airportCountry(String s)
    {
        return airportRegion(s);
    }

    static public boolean airportState(String s)
    {
        return airportRegion(s);
    }

    static public boolean airportCity(String s)
    {
        return airportRegion(s);
    }

    static public boolean airportName(String s)
    {
        return airportRegion(s);
    }

    static public boolean airportAltitude(String s)
    {
        return helipadAltitude(s);
    }

    static public boolean airportMagvar(String s)
    {
        return waypointMagvar(s);
    }

    static public boolean airportIdent(String s)
    {
        return s.length() <= 4;
    }

    static public boolean airportTestRadius(String s)
    {
        return runwayPrimaryMarkingBias(s);
    }

    static public boolean airportTrafficScalar(String s)
    {
        double d;

        try
        { d = Double.parseDouble(s); }
        catch (Exception e)
        { return false; }

        return d >= 0.01 && d <= 1.0;
    }


    /********************
     *     TaxiwayPoint
     */

    static public boolean taxiwayPointIndex(String s)
    {
        int d;

        try
        { d = Integer.parseInt(s); }
        catch (Exception e)
        { return false; }

        return d >= 0 && d <= 3999;
    }

    static public boolean taxiwayPointType(String s)
    {

        switch (s) {
            case "NORMAL":
            case "HOLD_SHORT":
            case "ILS_HOLD_SHORT":
            case "HOLD_SHORT_NO_DRAW":
            case "ILS_HOLD_SHORT_NO_DRAW":
                return true;
        }
        return false;
    }

    static public boolean taxiwayPointOrientation(String s){

        switch (s) {
            case "FORWARD":
            case "REVERSE":
                return true;
        }
        return false;
    }
    static public boolean taxiwayPointBiasX(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return true;

    }

    static public boolean taxiwayPointBiasZ(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return true;
    }

    /********************
     *     TaxiwayParking
     */

    static public boolean taxiwayParkingIndex(String s){
        return taxiwayPointIndex(s);
    }

    static public boolean taxiwayParkingBiasX(String s){
        return taxiwayPointBiasX(s);
    }

    static public boolean taxiwayParkingBiasZ(String s){
        return taxiwayPointBiasZ(s);
    }

    static public boolean taxiwayParkingHeading(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return d >= 0.0 && d <= 360.0;
    }

    static public boolean taxiwayParkingRadius(String s){
        /*
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return true;
        */

        //return s.matches("^[0-9]*\\.?[0-9]*[M]?$");
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[M]?$");
    }

    static public boolean taxiwayParkingType(String s){
    switch (s) {
        case "NONE":
        case "DOCK_GA":
        case "FUEL":
        case "GATE_HEAVY":
        case "GATE_MEDIUM":
        case "GATE_SMALL":
        case "RAMP_CARGO":
        case "RAMP_GA":
        case "RAMP_GA_LARGE":
        case "RAMP_GA_MEDIUM":
        case "RAMP_GA_SMALL":
        case "RAMP_MIL_CARGO":
        case "RAMP_MIL_COMBAT":
        case "VEHICLE":
            return true;
    }
    return false;
}

    static public boolean taxiwayParkingName(String s){
        switch (s) {
            case "PARKING":
            case "DOCK":
            case "GATE":
            case "N_PARKING":
            case "NE_PARKING":
            case "NW_PARKING":
            case "SE_PARKING":
            case "S_PARKING":
            case "SW_PARKING":
            case "W_PARKING":
            case "E_PARKING":
                return true;
        }
        return s.matches("^GATE_[A-Z]$");
    }

    static public boolean taxiwayParkingNumber(String s){
        return taxiwayPointIndex(s);
    }

    static public boolean taxiwayParkingAirlineCodes(String s){
        String[] icaos = s.split(",");

        for(String icao : icaos)
        {
            icao = icao.trim();
            if (!airportIdent(icao))
                return false;
        }

        return true;
    }

    static public boolean taxiwayParkingPushBack(String s){
        switch (s) {
            case "NONE":
            case "BOTH":
            case "LEFT":
            case "RIGHT":
                return true;
        }
        return false;
    }

    static public boolean taxiwayParkingteeOffset1(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return d >= 0.1 && d <= 50.0;

    }

    static public boolean taxiwayParkingteeOffset2(String s){
        return taxiwayParkingteeOffset1(s);
    }

    static public boolean taxiwayParkingteeOffset3(String s){
        return taxiwayParkingteeOffset1(s);
    }

    static public boolean taxiwayParkingteeOffset4(String s){
        return taxiwayParkingteeOffset1(s);
    }

    /********************
     *     Taxiname
     */

    static public boolean taxinameIndex(String s){
        int d;
        try {
            d = Integer.parseInt(s);
        } catch (Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 255;
    }

    static public boolean taxinameName(String s){
        return s.length() <= 8;
    }

    /********************
     *     TaxiwayPath
     */

    static public boolean taxiwayPathType(String s){
        switch(s)
        {
            case "RUNWAY":
            case "PARKING":
            case "TAXI":
            case "PATH":
            case "CLOSED":
            case "VEHICLE":
                return true;
        }
        return false;
    }

    static public boolean taxiwayPathStart(String s){
        int d;
        try {
            d = Integer.parseInt(s);
        } catch (Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 3999;
    }

    static public boolean taxiwayPathEnd(String s){
        return taxiwayPathStart(s);
    }

    static public boolean taxiwayPathWidth(String s){
        /*
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return true;
        */

        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[M]?$");
    }

    static public boolean taxiwayPathWeightLimit(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }

        return true;
    }

    static public boolean taxiwayPathSurface(String s){
        switch(s)
        {
            case "ASPHALT":
            case "BITUMINOUS":
            case "BRICK":
            case "CLAY":
            case "CEMENT":
            case "CONCRETE":
            case "CORAL":
            case "DIRT":
            case "GRASS":
            case "GRAVEL":
            case "ICE":
            case "MACADAM":
            case "OIL_TREATED":
            case "PLANKS":
            case "SAND":
            case "SHALE":
            case "SNOW":
            case "STEEL_MATS":
            case "TARMAC":
            case "UNKNOWN":
            case "WATER":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathDrawSurface(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathDrawDetail(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathCenterLine(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathCenterLineLighted(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathLeftEdge(String s){
        switch(s)
        {
            case "NONE":
            case "SOLID":
            case "DASHED":
            case "SOLID_DASHED":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathLeftEdgeLighted(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathRightEdge(String s){
        switch(s)
        {
            case "NONE":
            case "SOLID":
            case "DASHED":
            case "SOLID_DASHED":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathRightEdgeLighted(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean taxiwayPathNumber(String s){
        return approachRunway(s);
    }

    static public boolean taxiwayPathDesignator(String s){
        return approachDesignator(s);
    }

    static public boolean taxiwayPathName(String s){
        return helipadRed(s);
    }


    /********************
     *     Tower
     */

    static public boolean towerAltitude(String s){
        return helipadAltitude(s);
    }

    /********************
     *     Services
     */

    /********************
     *     Fuel
     */

    static public boolean fuelType(String s){
        switch(s)
        {
            case "73":
            case "87":
            case "100":
            case "130":
            case "145":
            case "MOGAS":
            case "JET":
            case "JETA":
            case "JETA1":
            case "JETAP":
            case "JETB":
            case "JET4":
            case "JET5":
            case "UNKNOWN":
                return true;
        }

        return false;
    }

    static public boolean fuelAvailability(String s){
        switch(s)
        {
            case "YES":
            case "NO":
            case "UNKNOWN":
            case "PRIOR_REQUEST":
                return true;
        }

        return false;
    }

    /********************
     *     Com
     */

    static public boolean comFrequency(String s){
        Double d;
        try {
            d = Double.parseDouble(s);
        } catch (Exception e)
        {
            return false;
        }
        return d >= 108 && d <= 136.992;
    }

    static public boolean comType(String s){
        switch(s)
        {
            case "APPROACH":
            case "ASOS":
            case "ATIS":
            case "AWOS":
            case "CENTER":
            case "CLEARANCE":
            case "CLEARANCE_PRE_TAXI":
            case "CTAF":
            case "DEPARTURE":
            case "FSS":
            case "GROUND":
            case "MULTICOM":
            case "REMOTE_CLEARANCE_DELIVERY":
            case "TOWER":
            case "UNICOM":
                return true;
        }

        return false;
    }

    static public boolean comName(String s){
        return s.length() <= 48;
    }

    /********************
     *     Runway
     */

    static public boolean runwayAltitude(String s){
        return helipadAltitude(s);
    }

    static public boolean runwaySurface(String s){
        switch(s)
        {
            case "ASPHALT":
            case "BITUMINOUS":
            case "BRICK":
            case "CLAY":
            case "CEMENT":
            case "CONCRETE":
            case "CORAL":
            case "DIRT":
            case "GRASS":
            case "GRAVEL":
            case "ICE":
            case "MACADAM":
            case "OIL_TREATED, PLANKS":
            case "SAND":
            case "SHALE":
            case "SNOW":
            case "STEEL_MATS":
            case "TARMAC":
            case "UNKNOWN":
            case "WATER":
                return true;
        }

        return false;
    }

    static public boolean runwayHeading(String s){
        Double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 360.0;
    }

    static public boolean runwayLength(String s){
        return helipadAltitude(s);
    }

    static public boolean runwayWidth(String s){
        return helipadAltitude(s);
    }

    static public boolean runwayNumber(String s){
        return approachRunway(s);
    }

    static public boolean runwayDesignator(String s){
        return approachDesignator(s);
    }

    static public boolean runwayPrimaryDesignator(String s){
        return approachDesignator(s);
    }

    static public boolean runwaySecondaryDesignator(String s){
        return approachDesignator(s);
    }

    static public boolean runwayPatternAltitude(String s){
        return helipadAltitude(s);
    }

    static public boolean runwayPrimaryTakeoff(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
            case "YES":
            case "NO":
                return true;
        }

        return false;
    }

    static public boolean runwayPrimaryLanding(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
            case "YES":
            case "NO":
                return true;
        }

        return false;
    }

    static public boolean runwayPrimaryPattern(String s){
        switch(s){
            case "LEFT":
            case "RIGHT":
                return true;
        }

        return false;
    }

    static public boolean runwaySecondaryTakeoff(String s){
        return runwayPrimaryTakeoff(s);
    }

    static public boolean runwaySecondaryLanding(String s){
        return runwayPrimaryLanding(s);
    }

    static public boolean runwaySecondaryPattern(String s){
        return runwayPrimaryPattern(s);
    }

    static public boolean runwayPrimaryMarkingBias(String s){
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[MFN]?$");
    }

    static public boolean runwaySecondaryMarkingBias(String s){
        return runwayPrimaryMarkingBias(s);
    }

    /********************
     *     RunwayStart
     */

    static public boolean runwayStartType(String s){
        return s.equals("RUNWAY");

    }

    static public boolean runwayStartLatitude(String s){
        return latitude(s);
    }

    static public boolean runwayStartLongitude(String s){
        return longitude(s);
    }

    static public boolean runwayStartAltitude(String s){
        return helipadAltitude(s);
    }

    static public boolean runwayStartHeading(String s){
        return runwayHeading(s);
    }

    static public boolean runwayStartEnd(String s){
        switch(s)
        {
            case "PRIMARY":
            case "SECONDARY":
                return true;
        }

        return false;
    }


    /***
     *   Waypoint
     */
    static public boolean waypointType(String s){
        switch (s) {
            case "NAMED":
            case "UNNAMED":
            case "VOR":
            case "NDB":
            case "OFF_ROUTE":
            case "IAF":
            case "FAF":
                return true;
        }
        return false;
    }

    static public boolean waypointMagvar(String s){
        Double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= -360.0 && d <= 360.0;
    }

    static public boolean waypointRegion(String s){
        return s.matches("^[a-zA-Z0-9]{2}$");
    }

    static public boolean waypointIdent(String s){
        return s.length() <= 5;
    }


    /***
     *   Route
     */

    static public boolean routeType(String s){
        switch (s) {
            case "VICTOR":
            case "JET":
            case "BOTH":
                return true;
        }
        return false;
    }

    static public boolean routeName(String s){
        return s.length() <= 8;
    }

    /***
     *   Previous
     */

    static public boolean previousType(String s)
    {
        return waypointType(s);
    }

    static public boolean previousRegion(String s)
    {
        return waypointRegion(s);
    }

    static public boolean previousIdent(String s)
    {
        return waypointIdent(s);
    }

    static public boolean previousAltitudeMinimum(String s)
    {
        try{
            double d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return true;
    }


    /***
     *   Next
     */

    static public boolean nextType(String s)
    {
        return waypointType(s);
    }

    static public boolean nextRegion(String s)
    {
        return waypointRegion(s);
    }

    static public boolean nextIdent(String s)
    {
        return waypointIdent(s);
    }

    static public boolean nextAltitudeMinimum(String s)
    {
        return previousAltitudeMinimum(s);
    }

    /***
     *   Approach
     */

    static public boolean approachType(String s){
        switch (s) {
            case "GPS":
            case "ILS":
            case "LDA":
            case "LOCALIZER":
            case "LOCALIZER_BACKCOURSE":
            case "NDB":
            case "NDBDME":
            case "RNAV":
            case "SDF":
            case "VOR":
            case "VORDME":
                return true;
        }
        return false;
    }

    static public boolean approachRunway(String s){
        switch(s) {
            case "EAST":
            case "NORTH":
            case "NORTHEAST":
            case "NORTHWEST":
            case "SOUTH":
            case "SOUTHEAST":
            case "SOUTHWEST":
            case "WEST":
                return true;
        }

        if(s.matches("^0[0-9]$"))
            return true;

        Double d;

        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 36;

    }

    static public boolean approachDesignator(String s){
        switch(s){
            case "NONE":
            case "C":
            case "CENTER":
            case "L":
            case "LEFT":
            case "R":
            case "RIGHT":
            case "W":
            case "WATER":
            case "A":
            case "B":
                return true;
        }

        return false;
    }


    static public boolean approachSuffix(String s){
        return s.length() == 1;
    }

    static public boolean approachGPSOverlay(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean approachFixType(String s){
        switch(s){
            case "NDB":
            case "TERMINAL_NDB":
            case "TERMINAL_WAYPOINT":
            case "VOR":
            case "WAYPOINT":
            case "RUNWAY":
                return true;
        }
        return false;
    }

    static public boolean approachFixRegion(String s){
        return s.length() <= 2;
    }

    static public boolean approachFixIdent(String s){
        return s.length() <= 5;
    }

    static public boolean approachAltitude(String s){
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[M]?$");
    }

    static public boolean approachHeading(String s){
        double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= -0.0 && d <= 360.0;
    }

    static public boolean approachMissedAltitude(String s){
        return approachAltitude(s);
    }

    /***
     *   ApproachLegs
     */

    /***
     *   MissedApproachLegs
     */

    /***
     *   Leg
     */

    static public boolean legType(String s){

        switch(s){
            case "AF":
            case "CA":
            case "CD":
            case "CF":
            case "CI":
            case "CR":
            case "DF":
            case "FA":
            case "FC":
            case "FD":
            case "FM":
            case "HA":
            case "HF":
            case "HM":
            case "IF":
            case "PI":
            case "RF":
            case "TF":
            case "VA":
            case "VD":
            case "VI":
            case "VM":
            case "VR":
                return true;
        }

        return false;
    }

    static public boolean legFixType(String s)
    {
        return approachFixType(s);
    }

    static public boolean legFixRegion(String s)
    {
        return approachFixRegion(s);
    }

    static public boolean legFixIdent(String s)
    {
        return approachFixIdent(s);
    }

    static public boolean legFlyOver(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean legTurnDirection(String s){
        switch(s){
            case "L":
            case "R":
            case "E":
                return true;
        }

        return false;
    }

    static public boolean legRecommendedType(String s){
        switch(s){
            case "NDB":
            case "TERMINAL_NDB":
            case "TERMINAL_WAYPOINT":
            case "VOR":
            case "WAYPOINT":
            case "RUNWAY":
            case "LOCALIZER":
                return true;
        }
        return false;
    }

    static public boolean legRecommendedRegion(String s)
    {
        return approachFixRegion(s);
    }


    static public boolean legRecommendedIdent(String s)
    {
        return approachFixIdent(s);
    }

    static public boolean legTheta(String s){
        double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= -0.0 && d <= 360.0;
    }

    static public boolean legRho(String s){
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[MN]?$");
    }

    static public boolean legTrueCourse(String s){
        double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= -0.0 && d <= 360.0;
    }

    static public boolean legMagneticCourse(String s){

        double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= -0.0 && d <= 360.0;
    }

    static public boolean legDistance(String s){
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[MN]?$");
    }

    static public boolean legTime(String s){
        double d;
        try{
            d = Double.parseDouble(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= 0;
    }

    static public boolean legAltitudeDescriptor(String s){
        switch(s)
        {
            case "+":
            case "-":
            case " ":
            case "A":
            case "B":
            case "C":
            case "G":
            case "H":
            case "I":
            case "J":
            case "V":
                return true;
        }

        return false;
    }

    static public boolean legAltitude1(String s){

        return approachAltitude(s);
    }

    static public boolean legAltitude2(String s){

        return approachAltitude(s);
    }



    /***
     *   Transition
     */

    static public boolean transitionType(String s){

        switch(s)
        {
            case "DME":
            case "FULL":
                return true;
        }

        return false;
    }

    static public boolean transitionFixType(String s){

        return approachFixType(s);
    }

    static public boolean transitionFixRegion(String s){

        return approachFixRegion(s);
    }

    static public boolean transitionFixIdent(String s){

        return approachFixIdent(s);
    }

    static public boolean transitionAltitude(String s){  // may be suffixed by f or m to designate meters

        return approachAltitude(s);
    }

    /***
     *   TransitionLegs
     */



    /***
     *   Helipad
     */
    static public boolean helipadAltitude(String s) {
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[MF]?$");
    }



    static public boolean helipadSurface(String s){
        switch(s)
        {
            case "ASPHALT":
            case "BITUMINOUS":
            case "BRICK":
            case "CLAY":
            case "CEMENT":
            case "CONCRETE":
            case "CORAL":
            case "DIRT":
            case "GRASS":
            case "GRAVEL":
            case "ICE":
            case "MACADAM":
            case "OIL_TREATED, PLANKS":
            case "SAND":
            case "SHALE":
            case "SNOW":
            case "STEEL_MATS":
            case "TARMAC":
            case "UNKNOWN":
            case "WATER":
                return true;
        }
        return false;
    }


    static public boolean helipadHeading(String s){
        return approachHeading(s);
    }

    static public boolean helipadLength(String s){
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[MF]?$");
    }

    static public boolean helipadWidth(String s){
        return s.matches("^(([0-9]*\\.?[0-9]+)|([0-9]+\\.?[0-9]*))[MF]?$");
    }

    static public boolean helipadType(String s){
        switch(s){
            case "NONE":
            case "CIRCLE":
            case "H":
            case "MEDICAL":
            case "SQUARE":
                return true;
        }

        return false;
    }

    static public boolean helipadClosed(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }

    static public boolean helipadTransparent(String s){
        switch(s){
            case "TRUE":
            case "FALSE":
                return true;
        }

        return false;
    }


    static public boolean helipadRed(String s){
        int d;
        try{
            d = Integer.parseInt(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 255;
    }

    static public boolean helipadGreen(String s){
        int d;
        try{
            d = Integer.parseInt(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 255;
    }

    static public boolean helipadBlue(String s){
        int d;
        try{
            d = Integer.parseInt(s);
        }
        catch(Exception e)
        {
            return false;
        }

        return d >= 0 && d <= 255;
    }


    /***
     *   Start
     */

    static public boolean startType(String s){
        return s.equals("RUNWAY") ||  s.equals("HELIPAD") || s.equals("WATER");
    }

    static public boolean startLatitude(String s){
        return latitude(s);
    }

    static public boolean startLongitude(String s){
        return longitude(s);
    }

    static public boolean startAltitude(String s){
        return helipadAltitude(s);
    }

    static public boolean startHeading(String s){
        return approachHeading(s);
    }

    static public boolean startNumber(String s){
        return approachRunway(s);
    }

    static public boolean startDesignator(String s){
        return approachDesignator(s);
    }

}
