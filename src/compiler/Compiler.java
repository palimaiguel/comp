package compiler;

import gen.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import sdl.SDLFile;

import java.io.IOException;

/**
 * Created by Jo�o on 12/04/2015.
 */
public class Compiler
{
    private SemanticListener semanticListener;

    public Compiler(String filename, String destinationFilename) throws IOException
    {
        // Create char stream:
        CharStream charStream = new ANTLRFileStream(filename);

        // Create lexer:
        AirportLexer airportLexer = new AirportLexer(charStream);

        // Create tokens:
        CommonTokenStream tokens = new CommonTokenStream(airportLexer);

        // Create error handler:
        LexicalSyntacticErrorListener errorStrategy = new LexicalSyntacticErrorListener();
        airportLexer.addErrorListener(errorStrategy);

        // Create parser:
        AirportParser airportParser = new AirportParser(tokens);
        airportParser.addErrorListener(errorStrategy);

        System.out.println("Lexical and Syntactic analysis:");

        // Create tree:
        ParserRuleContext tree = airportParser.s();

        // Create standard walk:
        ParseTreeWalker walker = new ParseTreeWalker();
        semanticListener = new SemanticListener();

        System.err.flush();

        if (errorStrategy.getError())
        {
            System.out.println("Lexical/syntactic errors detected! Aborting SDL file generation!!!");
            return;
        }

        System.out.println("\nSemantic analysis:");

        // Walk the code:
        try
        {
            // Analyze semantics:
            walker.walk(semanticListener, tree);
            SemanticAnalyzer semantic = new SemanticAnalyzer();
            semantic.analyzeSemantics(semanticListener);

            if(ErrorPrinter.getError())
            {
                System.err.println("Semantic errors detected! Aborting SDL file generation!!!");
                return;
            }

            System.err.flush();
            System.out.println("\nGenerating SDL code:");

            // Make the transition to the SDL filetype:
            SDLFile sdlFile = new SDLFile(semanticListener);
            sdlFile.writeToFile(destinationFilename);
        }
        catch(Exception e)
        {
            System.out.println("Unknown message: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public SemanticListener getSemanticListener()
    {
        return this.semanticListener;
    }

    public static void main(String[] args) throws IOException
    {
        System.out.println("Palimaiguel BGL2SDL compiler\n\n");

        if(args.length != 2)
        {
            System.out.println("Usage: <filename to parse> <destination filename>");
            return;
        }

        // Parse arguments:
        String filename = args[0];
        String destination = args[1];

        Compiler compiler = new Compiler(filename, destination);
    }
}
