lexer grammar Attributes;

// Other attributes:
ATTR_VERSION: 'version';
ATTR_XMLNS: 'xmlns:xsi';
ATTR_XSI: 'xsi:noNamespaceSchemaLocation';

// **** Common attributes: ****
ATTR_LAT: 'lat';
ATTR_LON: 'lon';
ATTR_ALT: 'alt';
ATTR_MAGVAR: 'magvar';
ATTR_NAME: 'name';
ATTR_TYPE: 'type';
ATTR_HEADING: 'heading';
ATTR_LENGTH: 'length';
ATTR_WIDTH: 'width';
ATTR_NUMBER: 'number';
ATTR_DESIGNATOR: 'designator';
ATTR_END: 'end';
ATTR_FIX_TYPE: 'fixType';
ATTR_FIX_REGION: 'fixRegion';
ATTR_FIX_IDENT: 'fixIdent';
ATTR_DISTANCE: 'distance';
ATTR_RANGE: 'range';
ATTR_IDENT: 'ident';
ATTR_PITCH: 'pitch';
ATTR_IMAGE_COMPLEXITY: 'imageComplexity';
ATTR_INSTANCE_ID: 'instanceId';
ATTR_RED: 'red';
ATTR_GREEN: 'green';
ATTR_BLUE: 'blue';
ATTR_BANK: 'bank';
ATTR_PROFILE: 'profile';
ATTR_INDEX: 'index';
ATTR_DRAW_SURFACE: 'drawSurface';
ATTR_DRAW_DETAIL: 'drawDetail';
ATTR_SURFACE: 'surface';
ATTR_RUNWAY: 'runway';

// Airport attributes
ATTR_REGION: 'region';
ATTR_COUNTRY: 'country';
ATTR_STATE: 'state';
ATTR_CITY: 'city';
ATTR_TEST_RADIUS: 'airportTestRadius';
ATTR_TRAFFIC_SCALAR: 'trafficScalar';

// TaxiwayPoint attributes:
ATTR_ORIENTATION: 'orientation';

// TaxiwayParking attributes:
ATTR_RADIUS: 'radius';
ATTR_AIRLINE_CODES: 'airlineCodes';
ATTR_PUSH_BACK: 'pushBack';
ATTR_TEE_OFFSET_1: 'teeOffset1';
ATTR_TEE_OFFSET_2: 'teeOffset2';
ATTR_TEE_OFFSET_3: 'teeOffset3';
ATTR_TEE_OFFSET_4: 'teeOffset4';

// TaxiwayName attributes:
// TaxiwayPath attributes:
ATTR_START: 'start';
ATTR_WEIGHT_LIMIT: 'weightLimit';
ATTR_CENTER_LINE: 'centerLine';
ATTR_CENTER_LINE_LIGHTED: 'centerLineLighted';
ATTR_LEFT_EDGE: 'leftEdge';
ATTR_LEFT_EDGE_LIGHTED: 'leftEdgeLighted';
ATTR_RIGHT_EDGE: 'rightEdge';
ATTR_RIGHT_EDGE_LIGHTED: 'rightEdgeLighted';


// ************************** Other tags attributes ************

// Fuel attributes
ATTR_AVAILABILITY: 'availability';

// Com attributes
ATTR_FREQUENCY: 'frequency';

// Runway attributes
ATTR_PRIMARY_DESIGNATOR: 'primaryDesignator';
ATTR_SECONDARY_DESIGNATOR: 'secondaryDesignator';
ATTR_PATTERN_ALTITUDE: 'patternAltitude';
ATTR_PRIMARY_TAKEOFF: 'primaryTakeoff';
ATTR_PRIMARY_LANDING: 'primaryLanding';
ATTR_PRIMARY_PATTERN: 'primaryPattern';
ATTR_SECONDARY_TAKEOFF: 'secondaryTakeoff';
ATTR_SECONDARY_LANDING: 'secondaryLanding';
ATTR_SECONDARY_PATTERN: 'secondaryPattern';
ATTR_PRIMARY_MARKING_BIAS: 'primaryMarkingBias';
ATTR_SECONDARY_MARKING_BIAS: 'secondaryMarkingBias';

// Markings attributes:
ATTR_FIXED_DISTANCE: 'fixedDistance';
ATTR_ALTERNATE_THRESHOLD: 'alternateThreshold';
ATTR_ALTERNATE_TOUCHDOWN: 'alternateTouchdown';
ATTR_ALTERNATE_FIXED_DISTANCE: 'alternateFixedDistance';
ATTR_ALTERNATE_PRECISION: 'alternatePrecision';
ATTR_LEADING_ZERO_IDENT: 'leadingZeroIdent';
ATTR_NO_THRESHOLD_END_ARROWS: 'noThresholdEndArrows';
ATTR_EDGES: 'edges';
ATTR_THRESHOLD: 'threshold';
ATTR_FIXED: 'fixed';
ATTR_TOUCHDOWN: 'touchdown';
ATTR_DASHES: 'dashes';
ATTR_PRECISION: 'precision';
ATTR_EDGE_PAVEMENT: 'edgePavement';
ATTR_SINGLE_END: 'singleEnd';
ATTR_PRIMARY_CLOSED: 'primaryClosed';
ATTR_SECONDARY_CLOSED: 'secondaryClosed';
ATTR_PRIMARY_STOL: 'primaryStol';
ATTR_SECONDARY_STOL: 'secondaryStol';

// Lights attributes:
ATTR_CENTER: 'center';
ATTR_EDGE: 'edge';
ATTR_CENTER_RED: 'centerRed';

// OffsetThreshold attributes:
// Blastpad attributes:
// Overrun attributes:
// ApproachLights attributes:
ATTR_SYSTEM: 'system';
ATTR_STROBES: 'strobes';
ATTR_REIL: 'reil';
ATTR_END_LIGHTS: 'endLights';

// Vasi attribues:
ATTR_SIDE: 'side';
ATTR_SPACING: 'spacing';

// ILS attributes:
ATTR_BACKCOURSE: 'backCourse';

// GlideSlope attributes:
// DME attributes:
// VisualModel attributes:
// RunwayStart attributes:
// RunwayAlias attributes:
// Apron attributes:
// Vertex attributes:
// ApronEdgeLights attributes:
// EdgeLights attributes:
// Taxiwaysign attributes:
ATTR_LABEL: 'label';
ATTR_SIZE: 'size';
ATTR_JUSTIFICATION: 'justification';

// Waypoint attributes:
ATTR_WAYPOINT_TYPE: 'waypointType';
ATTR_WAYPOINT_REGION: 'waypointRegion';
ATTR_WAYPOINT_INDENT: 'waypointIdent';

// Route attributes:
ATTR_ROUTE_TYPE: 'routeType';

// Previous & NEXT attributes:
ATTR_ALTITUDE_MINIMUM: 'altitudeMinimum';

// Approach attributes:
ATTR_SUFFIX: 'suffix';
ATTR_GPS_OVERLAY: 'gpsOverlay';
ATTR_MISSED_ALTITUDE: 'missedAltitude';

// ApproachLegs attributes:
// Leg attributes:
ATTR_FLY_OVER: 'flyOver';
ATTR_TURN_DIRECTION: 'turnDirection';
ATTR_RECOMMENDED_TYPE: 'recommendedType';
ATTR_RECOMMENDED_REGION: 'recommendedRegion';
ATTR_RECOMMENDED_IDENT: 'recommendedIdent';
ATTR_THETA: 'theta';
ATTR_RHO: 'rho';
ATTR_TRUE_COURSE: 'trueCourse';
ATTR_MAGNETIC_COURSE: 'magneticCourse';
ATTR_TIME: 'time';
ATTR_ALTITUDE_DESCRIPTOR: 'altitudeDescriptor';
ATTR_ALTITUDE1: 'altitude1';
ATTR_ALTITUDE2: 'altitude2';

// MissedApproachLegs attributes:
// Transition attributes:
ATTR_TRANSITION_TYPE: 'transitionType';
ATTR_ALTITUDE: 'altitude';

// DMEARC attributes:
ATTR_RADIAL: 'radial';
ATTR_DME_REGION: 'dmeRegion';
ATTR_DME_IDENT: 'dmeIdent';

// BiasXYZ attributes:
ATTR_BIAS_X: 'biasX';
ATTR_BIAS_Y: 'biasY';
ATTR_BIAS_Z: 'biasZ';

// Helipad attributes:
ATTR_CLOSED: 'closed';
ATTR_TRANSPARENT: 'transparent';

// Start attributes:
// Jetway attributes:
ATTR_GATE_NAME: 'gateName';
ATTR_PARKING_NUMBER: 'parkingNumber';

// SceneryObject attributes:
ATTR_ALTITUDE_IS_AGL: 'altitudeIsAgl';

// Beacon attributes:
ATTR_BASE_TYPE: 'baseType';

// Effect attributes:
ATTR_EFFECT_NAME: 'effectName';
ATTR_EFFECT_PARAMS: 'effectParams';

// ******* Buildings **********

// GenericBuilding attributes:
ATTR_SCALE: 'scale';
ATTR_BOTTOM_TEXTURE: 'bottomTexture';
ATTR_ROOF_TEXTURE: 'roofTexture';
ATTR_TOP_TEXTURE: 'topTexture';
ATTR_WINDOW_TEXTURE: 'windowTexture';

// RectangularBuilding Flat Roof (base RectangularBuilding):
ATTR_ROOF_TYPE: 'roofType';
ATTR_SIZE_X: 'sizeX';
ATTR_SIZE_Z: 'sizeZ';
ATTR_SIZE_BOTTOM_Y: 'sizeBottomY';
ATTR_TEXTURE_INDEX_BOTTOM_X: 'textureIndexBottomX';
ATTR_TEXTURE_INDEX_BOTTOM_Z: 'textureIndexBottomZ';
ATTR_SIZE_WINDOW_Y: 'sizeWindowY';
ATTR_TEXTURE_INDEX_WINDOW_X: 'textureIndexWindowX';
ATTR_TEXTURE_INDEX_WINDOW_Y: 'textureIndexWindowY';
ATTR_TEXTURE_INDEX_WINDOW_Z: 'textureIndexWindowZ';
ATTR_SIZE_TOP_Y: 'sizeTopY';
ATTR_TEXTURE_INDEX_TOP_X: 'textureIndexTopX';
ATTR_TEXTURE_INDEX_TOP_Z: 'textureIndexTopZ';
ATTR_TEXTURE_INDEX_ROOF_X: 'textureIndexRoofX';
ATTR_TEXTURE_INDEX_ROOF_Z: 'textureIndexRoofZ'; // Used for MultiSidedBuilding too

// RectangularBuilding Aditional attributes:
ATTR_SIZE_ROOF_Y: 'sizeRoofY';
ATTR_TEXTURE_INDEX_ROOF_Y: 'textureIndexRoofY';
ATTR_GABLE_TEXTURE: 'gableTexture';
ATTR_TEXTURE_INDEX_GABLE_Y: 'textureIndexGableY';
ATTR_TEXTURE_INDEX_GABLE_Z: 'textureIndexGableZ';
ATTR_FACE_TEXTURE: 'faceTexture';
ATTR_TEXTURE_INDEX_FACE_X: 'textureIndexFaceX';
ATTR_TEXTURE_INDEX_FACE_Y: 'textureIndexFaceY';

// PyramidalBuilding attributes:
ATTR_SIZE_TOP_X: 'sizeTopX';
ATTR_SIZE_TOP_Z: 'sizeTopZ';

// MultiSidedBuilding attributes:
ATTR_BUILDING_SIDES: 'buildingSides';
ATTR_SMOOTHING: 'smoothing';

// ******** Ending buildings **********

// Library object attributes:
// Trigger attributes:
ATTR_TRIGGER_HEIGHT: 'triggerHeight';

// Trigger weather attributes:
ATTR_SCALAR: 'scalar';

// Windsock attributes:
ATTR_POLE_HEIGHT: 'poleHeight';
ATTR_SOCK_LENGTH: 'sockLength';
ATTR_LIGHTED: 'lighted';

// PoleColor attributes:
// SockColor attributes:

// AttachedObject:
ATTR_ATTACHPOINT_NAME: 'attachpointName';

// RandomAttach attributes:
ATTR_RANDOMNESS: 'randomness';
ATTR_PROBABILITY: 'probability';

// BlastFence attributes:
// BoundaryFence attributes:
// DeleteAirportAttributes:
ATTR_DELETE_ALL_APPROACHES: 'deleteAllApproaches';
ATTR_DELETE_ALL_APRON_LIGHTS: 'deleteAllApronLights';
ATTR_DELETE_ALL_APRONS: 'deleteAllAprons';
ATTR_DELETE_ALL_FREQUENCIES: 'deleteAllFrequencies';
ATTR_DELETE_ALL_HELIPADS: 'deleteAllHelipads';
ATTR_DELETE_ALL_RUNWAYS: 'deleteAllRunways';
ATTR_DELETE_ALL_STARTS: 'deleteAllStarts';
ATTR_DELETE_ALL_TAXIWAYS: 'deleteAllTaxiways';
ATTR_DELETE_ALL_BLAST_FENCES: 'deleteAllBlastFences';
ATTR_DELETE_ALL_BOUNDARY_FENCES: 'deleteAllBoundaryFences';
ATTR_DELETE_ALL_CONTROL_TOWERS: 'deleteAllControlTowers';
ATTR_DELETE_ALL_JETWAYS: 'deleteAllJetways';


// AIRPORT_ATTR : ll | lsdald | ldaslda;