package sdl;

import bgl.node.AirportNode;
import bgl.node.Node;
import bgl.node.TaxiwayparkingNode;
import org.jdom2.Element;
import sdl.graph.ParkingPoint;
import sdl.graph.TaxiPoint;
import sdl.graph.TaxiwayGraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParkingSpaces {

    AirportNode airportNode;
    List<Node> taxiwayParkingListNode;


    public ParkingSpaces(AirportNode airportNode) {
        this.airportNode = airportNode;
        this.taxiwayParkingListNode = getTaxiwayParkingListNode();
    }

    public void writeToFather(Element airport) {


        Element parkingSpaces = new Element("parkingSpaces");
        if (taxiwayParkingListNode == null || taxiwayParkingListNode.size() == 0) {
            airport.addContent(parkingSpaces);
            return;
        }

        for (int i = 0; i < taxiwayParkingListNode.size(); i++) {
            Node node = taxiwayParkingListNode.get(i);
            Element parking = new Element("parking");
            processParking(parking, node, i);
            parkingSpaces.addContent(parking);
        }
        airport.addContent(parkingSpaces);
    }

    private void processParking(Element parking, Node node, int i) {

        parking.setAttribute("parkingType", "Medium");
        int num = i+1;
        String str = "p" + String.format("%02d", num);
        parking.setAttribute("id", str);
        parking.addContent(elementDesignation("Parking " + str));
        parking.addContent(elementDescription());
        parking.addContent(elementAirlines(node));
        parking.addContent(elementCoordinates(node));
        parking.addContent(elementRadius(node));
        elementConnectsToTaxiway(node, parking);
    }
    private Element elementDesignation(String label) {
        Element e = new Element("designation");
        e.addContent(label);
        return e;
    }
    private Element elementDescription() {
        Element e = new Element("description");
        e.addContent("XXX description XXX");
        return e;
    }
    private Element elementAirlines(Node node) {
        Element e = new Element("airlines");

        String airlines = node.getAttribute("airlineCodes");

        if (airlines == null)
            return e;

        String[] strArray = airlines.split(",");

        for(String str : strArray)
        {
            str = str.trim();
            Element elem = new Element("airlineCode");
            elem.addContent(str);
            e.addContent(elem);
        }

        return e;
    }
    private Element elementCoordinates(Node node) {
        Element coordinates = new Element("coordinates");
        Coordinates c = new Coordinates(node);
        coordinates.addContent(c.elementLatitude());
        coordinates.addContent(c.elementLongitude());
        coordinates.addContent(c.elementAltitude());
        return coordinates;
    }
    private Element elementRadius(Node node) {
        Element e = new Element("radius");

        String value = node.getAttribute("radius");
        //check attribute unit
        String lastChar;
        if(value.length() > 1) {
            lastChar = value.substring(value.length() - 1);

            if (lastChar.equals("F") || lastChar.equals("f"))
                e.setAttribute("lengthUnit", "Foot");
            else
                e.setAttribute("lengthUnit", "Meter");
        }
        else{
            e.setAttribute("lengthUnit", "Meter");
        }

        e.addContent(value);
        return e;
    }
    private void elementConnectsToTaxiway(Node node, Element parking) {

        HashMap<String, TaxiPoint> xways = ((TaxiwayparkingNode) node).getConnectsToTaxiways();

        for (Map.Entry<String, TaxiPoint> entry : xways.entrySet())
        {
            String xwayID = entry.getKey();
            TaxiPoint taxiwayPoint = entry.getValue();

            Element connectsToTaxiway = new Element("connectsToTaxiway");
            connectsToTaxiway.setAttribute("xway", xwayID);
            Element coordinates = new Element("coordinates");

            Coordinates c = new Coordinates(node);
            coordinates.addContent(c.elementLatitude(taxiwayPoint.getLatitude()));
            coordinates.addContent(c.elementLongitude(taxiwayPoint.getLongitude()));
            coordinates.addContent(c.elementAltitude(taxiwayPoint.getAltitude()));

            connectsToTaxiway.addContent(coordinates);
            parking.addContent(connectsToTaxiway);
        }
    }

    public List<Node> getTaxiwayParkingListNode() {

        return airportNode.getChildren("TaxiwayParking");
    }
}
