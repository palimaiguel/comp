package sdl;

import bgl.node.AirportNode;
import bgl.node.Node;
import org.jdom2.Element;

import java.util.List;

public class UtilitiesTag {

    AirportNode airportNode;

    public UtilitiesTag(AirportNode airport) {
        this.airportNode = airport;
    }

    public void writeToFather(Element airport) {
        List<Node> towerList = airportNode.getChildren("Tower");

        Element utilities = new Element("utilities");
        if(towerList == null) {
            airport.addContent(utilities);
            return;
        }

        for(int i = 0; i < towerList.size(); i++){
            Node node = towerList.get(i);
            Element tower = new Element("tower");
            processTower(tower, node, i + 1);
            utilities.addContent(tower);
        }
        airport.addContent(utilities);

    }
    private void processTower(Element tower, Node node, int i) {
        tower.setAttribute("id", "u" + i);

        tower.addContent(elementDesignation());
        tower.addContent(elementCoordinates(node));
        tower.addContent(elementRadius());
        tower.addContent(elementHeight());
    }
    private Element elementDesignation() {
        Element e = new Element("designation");
        e.addContent("Tower");
        return e;
    }
    private Element elementCoordinates(Node node) {
        Element coordinates = new Element("coordinates");
        Coordinates c = new Coordinates(node);
        coordinates.addContent(c.elementLatitude());
        coordinates.addContent(c.elementLongitude());
        coordinates.addContent(c.elementAltitude());
        return coordinates;
    }
    private Element elementRadius() {
        Element e = new Element("radius");
        e.setAttribute("lengthUnit","Meter");
        e.addContent("0");
        return e;
    }
    private Element elementHeight() {
        Element e = new Element("height");
        e.setAttribute("lengthUnit","Meter");
        e.addContent("0");
        return e;
    }
}
