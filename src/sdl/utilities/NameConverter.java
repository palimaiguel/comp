package sdl.utilities;

public class NameConverter
{
    public static String toPascalCase(String value)
    {
        int length = value.length();

        if(length == 0)
            return "";

        StringBuilder builder = new StringBuilder(length);
        builder.append(Character.toUpperCase(value.charAt(0)));

        for(int i = 1; i < length; i++)
            builder.append(Character.toLowerCase(value.charAt(i)));

        return builder.toString();
    }
}
