package sdl;

import bgl.node.AirportNode;
import bgl.node.Node;
import org.jdom2.Element;
import sdl.graph.TaxiPoint;
import sdl.graph.TaxiwayGraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Taxiways {

    AirportNode airportNode;
    TreeMap<String, TaxiwayGraph> taxiwaysGraphList;

    public Taxiways(AirportNode airportNode, TreeMap<String, TaxiwayGraph> taxiwaysGraphList) {
        this.airportNode = airportNode;
        this.taxiwaysGraphList = taxiwaysGraphList;
    }

    public void writeToFather(Element airport) {

        Element taxiways = new Element("taxiways");
        if(taxiwaysGraphList == null || taxiwaysGraphList.size() == 0) {
            airport.addContent(taxiways);
            return;
        }

        for (Map.Entry<String, TaxiwayGraph> entry : taxiwaysGraphList.entrySet()) {
            String key = entry.getKey();
            TaxiwayGraph taxiwayGraph = entry.getValue();

            Element taxiway = new Element("taxiway");
            processTaxiway(taxiway, taxiwayGraph, key);
            taxiways.addContent(taxiway);
        }
        airport.addContent(taxiways);
    }

    private void processTaxiway(Element taxiway, TaxiwayGraph taxiwayGraph, String id) {

        taxiway.setAttribute("id", taxiwayGraph.getID());
        String label = "Taxiway";
        taxiway.addContent(elementDesignation(label));
        taxiway.addContent(elementSurface(taxiwayGraph));
        taxiway.addContent(elementWidth(taxiwayGraph));

        Path path = new Path(taxiwayGraph);
        path.writeToFather(taxiway);
    }

    private Element elementDesignation(String label) {
        Element e = new Element("designation");
        e.addContent(label);
        return e;
    }
    private Element elementWidth(TaxiwayGraph tg) {
        Element width = new Element("width");

        //check attribute unit
        String value = tg.getWidth();
        String lastChar;

        if(value.length() > 1) {
            lastChar = value.substring(value.length() - 1);

            if (lastChar.equals("F") || lastChar.equals("f"))
                width.setAttribute("lengthUnit", "Foot");
            else
                width.setAttribute("lengthUnit", "Meter");
        }
        else{
            width.setAttribute("lengthUnit", "Meter");
        }
        width.addContent(value);
        return width;
    }
    private Element elementSurface(TaxiwayGraph tg) {
        Element surface = new Element("surface");
        surface.addContent(tg.getSurface());
        return surface;
    }
}
