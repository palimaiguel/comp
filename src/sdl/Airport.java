package sdl;

import bgl.node.AirportNode;
import org.jdom2.Element;
import sdl.graph.ParkingPoint;
import sdl.graph.RunwayGraph;
import sdl.graph.TaxiPoint;
import sdl.graph.TaxiwayGraph;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class Airport {

    // input structures
    AirportNode airportNode;

    //graph structures
    TreeMap<String, RunwayGraph> runwayGraphList;
    TreeMap<String, TaxiwayGraph> taxiwayGraphList;

    //elements to print in final format
    Element name;
    ContactPerson contactPerson;
    Location location;
    Runways runways;
    Helipads helipads;
    Taxiways taxiways;
    ParkingSpaces parkingSpaces;
    UtilitiesTag utilities;

    public Airport(AirportNode airport, Element name, ContactPerson contactPerson, Location location) {
        this.airportNode = airport;
        this.name = name;
        this.location = location;
        this.contactPerson = contactPerson;

        //do graph calculations
        GraphBuilder graphBuilder = new GraphBuilder(airport);
        runwayGraphList = graphBuilder.getRunwayGraphs();
        taxiwayGraphList = graphBuilder.getTaxiwayGraphs();

        runways = new Runways(airport, runwayGraphList);
        helipads = new Helipads(airport);
        taxiways = new Taxiways(airport, taxiwayGraphList);
        parkingSpaces = new ParkingSpaces(airport);
        utilities = new UtilitiesTag(airport);

    }

    public void writeToFather(Element baseOfOperations) {
        Element airport = new Element("airport");

        //airportNode tag
        airport.addContent(name);
        airport.addContent(elementDescription());
        contactPerson.writeToFather(airport);
        location.writeToFather(airport);
        airport.addContent(elementICAO());
        airport.addContent(elementIATA());
        airport.addContent(elementMagVar());

        runways.writeToFather(airport);
        helipads.writeToFather(airport);
        taxiways.writeToFather(airport);
        parkingSpaces.writeToFather(airport);

        Element hangars = new Element("hangars");
        airport.addContent(hangars);

        utilities.writeToFather(airport);

        baseOfOperations.addContent(airport);
    }

    private Element elementDescription() {
        Element description = new Element("description");
        description.addContent("XXX Description XXX");
        return description;
    }
    private Element elementICAO() {
        Element icao = new Element("ICAO");
        icao.addContent(airportNode.getAttribute("ident"));
        return icao;
    }
    private Element elementIATA() {

        //TODO get iata from icao
        Element iata = new Element("IATA");
        iata.addContent(airportNode.getAttribute("ident"));
        return iata;
    }
    private Element elementMagVar() {
        Element magVar = new Element("magVar");
        magVar.addContent(airportNode.getAttribute("magvar"));
        return magVar;
    }
}
