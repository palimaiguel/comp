package sdl.graph;

import java.util.ArrayList;
import java.util.List;

public class RunwayGraph
{
    private List<RunwayPoint> m_points = new ArrayList<>();
    private String m_ID;

    public RunwayGraph(String id)
    {
        m_ID = id;
    }

    public String getID()
    {
        return m_ID;
    }

    public List<RunwayPoint> getPoints()
    {
        return m_points;
    }

    public void addLink(RunwayPoint runwayPointStart, RunwayPoint runwayPointEnd)
    {
        if (m_points.isEmpty())
        {
            m_points.add(runwayPointStart);
            m_points.add(runwayPointEnd);
        }
        else
        {
            if (runwayPointEnd.getIndex().equals(m_points.get(0).getIndex()))
                m_points.add(0, runwayPointStart);
            else
                m_points.add(runwayPointEnd);
        }
    }

    /*public void addNode(TaxiwaypointNode node)
    {
        m_nodes.add(node);
    }*/
}

