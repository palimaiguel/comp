package sdl.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miguel on 09-06-2015.
 */
public class RunwayPoint
{
    private String m_index;

    private String m_longitude;
    private String m_latitude;
    private String m_altitude = "0";
    private List<String> m_taxiwaysConnected = new ArrayList<>();

    public void setIndex(String index)
    {
        m_index = index;
    }

    public void setLongitude(String longitude)
    {
        m_longitude = longitude;
    }

    public void setLatitude(String latitude)
    {
        m_latitude = latitude;
    }

    public void addTaxiwayConnected(String id)
    {
        m_taxiwaysConnected.add(id);
    }

    public String getIndex()
    {
        return m_index;
    }

    public String getLongitude()
    {
        return m_longitude;
    }

    public String getLatitude()
    {
        return m_latitude;
    }

    public String getAltitude()
    {
        return m_altitude;
    }

    public List<String> getTaxiwaysConnected()
    {
        return m_taxiwaysConnected;
    }
}
