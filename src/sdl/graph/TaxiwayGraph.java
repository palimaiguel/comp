package sdl.graph;

import bgl.node.Node;
import bgl.node.TaxiwaypointNode;

import java.util.ArrayList;
import java.util.List;

public class TaxiwayGraph
{
    private String m_ID;

    private List<TaxiPoint> m_points = new ArrayList<>();
    private String m_designation;
    private Double m_width = 0.0;
    private String m_surface;

    public TaxiwayGraph(String identifier, String designation, String surface)
    {
        m_ID = identifier;
        m_designation = designation;
        m_surface = surface;
    }

    public String getID()
    {
        return m_ID;
    }

    public List<TaxiPoint> getPoints()
    {
        return m_points;
    }

    public void addLink(TaxiPoint taxiPointStart, TaxiPoint taxiPointEnd, Double width)
    {
        if (m_points.isEmpty())
        {
            m_points.add(taxiPointStart);
            m_points.add(taxiPointEnd);
        }
        else
        {
            if (taxiPointEnd.getIndex().equals(m_points.get(0).getIndex()))
                m_points.add(0, taxiPointStart);
            else
                m_points.add(taxiPointEnd);
        }

        m_width += width;
    }

    public String getSurface() {
        return m_surface;
    }

    public String getWidth() {
        return m_width.toString();
    }
}
