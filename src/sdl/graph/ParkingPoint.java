package sdl.graph;

import java.util.List;

/**
 * Created by Miguel on 09-06-2015.
 */
public class ParkingPoint
{
    private String m_ID;
    private String m_designation;
    private String m_description = "Usually used for medium sized aircraft";
    private Double m_radius;

    private String m_longitude;
    private String m_latitude;
    private String m_altitude = "0";
    private List<String> m_taxiwaysConnected;

    public ParkingPoint(String ID, String designation, Double radius, String longitude, String latitude)
    {
        m_ID = ID;
        m_designation = designation;
        m_radius = radius;
        m_longitude = longitude;
        m_latitude = latitude;
    }

    public String getID()
    {
        return m_ID;
    }

    public String getDesignation()
    {
        return m_designation;
    }

    public String getDescription()
    {
        return m_description;
    }

    public Double radius()
    {
        return m_radius;
    }

    public String getLongitude()
    {
        return m_longitude;
    }

    public String getLatitude()
    {
        return m_latitude;
    }

    public String getAltitude()
    {
        return m_altitude;
    }

    public List<String> getTaxiwaysConnected()
    {
        return m_taxiwaysConnected;
    }
}
