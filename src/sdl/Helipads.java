package sdl;

import bgl.node.AirportNode;
import bgl.node.Node;
import org.jdom2.Element;

import java.util.List;

/**
 * Created by Paula on 6/8/2015.
 */
public class Helipads {

    AirportNode airportNode;

    public Helipads(AirportNode airportNode) {
        this.airportNode = airportNode;
    }

    public void writeToFather(Element airport) {
        Element e = new Element("helipads");

        List<Node> helipadList = airportNode.getChildren("Helipad");

        if(helipadList == null) {
            airport.addContent(e);
            return;
        }
        for(Node node : helipadList){

            Element helipad = new Element("helipad");
            processHelipad(helipad, node);
            e.addContent(helipad);
        }
        airport.addContent(e);
    }

    private void processHelipad(Element helipad, Node node) {
        helipad.addContent(elementDesignation());
        helipad.addContent(elementSurface(node));
        helipad.addContent(elementCoordinates(node));
        helipad.addContent(elementRadius(node));
    }

    private Element elementDesignation() {
        //TODO
        Element e = new Element("designation");
        e.addContent("XXX designation XXX");
        return e;
    }
    private Element elementSurface(Node node) {
        Element surface = new Element("surface");
        surface.addContent(node.getAttribute("surface"));
        return surface;
    }
    private Element elementCoordinates(Node node) {
        Element coordinates = new Element("coordinates");
        Coordinates c = new Coordinates(node);
        coordinates.addContent(c.elementLatitude());
        coordinates.addContent(c.elementLongitude());
        coordinates.addContent(c.elementAltitude());
        return coordinates;
    }
    private Element elementRadius(Node node) {
        //TODO
        Element e = new Element("radius");
        e.addContent(node.getAttribute("radius"));
        return e;
    }
}
