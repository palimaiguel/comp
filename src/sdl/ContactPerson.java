package sdl;

import org.jdom2.Element;

/**
 * Created by Paula on 6/7/2015.
 */
public class ContactPerson {
    public static void writeToFather(Element baseOfOperations) {
        Element contactPerson = new Element("contactPerson");
        contactPerson.addContent(elementName());
        contactPerson.addContent(elementTitle());
        contactPerson.addContent(elementInstitution());
        contactPerson.addContent(elementPosition());
        contactPerson.addContent(elementAddress());
        contactPerson.addContent(elementZipCode());
        contactPerson.addContent(elementCity());
        contactPerson.addContent(elementStateDistrictRegion());
        contactPerson.addContent(elementCountry());
        contactPerson.addContent(elementTelephone());
        contactPerson.addContent(elementCellphone());
        contactPerson.addContent(elementFax());
        contactPerson.addContent(elementEmail());

        baseOfOperations.addContent(contactPerson);
    }

    private static Element elementName() {
        Element name = new Element("name");
        name.addContent("XXX Name XXX");
        return name;
    }
    private static Element elementTitle() {
        Element title = new Element("title");
        title.addContent("XXX Title XXX");
        return title;
    }
    private static Element elementInstitution() {
        Element institution = new Element("institution");
        institution.addContent("XXX Institution XXX");
        return institution;
    }
    private static Element elementPosition() {
        Element position = new Element("position");
        position.addContent("XXX Position XXX");
        return position;
    }
    private static Element elementAddress() {
        Element address = new Element("address");
        address.addContent("XXX Address XXX");
        return address;
    }
    private static Element elementZipCode() {
        Element zipCode = new Element("zipCode");
        zipCode.addContent("XXX ZipCode XXX");
        return zipCode;
    }
    private static Element elementCity() {
        Element city = new Element("city");
        city.addContent("XXX City XXX");
        return city;
    }
    private static Element elementStateDistrictRegion() {
        Element stateDistrictRegion = new Element("stateDistrictRegion");
        stateDistrictRegion.addContent("XXX State XXX");
        return stateDistrictRegion;
    }
    private static Element elementCountry() {
        Element country = new Element("country");
        country.addContent("XXX Country XXX");
        return country;
    }
    private static Element elementTelephone() {
        Element telephone = new Element("telephone");
        telephone.addContent("XXX Telephone XXX");
        return telephone;
    }
    private static Element elementCellphone() {
        Element cellphone = new Element("cellphone");
        cellphone.addContent("XXX Cellphone XXX");
        return cellphone;
    }
    private static Element elementFax() {
        Element fax = new Element("fax");
        fax.addContent("XXX fax XXX");
        return fax;
    }
    private static Element elementEmail() {
        Element email = new Element("email");
        email.addContent("XXX Email XXX");
        return email;
    }
}
