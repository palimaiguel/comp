package sdl;

import bgl.node.Node;
import org.jdom2.Element;
import sdl.graph.RunwayGraph;
import sdl.graph.RunwayPoint;

import java.util.ArrayList;
import java.util.List;


public class BaseEnd {

    Node node;
    RunwayGraph runwaysGraph;

    String designationBaseEnd;
    String designationReciprocalEnd;

    Point startPoint;
    Point endPoint;


    public BaseEnd(Node node, String runwayId, RunwayGraph runwaysGraph) {

        this.node = node;
        this.runwaysGraph = runwaysGraph;

        String[] designations = runwayId.split("-");

        if(designations.length > 1) {
            designationBaseEnd = runwayId.split("-")[0];
            designationReciprocalEnd = runwayId.split("-")[1];
        }
        else{
            designationBaseEnd = "dbase";
            designationReciprocalEnd = "dreciprocal";
        }
    }

    public void writeToFatherBaseEnd(Element runway) {

        Element end = new Element("baseEnd");

        List<RunwayPoint> pointList = runwaysGraph.getPoints();
        int startPos = 0;
        if(pointList.size() > 0){
            end.addContent(elementDesignationBaseEnd());
            Coordinates coord = new Coordinates(node);
            startPoint = new Point("startpoint", "runway", coord, pointList.get(startPos));
            startPoint.writeToFather(end);
            end.addContent(elementHeading(true));
            endPoint = new Point("endpoint", "runway", coord, pointList.get(pointList.size()-1));
            endPoint.writeToFather(end);
        }
        runway.addContent(end);
    }

    public void writeToFatherReciprocalEnd(Element runway) {
        Element end = new Element("reciprocalEnd");

        end.addContent(elementDesignationReciprocalEnd());

        endPoint.setType("startpoint");
        endPoint.writeToFather(end);

        end.addContent(elementHeading(false));

        startPoint.setType("endpoint");
        startPoint.writeToFather(end);

        runway.addContent(end);
    }

    private Element elementDesignationBaseEnd() {
        Element e = new Element("designation");
        e.addContent(designationBaseEnd);
        return e;
    }

    private Element elementDesignationReciprocalEnd() {
        Element e = new Element("designation");
        e.addContent(designationReciprocalEnd);
        return e;
    }

    private Element elementHeading(boolean baseEnd) {
        Element e = new Element("heading");

        String value = node.getAttribute("heading");
        if(value.equals(""))
            e.setAttribute("headingType","False");
        else
            e.setAttribute("headingType", "True");

        if(!baseEnd){
            try {
                Double valueDouble = Double.parseDouble(value);
                valueDouble = (valueDouble + 180) % 360;
                value = valueDouble.toString();
            }catch (Exception exc){
                value = "";
            }
        }
        e.addContent(value);
        return e;
    }


}