package sdl;

import bgl.node.Node;
import org.jdom2.Element;
import sdl.graph.TaxiPoint;
import sdl.graph.TaxiwayGraph;

import java.util.ArrayList;
import java.util.List;

public class Path {

    TaxiwayGraph taxiwaysGraph;

    public Path(TaxiwayGraph taxiwayGraph) {
        this.taxiwaysGraph = taxiwayGraph;
    }

    public void writeToFather(Element taxiway) {

        Element path = new Element("path");

        List<TaxiPoint> points = taxiwaysGraph.getPoints();
        if(points == null || points.size() == 0){
            return;
        }

        Coordinates coord = new Coordinates();

        for(int i = 0; i < points.size(); i++){
            if(i == 0){
                Point start = new Point("startpoint", "taxiway", coord, points.get(i));
                start.writeToFather(path);
            }
            if(i == points.size()-1){
                Point end = new Point("endpoint", "taxiway", coord, points.get(i));
                end.writeToFather(path);
            }

            if(i != 0 && i != points.size()-1){
                Point mid = new Point("midpoint", "taxiway", coord, points.get(i));
                mid.writeToFather(path);
            }
        }
        taxiway.addContent(path);
    }
}
