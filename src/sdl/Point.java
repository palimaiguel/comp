package sdl;

import bgl.node.Node;
import org.jdom2.Element;
import sdl.graph.RunwayGraph;
import sdl.graph.RunwayPoint;
import sdl.graph.TaxiPoint;

import java.util.ArrayList;
import java.util.List;


public class Point {

    String type;
    String runwayOrTaxiway;
    Coordinates coord;
    RunwayPoint point;
    TaxiPoint tPoint;

    public Point(String type, String runwayOrTaxiway, Coordinates coord, RunwayPoint point) {
        this.type = type;
        this.runwayOrTaxiway = runwayOrTaxiway;
        this.coord = coord;
        this.point = point;
    }

    public Point(String type, String runwayOrTaxiway, Coordinates coord, TaxiPoint tPoint) {
        this.type = type;
        this.runwayOrTaxiway = runwayOrTaxiway;
        this.coord = coord;
        this.tPoint = tPoint;
    }
    public void writeToFather(Element end) {
        Element point = new Element(type);

        point.addContent(elementCoordinates());
        point.addContent(elementConnectsTo());
        end.addContent(point);
    }
    private Element elementCoordinates() {
        Element coordinates = new Element("coordinates");

        if(runwayOrTaxiway.equals("runway")) {
            coordinates.addContent(coord.elementLatitude(point.getLatitude()));
            coordinates.addContent(coord.elementLongitude(point.getLongitude()));
            coordinates.addContent(coord.elementAltitude());
        }
        else if(runwayOrTaxiway.equals("taxiway")){
            coordinates.addContent(coord.elementLatitude(tPoint.getLatitude()));
            coordinates.addContent(coord.elementLongitude(tPoint.getLongitude()));
            coordinates.addContent(coord.elementAltitude(tPoint.getAltitude()));
        }
        return coordinates;
    }

    private Element elementConnectsTo() {

        Element e = new Element("connectsTo");

        if(runwayOrTaxiway.equals("runway")) {
            List<String> ways = point.getTaxiwaysConnected();
            printList(ways, e);
        }
        else if(runwayOrTaxiway.equals("taxiway")){
            List<String> ways = tPoint.getTaxiwaysConnected();
            printList(ways, e);
            ways = tPoint.getRunwaysConnected();
            printList(ways, e);
        }
        return e;
    }

    private void printList(List<String> ways, Element e) {
        for(int i = 0; i < ways.size(); i++){
            String firstChar = "";
            String way = ways.get(i);
            if(way.length() > 0){
                if(way.substring(0,1).equals("r"))
                    firstChar = "r";
                else
                    firstChar = "x";
            }
            Element elem_way = new Element(firstChar + "way");
            elem_way.setAttribute("idr", ways.get(i));
            e.addContent(elem_way);
        }
    }

    public void setType(String type) {
        this.type = type;
    }
}
