package sdl;

import bgl.node.AirportNode;
import compiler.SemanticListener;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miguel on 02-06-2015.
 */
public class SDLFile
{
    private static final Namespace s_DcsNamespace = Namespace.getNamespace("dcs:scenario");
    private List<BaseOfOperations> m_operations = new ArrayList<>();

    public SDLFile(SemanticListener listener)
    {
        List<AirportNode> airports = listener.getAirportList();

        for(AirportNode airport: airports)
            m_operations.add(new BaseOfOperations(airport));
    }

    public void writeToFile(String filename) throws IOException
    {
        // Document definition and first tags:
        Document doc = new Document();
        Element root = new Element("scenario");
        doc.setRootElement(root);

        // Create namespaces:
        Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.addNamespaceDeclaration(xsi);
        Namespace xsd = Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
        root.addNamespaceDeclaration(xsd);
        root.setNamespace(s_DcsNamespace);

        // TODO stuff here
        Element bases = new Element("bases");

        for (int i = 0; i < m_operations.size(); i++)
            m_operations.get(i).writeToFather(bases, i+1);

        root.addContent(bases);

        // Add remaining (empty) objects:
        Element controllers = new Element("controllers");
        root.addContent(controllers);
        Element agentTypes = new Element("agentTypes");
        root.addContent(agentTypes);
        Element noFlyAreas = new Element("noFlyAreas");
        root.addContent(noFlyAreas);

        // Replace namespaces:
        setDefaultNamespace(root.getChildren());

        // Output the XML File:
        XMLOutputter outter = new XMLOutputter();
        outter.setFormat(Format.getPrettyFormat().setIndent("    "));
        outter.output(doc, new FileWriter(new File(filename)));
    }

    private void setDefaultNamespace(List<Element> children)
    {
        for (Element elem: children)
        {
            elem.setNamespace(s_DcsNamespace);
            setDefaultNamespace(elem.getChildren());
        }
    }
}
