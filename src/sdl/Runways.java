package sdl;

import bgl.node.AirportNode;
import bgl.node.Node;
import org.jdom2.Element;
import sdl.graph.RunwayGraph;
import sdl.utilities.NameConverter;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class Runways {

    AirportNode airportNode;
    TreeMap<String, RunwayGraph> runwaysGraphList;
    //private RunwayGraph runwaysGraph;
    //private String designatorLetter;

    public Runways(AirportNode airport, TreeMap<String, RunwayGraph> runwaysGraphList) {
        this.airportNode = airport;
        this.runwaysGraphList = runwaysGraphList;
    }

    public void writeToFather(Element airport) {

        List<Node> runwaysNodeList = airportNode.getChildren("Runway");

        Element runways = new Element("runways");
        if(runwaysNodeList == null || runwaysNodeList.size() == 0) {
            airport.addContent(runways);
            return;
        }

        for(Node node : runwaysNodeList){
            Element runway = new Element("runway");
            processRunway(runway, node);
            runways.addContent(runway);
        }

        airport.addContent(runways);
    }

    private void processRunway(Element runway, Node node) {

        String runwayId = getRunwayId(node, true);
        runway.setAttribute("id", runwayId);
        runway.addContent(elementCoordinates(node));
        runway.addContent(elementLength(node));
        runway.addContent(elementWidth(node));
        runway.addContent(elementSurface(node));

        RunwayGraph rg = getRunwaysGraph(node);
        if(rg != null) {
            BaseEnd baseEnd = new BaseEnd(node, runwayId, rg);
            baseEnd.writeToFatherBaseEnd(runway);
            baseEnd.writeToFatherReciprocalEnd(runway);
        }
    }

    public static String getRunwayId(Node node, boolean reverseIDAttached) {
        String res = "r";

        Double base = getAttributeNumber(node);
        String designator = getDesignatorLetter(node);

        if (!reverseIDAttached)
            return res + base.intValue() + designator;

        Double reverse = getReverse(node, base);
        String designatorReverse = getDesignatorLetterReverse(designator);
        return res + base.intValue() + designator + "-" + reverse.intValue() + designatorReverse;
    }
    public static Double getAttributeNumber(Node node) {
        String value = node.getAttribute("number");

        Double valueDouble = 0.0;
        try{
            valueDouble = Double.parseDouble(value);
        }catch (Exception e){
            switch (value) {
                case "EAST":
                    valueDouble = 270.0;
                    break;
                case "NORTH":
                    valueDouble = 0.0;
                    break;
                case "NORTHEAST":
                    valueDouble = 315.0;
                    break;
                case "NORTHWEST":
                    valueDouble = 45.0;
                    break;
                case "SOUTH":
                    valueDouble = 180.0;
                    break;
                case "SOUTHEAST":
                    valueDouble = 225.0;
                    break;
                case "SOUTHWEST":
                    valueDouble = 135.0;
                    break;
                case "WEST":
                    valueDouble = 90.0;
                    break;
                default:
                    valueDouble = 0.0;
                    break;
            }
        }

        return valueDouble;
    }
    public static Double getReverse(Node node, Double base) {
        Double heading = Double.parseDouble(node.getAttribute("heading"));
        return (base + heading / 10.0) % 36;
    }
    public static String getDesignatorLetter(Node node) {

        String letter = node.getAttribute("designator");

        if(letter.length() > 0) {
            letter = letter.substring(0, 1);
            letter = letter.toUpperCase();
        }

        if(letter.equals("N"))
            letter = "";

        return letter;
    }
    public static String getDesignatorLetterReverse(String str) {
        String res = "";
        switch (str) {
            case "L":
                res = "R";
                break;
            case "R":
                res = "L";
                break;
            case "A":
                res = "B";
                break;
            case "B":
                res = "A";
                break;
        }

        return res;
    }
    private Element elementCoordinates(Node node) {
        Element coordinates = new Element("coordinates");
        Coordinates c = new Coordinates(node);
        coordinates.addContent(c.elementLatitude());
        coordinates.addContent(c.elementLongitude());
        coordinates.addContent(c.elementAltitude());
        return coordinates;
    }
    private Element elementLength(Node node) {

        Element length = new Element("length");

        //check attribute unit
        String value = node.getAttribute("length");
        String lastChar;

        if(value.length() > 1) {
            lastChar = value.substring(value.length() - 1);

            if (lastChar.equals("F") || lastChar.equals("f"))
                length.setAttribute("lengthUnit", "Foot");
            else
                length.setAttribute("lengthUnit", "Meter");
        }
        else{
            length.setAttribute("lengthUnit", "Meter");
        }
        length.addContent(value);
        return length;
    }
    private Element elementWidth(Node node) {
        Element width = new Element("width");

        //check attribute unit
        String value = node.getAttribute("width");
        String lastChar;

        if(value.length() > 1) {
            lastChar = value.substring(value.length() - 1);

            if (lastChar.equals("F") || lastChar.equals("f"))
                width.setAttribute("lengthUnit", "Foot");
            else
                width.setAttribute("lengthUnit", "Meter");
        }
        else{
            width.setAttribute("lengthUnit", "Meter");
        }
        width.addContent(value);
        return width;
    }
    private Element elementSurface(Node node) {
        Element surface = new Element("surface");

        // Convert to SDL surface name convention:
        String surfaceValue = NameConverter.toPascalCase(node.getAttribute("surface"));

        surface.addContent(surfaceValue);

        return surface;
    }
    public RunwayGraph getRunwaysGraph(Node node) {
        String id = getRunwayId(node, false);
        return runwaysGraphList.get(id);
    }
}
