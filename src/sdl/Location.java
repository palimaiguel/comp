package sdl;

import bgl.node.AirportNode;
import org.jdom2.Element;

/**
 * Created by Paula on 6/7/2015.
 */
public class Location {

    private AirportNode airport;


    public Location(AirportNode airport) {
        this.airport = airport;
    }

    public void writeToFather(Element baseOfOperations) {
        Element location = new Element("location");

        location.addContent(elementAddress());
        location.addContent(elementZipCode());
        location.addContent(elementCity());
        location.addContent(elementStateDistrictRegion());
        location.addContent(elementCountry());
        location.addContent(elementCoordinates());

        baseOfOperations.addContent(location);
    }

    private Element elementAddress() {
        Element address = new Element("address");
        address.addContent("xxxxxxxxxxxxx");
        return address;
    }
    private Element elementZipCode() {
        Element zipCode = new Element("zipCode");
        zipCode.addContent("xxxxxxxxxxxxx");
        return zipCode;
    }
    private Element elementCity() {
        Element city = new Element("city");
        city.addContent(airport.getAttribute("city"));
        return city;
    }
    private Element elementStateDistrictRegion() {
        Element stateDistrictRegion = new Element("stateDistrictRegion");
        stateDistrictRegion.addContent(airport.getAttribute("state"));
        return stateDistrictRegion;
    }
    private Element elementCountry() {
        Element country = new Element("country");
        country.addContent(airport.getAttribute("country"));
        return country;
    }
    private Element elementCoordinates() {
        Element coordinates = new Element("coordinates");
        Coordinates c = new Coordinates(airport);

        coordinates.addContent(c.elementLatitude());
        coordinates.addContent(c.elementLongitude());
        coordinates.addContent(c.elementAltitude());
        return coordinates;
    }



}
