package sdl;

import bgl.node.Node;
import org.jdom2.Element;

/**
 * Created by Paula on 6/8/2015.
 */
public class Coordinates {

    Node node;

    Coordinates(Node node){
        this.node = node;
    }

    public Coordinates() {
    }

    public Element elementLatitude() {
        return elementLatitude(node.getAttribute("lat"));
    }
    public Element elementLongitude() {
        return elementLongitude(node.getAttribute("lon"));
    }
    public Element elementAltitude() {
        return elementAltitude(node.getAttribute("alt"));

    }

    public Element elementLatitude(String val) {
        Element latitude = new Element("latitude");
        latitude.addContent(val);
        return latitude;
    }
    public Element elementLongitude(String val) {
        Element longitude = new Element("longitude");
        longitude.addContent(val);
        return longitude;
    }
    public Element elementAltitude(String val) {
        Element altitude = new Element("altitude");
        altitude.setAttribute("measured", "amsl");

        String value = val;
        String lastChar;

        if(value.length() > 1) {
            lastChar = value.substring(value.length() - 1);

            if (lastChar.equals("F") || lastChar.equals("f"))
                altitude.setAttribute("lengthUnit", "Foot");
            else
                altitude.setAttribute("lengthUnit", "Meter");
        }
        else{
            altitude.setAttribute("lengthUnit", "Meter");
        }

        altitude.addContent(value);

        return altitude;
    }

}
