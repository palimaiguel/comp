package sdl;

import bgl.node.AirportNode;
import org.jdom2.Element;

/**
 * Created by Miguel on 04-06-2015.
 */
public class BaseOfOperations
{
    private AirportNode airport;
    ContactPerson contactPerson = new ContactPerson();
    Location location;
    Airport airportTag;


    public BaseOfOperations(AirportNode airport)
    {
        this.airport = airport;
        location = new Location(airport);
    }

    public void writeToFather(Element bases, int i)
    {
        Element baseOfOperations = new Element("baseOfOperations");

        //add attributes to base Opperations
        baseOfOperations.setAttribute("id", "b" + i);

        //add nodes to base Operations
        baseOfOperations.addContent(elementName());
        baseOfOperations.addContent(elementMobility());
        baseOfOperations.addContent(elementDescription());
        baseOfOperations.addContent(elementHistory());

        contactPerson.writeToFather(baseOfOperations);
        location.writeToFather(baseOfOperations);
        baseOfOperations.addContent(elementAvailability());

        airportTag = new Airport(airport, elementName(), new ContactPerson(), new Location(airport));

        airportTag.writeToFather(baseOfOperations);

        bases.addContent(baseOfOperations);
    }


    private Element elementName() {
        Element name = new Element("name");
        name.addContent(airport.getAttribute("name"));
        return name;
    }
    private Element elementMobility() {
        //mobility como e airportNode e sempre
        Element mobility = new Element("mobility");
        mobility.setAttribute("water", "false");
        mobility.setAttribute("underwater", "false");
        mobility.setAttribute("land", "false");
        mobility.setAttribute("air", "true");
        return mobility;
    }
    private Element elementDescription() {
        Element description = new Element("description");
        description.addContent("XXX Description XXX");
        return description;
    }
    private Element elementHistory() {
        Element history = new Element("history");
        history.addContent("XXX History XXX");
        return history;
    }
    private Element elementAvailability() {
        //TODO onde ir buscar isto?
        //availability como e airportNode e sempre
        Element availability = new Element("availability");
        availability.setAttribute("available", "always");
        return availability;
    }


}
