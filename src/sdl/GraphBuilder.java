package sdl;

import bgl.node.*;
import sdl.graph.*;

import java.util.*;

/**
 * Created by Miguel on 08-06-2015.
 */
public class GraphBuilder
{
    private AirportNode m_airport;

    // Structures needed to build the graph from the Airport node:
    private List<Node> m_taxiwayPointNodes;
    private List<Node> m_parkingNodes;
    //private List<Node> m_runwayNodes;

    // Structures we are going to need:
    private TreeMap<String, TaxiwayGraph> m_taxiwayPathsByName = new TreeMap<>();
    private TreeMap<String, TaxiwayGraph> m_taxiwayPathsById = new TreeMap<>();
    private TreeMap<String, RunwayGraph> m_runwayPaths = new TreeMap<>();

    private TreeMap<String, List<String>> m_parkingConnections = new TreeMap<>();

    // Counters:
    private Integer m_taxiCounter = 1;
    private Integer m_parkingCounter = 1;

    public GraphBuilder(AirportNode airportNode)
    {
        m_airport = airportNode;

        m_taxiwayPointNodes = airportNode.getChildren("TaxiwayPoint");
        m_parkingNodes = airportNode.getChildren("TaxiwayParking");

        build();
    }

    private void build()
    {
        List<Node> paths = m_airport.getChildren("TaxiwayPath");

        // Find all the different IDs we have:
        for(Node path: paths)
        {
            String startID = path.getAttribute("start");
            String endID = path.getAttribute("end");

            if (path.getAttribute("type").equals("RUNWAY"))
            {
                String identifier = Runways.getRunwayId(path, false);

                if (!m_runwayPaths.containsKey(identifier))
                {
                    RunwayGraph runwayGraph = new RunwayGraph(identifier);
                    runwayGraph.addLink(generateRunwayPoint(startID), generateRunwayPoint(endID));

                    m_runwayPaths.put(identifier, runwayGraph);
                }
                else
                {
                    RunwayGraph runwayGraph = m_runwayPaths.get(identifier);
                    runwayGraph.addLink(generateRunwayPoint(startID), generateRunwayPoint(endID));
                }
            }

            else if (path.getAttribute("type").equals("TAXI"))
            {
                String name = path.getAttribute("name");

                // Get width:
                Double width = Double.parseDouble(path.getAttribute("width").replaceFirst("M", ""));

                if (!m_taxiwayPathsByName.containsKey(name))
                {
                    // Build identifier:
                    String identifier = "x" + String.format("%02d", m_taxiCounter);

                    // Get designation:
                    String designation = "Taxiway";

                    // Get surface:
                    String surface = path.getAttribute("surface");
                    surface = surface.charAt(0) + surface.substring(1).toLowerCase();

                    TaxiwayGraph taxiwayGraph = new TaxiwayGraph(identifier, designation, surface);
                    taxiwayGraph.addLink(generateTaxiwayPoint(startID), generateTaxiwayPoint(endID), width);

                    m_taxiwayPathsByName.put(name, taxiwayGraph);

                    // Inc counter:
                    m_taxiCounter++;
                }
                else
                {
                    TaxiwayGraph runwayGraph = m_taxiwayPathsByName.get(name);
                    runwayGraph.addLink(generateTaxiwayPoint(startID), generateTaxiwayPoint(endID), width);
                }
            }

            else if (path.getAttribute("type").equals("PARKING"))
            {
                if (!m_parkingConnections.containsKey(endID))
                    m_parkingConnections.put(endID, new ArrayList<>());

                m_parkingConnections.get(endID).add(startID);
            }
        }

        // Percorrer todas as taxiways e associar taxiways a runways:
        for (Map.Entry<String, TaxiwayGraph> entry : m_taxiwayPathsByName.entrySet())
        {
            TaxiwayGraph graph = entry.getValue();

            for(TaxiPoint point: graph.getPoints())
            {
                for (Map.Entry<String, TaxiwayGraph> entry2 : m_taxiwayPathsByName.entrySet())
                {
                    TaxiwayGraph graph2 = entry2.getValue();

                    if (graph.getID().equals(graph2.getID()))
                        continue;

                    for(TaxiPoint point2: graph2.getPoints())
                    {
                        if (point.getIndex().equals(point2.getIndex()))
                            point.addTaxiwayConnected(graph2.getID());
                    }
                }

                for (Map.Entry<String, RunwayGraph> entry2 : m_runwayPaths.entrySet())
                {
                    RunwayGraph graph2 = entry2.getValue();

                    for(RunwayPoint point2: graph2.getPoints())
                    {
                        if (point.getIndex().equals(point2.getIndex()))
                        {
                            point.addRunwayConnected(graph2.getID());
                            point2.addTaxiwayConnected(graph.getID());
                        }
                    }
                }
            }
        }

        // Percorrer todos os parkings e associar a taxiways:
        for (Map.Entry<String, List<String>> entry : m_parkingConnections.entrySet())
        {
            String parkingID = entry.getKey();

            // Find parking node:
            Node parkingNode = null;
            for (Node taxiwayParkingNode: m_parkingNodes)
            {
                if (taxiwayParkingNode.getAttribute("index").equals(parkingID))
                    parkingNode = taxiwayParkingNode;
            }

            List<String> pointIDs = entry.getValue();

            for (String pointID: pointIDs)
            {
                for (Map.Entry<String, TaxiwayGraph> entry2 : m_taxiwayPathsByName.entrySet())
                {
                    TaxiwayGraph graph2 = entry2.getValue();

                    for(TaxiPoint point2: graph2.getPoints())
                    {
                        if (pointID.equals(point2.getIndex()))
                            ((TaxiwayparkingNode) parkingNode).addConnectToTaxiway(graph2.getID(), point2);
                    }
                }
            }
        }

        for (Map.Entry<String, TaxiwayGraph> entry : m_taxiwayPathsByName.entrySet())
        {
            TaxiwayGraph graph = entry.getValue();

            m_taxiwayPathsById.put(graph.getID(), graph);
        }
    }

    TreeMap<String, RunwayGraph> getRunwayGraphs()
    {
        return m_runwayPaths;
    }

    TreeMap<String, TaxiwayGraph> getTaxiwayGraphs()
    {
        return m_taxiwayPathsById;
    }

    private RunwayPoint generateRunwayPoint(String index)
    {
        // Find the taxiwaypointNode we want:
        Node correspondingNode = null;
        for (Node taxiwayPointNode: m_taxiwayPointNodes)
        {
            if (taxiwayPointNode.getAttribute("index").equals(index))
                correspondingNode = taxiwayPointNode;
        }

        RunwayPoint point = new RunwayPoint();
        point.setIndex(index);
        point.setLongitude(correspondingNode.getAttribute("lon"));
        point.setLatitude(correspondingNode.getAttribute("lat"));

        return point;
    }

    private TaxiPoint generateTaxiwayPoint(String index)
    {
        // Find the taxiwaypointNode we want:
        Node correspondingNode = null;
        for (Node taxiwayPointNode: m_taxiwayPointNodes)
        {
            if (taxiwayPointNode.getAttribute("index").equals(index))
                correspondingNode = taxiwayPointNode;
        }

        TaxiPoint point = new TaxiPoint();
        point.setIndex(index);
        point.setLongitude(correspondingNode.getAttribute("lon"));
        point.setLatitude(correspondingNode.getAttribute("lat"));

        return point;
    }
}
