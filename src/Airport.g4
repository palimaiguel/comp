grammar Airport;

import Tags, Attributes;

OPEN: '<';
CLOSE: '>';
EQUALS: '=';
SLASH: '/';
WS: [ \t\n\r]+ -> skip ;
REGEX_STRING: '"' [A-Za-z0-9\,\-\[\]\?\!\\\+\-\_\.\=\{\}\;\:\/\&\`\|\(\)  ]*? '"' ;
COMMENT : '<!--' .*? '-->' -> skip ;
XML_INIT: '<?xml'.*? '?>' -> skip ;
MARKER: '<Marker' .*? '/>' -> skip ;

// ************************************************************************
// ****************************** Attributes ******************************
// ************************************************************************


// detectar no lexer: OPEN TAG, CLOSE TAG, TIPOS DE VALOR:STRING, INT, ETC., TAGS (ENUM), ATRIBUTOS (ENUM)
// detectar no parser - as regras da gramática: uma tag tem k abrir, fechar, etc. uma tag so pode ter o atributo x, y, z....
// https://msdn.microsoft.com/en-us/library/cc526978.aspx#Airport

////////////////////////////////
///// START
//////////////////////////////////


// *'**************************************************************REPLACE FOR THIS
s: facilityData EOF;

////////////////////////////////
///// Facility Data
//////////////////////////////////

facilityData: OPEN TAG_FACILITY_DATA facilityData_attr* CLOSE facilityData_descendents* OPEN SLASH TAG_FACILITY_DATA CLOSE;

facilityData_attr: (ATTR_VERSION EQUALS country = REGEX_STRING)
					| (ATTR_XMLNS EQUALS state = REGEX_STRING)
					| (ATTR_XSI EQUALS city = REGEX_STRING);

facilityData_descendents: scenery_object | airport;


/*
s: airport EOF; */

////////////////////////////////
///// AIRPORT
//////////////////////////////////

airport: OPEN TAG_AIRPORT airport_attr* CLOSE airport_descendents OPEN SLASH TAG_AIRPORT CLOSE;

airport_attr: (ATTR_COUNTRY EQUALS country = REGEX_STRING)
				| (ATTR_STATE EQUALS state = REGEX_STRING)
				| (ATTR_CITY EQUALS city = REGEX_STRING)
				| (ATTR_REGION EQUALS region = REGEX_STRING)
				| (ATTR_NAME EQUALS name = REGEX_STRING)
				| (ATTR_LAT EQUALS lat = REGEX_STRING)
				| (ATTR_LON EQUALS lon = REGEX_STRING)
				| (ATTR_ALT EQUALS alt = REGEX_STRING)
				| (ATTR_MAGVAR EQUALS magvar = REGEX_STRING)
				| (ATTR_IDENT EQUALS ident = REGEX_STRING)
				| (ATTR_TEST_RADIUS EQUALS test = REGEX_STRING)
				| (ATTR_TRAFFIC_SCALAR EQUALS trafficScalar = REGEX_STRING);

airport_descendents: airport_other_info airport_taxi_info airport_other_info;

////////////////////////////////
///// TAXI_INFO
//////////////////////////////////

airport_taxi_info: (taxiway_point* taxiway_parking* taxi_name* taxiway_path*);

taxiway_point: OPEN TAG_TAXIWAY_POINT taxiway_point_attr* SLASH CLOSE;

taxiway_point_attr: (ATTR_INDEX EQUALS index = REGEX_STRING)
				| (ATTR_TYPE EQUALS type = REGEX_STRING)
				| (ATTR_ORIENTATION EQUALS orientation = REGEX_STRING)
				| (ATTR_LAT EQUALS lat = REGEX_STRING)
				| (ATTR_LON EQUALS lon = REGEX_STRING)
				| (ATTR_BIAS_X EQUALS bias_x = REGEX_STRING)
				| (ATTR_BIAS_Z EQUALS bias_z = REGEX_STRING);

taxiway_parking: OPEN TAG_TAXIWAY_PARKING taxiway_parking_attr* SLASH CLOSE;

taxiway_parking_attr: (ATTR_INDEX EQUALS index = REGEX_STRING)
				| (ATTR_LAT EQUALS lat = REGEX_STRING)
				| (ATTR_LON EQUALS lon = REGEX_STRING)
				| (ATTR_BIAS_X EQUALS bias_x = REGEX_STRING)
				| (ATTR_BIAS_Z EQUALS bias_z = REGEX_STRING)
				| (ATTR_HEADING EQUALS heading = REGEX_STRING)
				| (ATTR_RADIUS EQUALS radius = REGEX_STRING)
				| (ATTR_TYPE EQUALS type = REGEX_STRING)
				| (ATTR_NAME EQUALS name = REGEX_STRING)
				| (ATTR_NUMBER EQUALS number = REGEX_STRING)
				| (ATTR_AIRLINE_CODES EQUALS airline = REGEX_STRING)
				| (ATTR_PUSH_BACK EQUALS push_back = REGEX_STRING)
				| (ATTR_TEE_OFFSET_1 EQUALS tee_offset1 = REGEX_STRING)
				| (ATTR_TEE_OFFSET_2 EQUALS tee_offset2 = REGEX_STRING)
				| (ATTR_TEE_OFFSET_3 EQUALS tee_offset3 = REGEX_STRING)
				| (ATTR_TEE_OFFSET_4 EQUALS tee_offset4 = REGEX_STRING);

taxi_name: OPEN TAG_TAXINAME taxi_name_attr* SLASH CLOSE;
taxi_name_attr: (ATTR_INDEX EQUALS index = REGEX_STRING)
				| (ATTR_NAME EQUALS name = REGEX_STRING);

taxiway_path: OPEN TAG_TAXIWAY_PATH taxiway_path_attr* SLASH CLOSE;
taxiway_path_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
				| (ATTR_START EQUALS start_var = REGEX_STRING)
				| (ATTR_END EQUALS end = REGEX_STRING)
				| (ATTR_WIDTH EQUALS width = REGEX_STRING)
				| (ATTR_WEIGHT_LIMIT EQUALS weight = REGEX_STRING)
				| (ATTR_SURFACE EQUALS surface = REGEX_STRING)
				| (ATTR_DRAW_SURFACE EQUALS draw_surface = REGEX_STRING)
				| (ATTR_DRAW_DETAIL EQUALS draw_detail = REGEX_STRING)
				| (ATTR_CENTER_LINE EQUALS center_line = REGEX_STRING)
				| (ATTR_CENTER_LINE_LIGHTED EQUALS center_line_lighted = REGEX_STRING)
				| (ATTR_LEFT_EDGE EQUALS left_edge = REGEX_STRING)
				| (ATTR_LEFT_EDGE_LIGHTED EQUALS left_edge_lighted = REGEX_STRING)
				| (ATTR_RIGHT_EDGE EQUALS right_edge = REGEX_STRING)
				| (ATTR_RIGHT_EDGE_LIGHTED EQUALS right_edge_lighted = REGEX_STRING)
				| (ATTR_NUMBER EQUALS number = REGEX_STRING)
				| (ATTR_DESIGNATOR EQUALS designator = REGEX_STRING)
				| (ATTR_NAME EQUALS name = REGEX_STRING);

////////////////////////////////
///// OTHER_DESCENDANTS
//////////////////////////////////

// airport_other_info: other*;
airport_other_info: other* ;
other: (tower) | (services) | (com) | (runway) | (runway_alias) | (aprons) | (aprons_edge_lights) |
		(taxiway_sign) | (waypoint) | (approach) | (ndb) | (helipad) | (start) | (jetway)
		| (blast_fence) | (boundary_fence) | (delete_airport);

////////////////////////////////
///// DESCENDANT TOWER
//////////////////////////////////

tower: OPEN TAG_TOWER tower_attr* (SLASH CLOSE | (CLOSE tower_descendants* OPEN SLASH TAG_TOWER CLOSE));
tower_attr: (ATTR_LAT EQUALS lat = REGEX_STRING)
				| (ATTR_LON EQUALS lon = REGEX_STRING)
				| (ATTR_ALT EQUALS alt = REGEX_STRING); //[TO DO]

tower_descendants: scenery_object;

////////////////////////////////
///// DESCENDANT SERVICES
//////////////////////////////////

services: OPEN TAG_SERVICES CLOSE services_descendant* OPEN SLASH TAG_SERVICES CLOSE;

services_descendant: fuel;

fuel: OPEN TAG_FUEL fuel_attr* SLASH CLOSE;

fuel_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
		| (ATTR_AVAILABILITY EQUALS availability = REGEX_STRING);

////////////////////////////////
///// DESCENDANT COM
//////////////////////////////////

com: OPEN TAG_COM com_attr* SLASH CLOSE;

com_attr: (ATTR_FREQUENCY EQUALS frequency = REGEX_STRING)
			| (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_NAME EQUALS name = REGEX_STRING);

////////////////////////////////
///// DESCENDANT RUNWAY
//////////////////////////////////

runway: OPEN TAG_RUNWAY runway_attr* CLOSE runway_descendant* OPEN SLASH TAG_RUNWAY CLOSE;

runway_attr: (ATTR_LAT EQUALS lat = REGEX_STRING)
			| (ATTR_LON EQUALS lon = REGEX_STRING)
			| (ATTR_ALT EQUALS alt = REGEX_STRING) // [TO DO]
			| (ATTR_SURFACE EQUALS surface = REGEX_STRING)
			| (ATTR_HEADING EQUALS heading = REGEX_STRING)
			| (ATTR_LENGTH EQUALS length = REGEX_STRING) // [TO DO]
			| (ATTR_WIDTH EQUALS width = REGEX_STRING) // [TO DO]
			| (ATTR_NUMBER EQUALS number = REGEX_STRING)
			| (ATTR_DESIGNATOR EQUALS designator = REGEX_STRING)
			| (ATTR_PRIMARY_DESIGNATOR EQUALS primary_designator = REGEX_STRING)
			| (ATTR_SECONDARY_DESIGNATOR EQUALS secondary_designator = REGEX_STRING)
			| (ATTR_PATTERN_ALTITUDE EQUALS pattern_altitude = REGEX_STRING) // [TO DO]
			| (ATTR_PRIMARY_TAKEOFF EQUALS primary_takeoff = REGEX_STRING)
			| (ATTR_PRIMARY_LANDING EQUALS primary_landing = REGEX_STRING)
			| (ATTR_PRIMARY_PATTERN EQUALS primary_pattern = REGEX_STRING)
			| (ATTR_SECONDARY_TAKEOFF EQUALS secondary_takeoff = REGEX_STRING)
			| (ATTR_SECONDARY_LANDING EQUALS secondary_landing = REGEX_STRING)
			| (ATTR_SECONDARY_PATTERN EQUALS secondary_pattern = REGEX_STRING)
			| (ATTR_PRIMARY_MARKING_BIAS EQUALS primary_marking_bias = REGEX_STRING) // [TO DO]
			| (ATTR_SECONDARY_MARKING_BIAS EQUALS secondary_marking_bias = REGEX_STRING); // [TO DO]

///// RUNWAY DESCENDANTS

runway_descendant: markings | lights | offset_threshold | blast_pad | overrun | approach_lights |
							vasi | ils | runway_start;

markings: OPEN TAG_MARKINGS markings_attr* SLASH CLOSE;
markings_attr: (ATTR_ALTERNATE_THRESHOLD EQUALS alternate_threshold = REGEX_STRING)
			| (ATTR_ALTERNATE_TOUCHDOWN EQUALS alternate_touchdown = REGEX_STRING)
			| (ATTR_ALTERNATE_FIXED_DISTANCE EQUALS alternate_fixed_distance = REGEX_STRING)
			| (ATTR_ALTERNATE_PRECISION EQUALS alternate_precision = REGEX_STRING)
			| (ATTR_LEADING_ZERO_IDENT EQUALS leading_zero_ident = REGEX_STRING)
			| (ATTR_NO_THRESHOLD_END_ARROWS EQUALS no_threshold_end_arrows = REGEX_STRING)
			| (ATTR_EDGES EQUALS edges = REGEX_STRING)
			| (ATTR_THRESHOLD EQUALS threshold = REGEX_STRING)
			| (ATTR_FIXED_DISTANCE EQUALS fixed_distance = REGEX_STRING)
			| (ATTR_TOUCHDOWN EQUALS touchdown = REGEX_STRING)
			| (ATTR_DASHES EQUALS dashes = REGEX_STRING)
			| (ATTR_IDENT EQUALS ident = REGEX_STRING)
			| (ATTR_PRECISION EQUALS precision = REGEX_STRING)
			| (ATTR_EDGE_PAVEMENT EQUALS edge = REGEX_STRING)
			| (ATTR_SINGLE_END EQUALS single = REGEX_STRING)
			| (ATTR_PRIMARY_CLOSED EQUALS primary_closed = REGEX_STRING)
			| (ATTR_SECONDARY_CLOSED EQUALS secondary_closed = REGEX_STRING)
			| (ATTR_PRIMARY_STOL EQUALS primary_stol = REGEX_STRING)
			| (ATTR_SECONDARY_STOL EQUALS secondary_stol = REGEX_STRING);

lights: OPEN TAG_LIGHTS lights_attr* SLASH CLOSE;
lights_attr:(ATTR_CENTER EQUALS center = REGEX_STRING)
			| (ATTR_EDGE EQUALS edge = REGEX_STRING)
			| (ATTR_CENTER_RED EQUALS center_red = REGEX_STRING);

offset_threshold: OPEN TAG_OFFSET_THRESHOLD offset_threshold_attr* SLASH CLOSE;
offset_threshold_attr: (ATTR_END EQUALS end = REGEX_STRING)
			| (ATTR_LENGTH EQUALS length = REGEX_STRING) // [TO DO]
			| (ATTR_WIDTH EQUALS width = REGEX_STRING) // [TO DO]
			| (ATTR_SURFACE EQUALS surface= REGEX_STRING);

blast_pad: OPEN TAG_BLAST_PAD blast_pad_attr* SLASH CLOSE;
blast_pad_attr: (ATTR_END EQUALS end = REGEX_STRING)
			| (ATTR_LENGTH EQUALS length = REGEX_STRING) // [TO DO]
			| (ATTR_WIDTH EQUALS width = REGEX_STRING) // [TO DO]
			| (ATTR_SURFACE EQUALS surface = REGEX_STRING);

overrun: OPEN TAG_OVERRUN overrun_attr* SLASH CLOSE;
overrun_attr: (ATTR_END EQUALS end = REGEX_STRING)
			| (ATTR_LENGTH EQUALS length = REGEX_STRING) // [TO DO]
			| (ATTR_WIDTH EQUALS width = REGEX_STRING) // [TO DO]
			| (ATTR_SURFACE EQUALS surface = REGEX_STRING);

approach_lights: OPEN TAG_APPROACH_LIGHTS approach_lights_attr* SLASH CLOSE;
approach_lights_attr: (ATTR_END EQUALS end = REGEX_STRING)
			| (ATTR_SYSTEM EQUALS system = REGEX_STRING)
			| (ATTR_STROBES EQUALS strobes = REGEX_STRING)
			| (ATTR_REIL EQUALS reil = REGEX_STRING)
			| (ATTR_TOUCHDOWN EQUALS touchdown = REGEX_STRING)
			| (ATTR_END_LIGHTS EQUALS end_lights = REGEX_STRING);

vasi: OPEN TAG_VASI vasi_attr* SLASH CLOSE;
vasi_attr: (ATTR_END EQUALS end = REGEX_STRING)
			| (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_SIDE EQUALS side = REGEX_STRING)
			| (ATTR_BIAS_X EQUALS bias_x = REGEX_STRING)
			| (ATTR_BIAS_Z EQUALS bias_z = REGEX_STRING)
			| (ATTR_SPACING EQUALS spacing = REGEX_STRING)
			| (ATTR_PITCH EQUALS pitch = REGEX_STRING);

ils: OPEN TAG_ILS ils_attr* CLOSE ils_descendant* OPEN SLASH TAG_ILS CLOSE;
ils_attr: (ATTR_LAT EQUALS lat = REGEX_STRING)
			| (ATTR_LON EQUALS lon = REGEX_STRING)
			| (ATTR_ALT EQUALS alt = REGEX_STRING) // [TO DO]
			| (ATTR_HEADING EQUALS heading = REGEX_STRING)
			| (ATTR_FREQUENCY EQUALS frequency = REGEX_STRING)
			| (ATTR_END EQUALS end = REGEX_STRING)
			| (ATTR_RANGE EQUALS range = REGEX_STRING)
			| (ATTR_MAGVAR EQUALS magvar = REGEX_STRING)
			| (ATTR_IDENT EQUALS ident = REGEX_STRING)
			| (ATTR_WIDTH EQUALS width = REGEX_STRING)
			| (ATTR_NAME EQUALS name = REGEX_STRING)
			| (ATTR_BACKCOURSE EQUALS backcourse = REGEX_STRING);

ils_descendant: glide_slope | dme | visual_model;

glide_slope: OPEN TAG_GLIDE_SLOPE glide_slope_attr* SLASH CLOSE;
glide_slope_attr: (ATTR_LAT EQUALS lat = REGEX_STRING)
                | (ATTR_LON EQUALS lon = REGEX_STRING)
                | (ATTR_ALT EQUALS alt = REGEX_STRING) // [TO DO]
                | (ATTR_PITCH EQUALS pitch = REGEX_STRING)
                | (ATTR_RANGE EQUALS range = REGEX_STRING);

runway_start: OPEN TAG_RUNWAY_START runway_start_attr* SLASH CLOSE;
runway_start_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_LAT EQUALS lat = REGEX_STRING)
			| (ATTR_LON EQUALS lon = REGEX_STRING)
			| (ATTR_ALT EQUALS alt = REGEX_STRING) // [TO DO]
			| (ATTR_HEADING EQUALS heading = REGEX_STRING)
			| (ATTR_END EQUALS end = REGEX_STRING);

////////////////////////////////
///// DESCENDANT RUNWAY_ALIAS
//////////////////////////////////

runway_alias: OPEN TAG_RUNWAY_ALIAS runway_alias_attr* SLASH CLOSE;
runway_alias_attr: (ATTR_NUMBER EQUALS REGEX_STRING)
			| (ATTR_DESIGNATOR EQUALS REGEX_STRING);

////////////////////////////////
///// DESCENDANT APRONS
//////////////////////////////////

aprons: OPEN TAG_APRONS CLOSE apron* OPEN SLASH TAG_APRONS CLOSE;

apron: OPEN TAG_APRON apron_attr* CLOSE apron_descendant* OPEN SLASH TAG_APRON CLOSE;

apron_attr: (ATTR_SURFACE EQUALS REGEX_STRING)
			| (ATTR_DRAW_SURFACE EQUALS REGEX_STRING)
			| (ATTR_DRAW_DETAIL EQUALS REGEX_STRING);

apron_descendant: vertex;

vertex: OPEN TAG_VERTEX vertex_attr* SLASH CLOSE;
vertex_attr: (ATTR_BIAS_X EQUALS REGEX_STRING)
			| (ATTR_BIAS_Z EQUALS REGEX_STRING)
			| (ATTR_LAT EQUALS REGEX_STRING)
			| (ATTR_LON EQUALS REGEX_STRING);

////////////////////////////////
///// DESCENDANT APRONS_EDGE_LIGHTS
//////////////////////////////////

aprons_edge_lights: OPEN TAG_APRON_EDGE_LIGHTS CLOSE aprons_edge_lights_descendant* OPEN SLASH TAG_APRON_EDGE_LIGHTS CLOSE;

aprons_edge_lights_descendant: edge_lights;

edge_lights: OPEN TAG_EDGE_LIGHTS CLOSE vertex* OPEN SLASH TAG_EDGE_LIGHTS CLOSE;

////////////////////////////////
///// DESCENDANT TaxiwaySigns
//////////////////////////////////

taxiway_sign: OPEN TAG_TAXIWAY_SIGN taxiway_sign_attr* SLASH CLOSE;
taxiway_sign_attr: (ATTR_LAT EQUALS REGEX_STRING)
			| (ATTR_LON EQUALS REGEX_STRING)
			| (ATTR_HEADING EQUALS REGEX_STRING)
			| (ATTR_LABEL EQUALS REGEX_STRING)
			| (ATTR_JUSTIFICATION EQUALS REGEX_STRING)
			| (ATTR_SIZE EQUALS REGEX_STRING);

////////////////////////////////
///// DESCENDANT WAYPOINT
////////////////////////////////// 

waypoint: OPEN TAG_WAYPOINT waypoint_attr* CLOSE waypoint_descendant* OPEN SLASH TAG_WAYPOINT CLOSE;

waypoint_attr: (ATTR_LAT EQUALS lat = REGEX_STRING)
			| (ATTR_LON EQUALS lon = REGEX_STRING)
			| (ATTR_WAYPOINT_TYPE waypoint_type = EQUALS REGEX_STRING)
			| (ATTR_MAGVAR EQUALS magvar = REGEX_STRING)
			| (ATTR_LABEL EQUALS label = REGEX_STRING)
			| (ATTR_WAYPOINT_REGION EQUALS waypoint_region = REGEX_STRING)
			| (ATTR_WAYPOINT_INDENT EQUALS waypoint_indent = REGEX_STRING);


waypoint_descendant: route;
route: OPEN TAG_ROUTE route_attr* CLOSE route_descendant* OPEN SLASH TAG_ROUTE CLOSE;
route_attr: (ATTR_ROUTE_TYPE EQUALS route_type = REGEX_STRING)
			| (ATTR_NAME EQUALS name = REGEX_STRING);

route_descendant: previous | next;

previous: OPEN TAG_PREVIOUS previous_attr* SLASH CLOSE;
previous_attr: (ATTR_WAYPOINT_TYPE EQUALS waypoint_type = REGEX_STRING)
			| (ATTR_WAYPOINT_REGION EQUALS waypoint_region = REGEX_STRING)
			| (ATTR_WAYPOINT_INDENT EQUALS waypoint_indent = REGEX_STRING)
			| (ATTR_ALTITUDE_MINIMUM EQUALS altitude_minimum = REGEX_STRING);

next: OPEN TAG_NEXT next_attr* SLASH CLOSE;
next_attr: (ATTR_WAYPOINT_TYPE EQUALS waypoint_type = REGEX_STRING)
			| (ATTR_WAYPOINT_REGION EQUALS waypoint_region = REGEX_STRING)
			| (ATTR_WAYPOINT_INDENT EQUALS waypoint_indent = REGEX_STRING)
			| (ATTR_ALTITUDE_MINIMUM EQUALS altitude_minimum = REGEX_STRING);


////////////////////////////////
///// DESCENDANT Approach
////////////////////////////////// 

approach: OPEN TAG_APPROACH approach_attr* CLOSE approach_descendant* OPEN SLASH TAG_APPROACH CLOSE;

approach_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_RUNWAY EQUALS runway_var = REGEX_STRING)
			| (ATTR_DESIGNATOR EQUALS designator = REGEX_STRING)
			| (ATTR_SUFFIX EQUALS suffix = REGEX_STRING)
			| (ATTR_GPS_OVERLAY EQUALS gps_overlay = REGEX_STRING)
			| (ATTR_FIX_TYPE EQUALS fix_type = REGEX_STRING)
			| (ATTR_FIX_REGION EQUALS fix_region = REGEX_STRING)
			| (ATTR_FIX_IDENT EQUALS fix_ident = REGEX_STRING)
			| (ATTR_ALTITUDE EQUALS altitude = REGEX_STRING)
			| (ATTR_HEADING EQUALS heading = REGEX_STRING)
			| (ATTR_MISSED_ALTITUDE EQUALS missed_altitude = REGEX_STRING);

approach_descendant: approach_legs | missed_approach_legs | transition;

approach_legs: OPEN TAG_APPROACH_LEGS CLOSE approach_legs_descendant* OPEN SLASH TAG_APPROACH_LEGS CLOSE;
approach_legs_descendant: leg;
leg: OPEN TAG_LEG leg_attr* SLASH CLOSE;
leg_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_FIX_TYPE EQUALS fix_type = REGEX_STRING)
			| (ATTR_FIX_REGION EQUALS fix_region = REGEX_STRING)
			| (ATTR_FIX_IDENT EQUALS fix_ident = REGEX_STRING)
			| (ATTR_FLY_OVER EQUALS fly_over = REGEX_STRING)
			| (ATTR_TURN_DIRECTION EQUALS turn_direction = REGEX_STRING)
			| (ATTR_RECOMMENDED_TYPE EQUALS recommended_type = REGEX_STRING)
			| (ATTR_RECOMMENDED_REGION EQUALS recommended_region = REGEX_STRING)
			| (ATTR_RECOMMENDED_IDENT EQUALS recommended_ident = REGEX_STRING)
			| (ATTR_THETA EQUALS theta = REGEX_STRING)
			| (ATTR_RHO EQUALS rho = REGEX_STRING)
			| (ATTR_TRUE_COURSE EQUALS true_course = REGEX_STRING)
			| (ATTR_MAGNETIC_COURSE EQUALS magnetic_course = REGEX_STRING)
			| (ATTR_DISTANCE EQUALS distance = REGEX_STRING)
			| (ATTR_TIME EQUALS time = REGEX_STRING)
			| (ATTR_ALTITUDE_DESCRIPTOR EQUALS altitude_descriptor = REGEX_STRING)
			| (ATTR_ALTITUDE1 EQUALS altitude1 = REGEX_STRING)
			| (ATTR_ALTITUDE2 EQUALS altitude2 = REGEX_STRING);

missed_approach_legs: OPEN TAG_MISSED_APROACH_LEGS CLOSE missed_approach_legs_descendant* OPEN SLASH TAG_MISSED_APROACH_LEGS CLOSE;
missed_approach_legs_descendant: leg;

transition: OPEN TAG_TRANSITION transition_attr* CLOSE transition_descendant* OPEN SLASH TAG_TRANSITION CLOSE;

transition_attr: (ATTR_TRANSITION_TYPE EQUALS transition_type = REGEX_STRING)
			| (ATTR_FIX_TYPE EQUALS fix_type = REGEX_STRING)
			| (ATTR_FIX_REGION EQUALS fix_region = REGEX_STRING)
			| (ATTR_FIX_IDENT EQUALS fix_ident = REGEX_STRING)
			| (ATTR_ALTITUDE EQUALS altitude = REGEX_STRING);

transition_descendant: dme_arc | transition_legs;

dme_arc: OPEN TAG_DME_ARC dme_arc_attr* SLASH CLOSE;
dme_arc_attr: (ATTR_RADIAL EQUALS radial = REGEX_STRING)
			| (ATTR_DISTANCE EQUALS distance = REGEX_STRING)
			| (ATTR_DME_REGION EQUALS dme_region = REGEX_STRING)
			| (ATTR_DME_IDENT EQUALS dme_ident = REGEX_STRING);

transition_legs: OPEN TAG_TRANSITION_LEGS CLOSE transition_legs_descendant* OPEN SLASH TAG_TRANSITION_LEGS CLOSE;
transition_legs_descendant: leg;

////////////////////////////////
///// DESCENDANT NDB
////////////////////////////////// 

ndb: OPEN TAG_NDB ndb_attr* CLOSE ndb_descendant* OPEN SLASH TAG_NDB CLOSE;

ndb_attr: (ATTR_LAT EQUALS REGEX_STRING)
			| (ATTR_LON EQUALS REGEX_STRING)
			| (ATTR_ALT EQUALS REGEX_STRING) // [TO DO]
			| (ATTR_TYPE EQUALS REGEX_STRING)
			| (ATTR_FREQUENCY EQUALS REGEX_STRING)
			| (ATTR_RANGE EQUALS REGEX_STRING)
			| (ATTR_MAGVAR EQUALS REGEX_STRING)
			| (ATTR_REGION EQUALS REGEX_STRING)
			| (ATTR_IDENT EQUALS REGEX_STRING)
			| (ATTR_NAME EQUALS REGEX_STRING);

ndb_descendant: visual_model;

visual_model: OPEN TAG_VISUAL_MODEL visual_model_attr* CLOSE visual_model_descendant* OPEN SLASH TAG_VISUAL_MODEL CLOSE;

visual_model_attr: (ATTR_HEADING EQUALS REGEX_STRING)
			| (ATTR_IMAGE_COMPLEXITY EQUALS REGEX_STRING)
			| (ATTR_NAME EQUALS REGEX_STRING)
			| (ATTR_INSTANCE_ID EQUALS REGEX_STRING);

// visual_model_descendant: ndb | ils | vor | bias_xyz;
visual_model_descendant: bias_xyz;

dme: OPEN TAG_DME dme_attr* SLASH CLOSE;

dme_attr: (ATTR_LAT EQUALS REGEX_STRING)
			| (ATTR_LON EQUALS REGEX_STRING)
			| (ATTR_ALT EQUALS REGEX_STRING) // [TO DO]
			| (ATTR_RANGE EQUALS REGEX_STRING);

////////////////////////////////
///// DESCENDANT HELIPAD
////////////////////////////////// 

helipad: OPEN TAG_HELIPAD helipad_attr* SLASH CLOSE;

helipad_attr: (ATTR_LAT EQUALS lat = REGEX_STRING)
			| (ATTR_LON EQUALS lon = REGEX_STRING)
			| (ATTR_ALT EQUALS alt = REGEX_STRING) // [TO DO]
			| (ATTR_SURFACE EQUALS surface = REGEX_STRING)
			| (ATTR_HEADING EQUALS heading = REGEX_STRING)
			| (ATTR_LENGTH EQUALS length = REGEX_STRING)
			| (ATTR_WIDTH EQUALS width = REGEX_STRING)
			| (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_CLOSED EQUALS closed = REGEX_STRING)
			| (ATTR_TRANSPARENT EQUALS transparent = REGEX_STRING)
			| (ATTR_RED EQUALS red = REGEX_STRING)
			| (ATTR_GREEN EQUALS green = REGEX_STRING)
			| (ATTR_BLUE EQUALS blue = REGEX_STRING);

////////////////////////////////
///// DESCENDANT START
////////////////////////////////// 

start: OPEN TAG_START start_attr* SLASH CLOSE;

start_attr: (ATTR_TYPE EQUALS REGEX_STRING)
			| (ATTR_LAT EQUALS REGEX_STRING)
			| (ATTR_LON EQUALS REGEX_STRING)
			| (ATTR_ALT EQUALS REGEX_STRING) // [TO DO]
			| (ATTR_HEADING EQUALS REGEX_STRING)
			| (ATTR_NUMBER EQUALS REGEX_STRING)
			| (ATTR_DESIGNATOR EQUALS REGEX_STRING);

////////////////////////////////
///// DESCENDANT JETWAY
////////////////////////////////// 
jetway: OPEN TAG_JETWAY jetway_attr* CLOSE jetway_descendant* OPEN SLASH TAG_JETWAY CLOSE;

jetway_attr: (ATTR_GATE_NAME EQUALS gate_name = REGEX_STRING)
			| (ATTR_PARKING_NUMBER EQUALS parking_number = REGEX_STRING);

jetway_descendant: scenery_object;

scenery_object: OPEN TAG_SCENERY_OBJECT scenery_object_attr* CLOSE scenery_object_descendant* OPEN SLASH TAG_SCENERY_OBJECT CLOSE;

scenery_object_attr: (ATTR_INSTANCE_ID EQUALS instance_id = REGEX_STRING)
					| (ATTR_LAT EQUALS lat = REGEX_STRING)
					| (ATTR_LON EQUALS lon = REGEX_STRING)
					| (ATTR_ALT EQUALS alt = REGEX_STRING) // [TO DO]
					| (ATTR_ALTITUDE_IS_AGL EQUALS altitude_is_agl = REGEX_STRING)
					| (ATTR_PITCH EQUALS pitch = REGEX_STRING)
					| (ATTR_BANK EQUALS bank = REGEX_STRING)
					| (ATTR_HEADING EQUALS heading = REGEX_STRING)
					| (ATTR_IMAGE_COMPLEXITY EQUALS image_complexity = REGEX_STRING);

scenery_object_descendant: bias_xyz | beacon | effect | generic_building | library_object | trigger | windsock | attached_object;

bias_xyz: OPEN TAG_BIAS_XYZ bias_xyz_attr* SLASH CLOSE ;

bias_xyz_attr: (ATTR_BIAS_X EQUALS bias_x = REGEX_STRING)
			| (ATTR_BIAS_Y EQUALS bias_y = REGEX_STRING)
			| (ATTR_BIAS_Z EQUALS bias_z = REGEX_STRING);

beacon: OPEN TAG_BEACON beacon_attr* SLASH CLOSE;

beacon_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_BASE_TYPE EQUALS base_type = REGEX_STRING);

effect: OPEN TAG_EFFECT effect_attr* SLASH CLOSE;

effect_attr: (ATTR_EFFECT_NAME EQUALS effect_name = REGEX_STRING)
			| (ATTR_EFFECT_PARAMS EQUALS effect_params = REGEX_STRING);
 
generic_building: OPEN TAG_GENERIC_BUILDING generic_building_attr* CLOSE generic_building_descendant* OPEN SLASH TAG_GENERIC_BUILDING CLOSE;

generic_building_attr: (ATTR_SCALE EQUALS scale = REGEX_STRING)
						| (ATTR_BOTTOM_TEXTURE EQUALS bottom_texture = REGEX_STRING)
						| (ATTR_ROOF_TEXTURE EQUALS roof_texture = REGEX_STRING)
						| (ATTR_TOP_TEXTURE EQUALS top_texture = REGEX_STRING)
						| (ATTR_WINDOW_TEXTURE EQUALS window_texture = REGEX_STRING);

generic_building_descendant: rectangular_building | pyramidal_building | multi_sided_building; 

rectangular_building: OPEN TAG_RECTANGULAR_BUILDING rectangular_building_attr* SLASH CLOSE;

rectangular_building_attr: (ATTR_ROOF_TYPE EQUALS roof_type = REGEX_STRING)
						| (ATTR_SIZE_X EQUALS size_x = REGEX_STRING)
						| (ATTR_SIZE_Z EQUALS size_z = REGEX_STRING)
						| (ATTR_SIZE_BOTTOM_Y EQUALS size_bottom_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_BOTTOM_X EQUALS texture_index_bottom_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_BOTTOM_Z EQUALS texture_index_bottom_z = REGEX_STRING)
						| (ATTR_SIZE_WINDOW_Y EQUALS size_window_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_X EQUALS texture_index_window_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_Y EQUALS texture_index_window_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_Z EQUALS texture_index_window_z = REGEX_STRING)
						| (ATTR_SIZE_TOP_Y EQUALS size_top_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_TOP_X EQUALS texture_index_top_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_TOP_Z EQUALS texture_index_top_z = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_X EQUALS texture_index_roof_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_Z EQUALS texture_index_roof_z = REGEX_STRING)

						// Extras relativamente aos Rectangular flat roof:
						| (ATTR_SIZE_ROOF_Y EQUALS size_roof_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_Y EQUALS texture_index_roof_y = REGEX_STRING)
						| (ATTR_GABLE_TEXTURE EQUALS gable_texture = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_GABLE_Y EQUALS texture_index_gable_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_GABLE_Z EQUALS texture_index_gable_z = REGEX_STRING)
						| (ATTR_FACE_TEXTURE EQUALS face_texture = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_FACE_X EQUALS texture_index_face_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_FACE_Y EQUALS texture_index_face_y = REGEX_STRING);

pyramidal_building: OPEN TAG_PYRAMIDAL_BUILDING pyramidal_building_attr* SLASH CLOSE;

pyramidal_building_attr: (ATTR_ROOF_TYPE EQUALS roof_type = REGEX_STRING)
						| (ATTR_SIZE_X EQUALS size_x = REGEX_STRING)
						| (ATTR_SIZE_Z EQUALS size_z = REGEX_STRING)
						| (ATTR_SIZE_TOP_X EQUALS top_x = REGEX_STRING)
						| (ATTR_SIZE_TOP_Z EQUALS top_z = REGEX_STRING)
						| (ATTR_SIZE_BOTTOM_Y EQUALS size_bottom_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_BOTTOM_X EQUALS texture_index_bottom_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_BOTTOM_Z EQUALS texture_index_bottom_z = REGEX_STRING)
						| (ATTR_SIZE_WINDOW_Y EQUALS size_window_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_X EQUALS texture_index_window_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_Y EQUALS texture_index_window_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_Z EQUALS texture_index_window_z = REGEX_STRING)
						| (ATTR_SIZE_TOP_Y EQUALS size_top_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_TOP_X EQUALS texture_index_top_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_TOP_Z EQUALS texture_index_top_z = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_X EQUALS texture_index_roof_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_Z EQUALS texture_index_roof_z = REGEX_STRING);

multi_sided_building: OPEN TAG_MULTI_SIDED_BUILDING multi_sided_building_attr* SLASH CLOSE;
multi_sided_building_attr: (ATTR_BUILDING_SIDES EQUALS building_sides = REGEX_STRING)
						| (ATTR_SMOOTHING EQUALS smoothing = REGEX_STRING)
						| (ATTR_ROOF_TYPE EQUALS roof_type = REGEX_STRING)
						| (ATTR_SIZE_X EQUALS size_x = REGEX_STRING)
						| (ATTR_SIZE_Z EQUALS size_z = REGEX_STRING)
						| (ATTR_SIZE_BOTTOM_Y EQUALS size_bottom_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_BOTTOM_X EQUALS texture_index_bottom_x = REGEX_STRING)
						| (ATTR_SIZE_WINDOW_Y EQUALS size_window_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_X EQUALS texture_index_window_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_WINDOW_Y EQUALS texture_index_window_y = REGEX_STRING)
						| (ATTR_SIZE_TOP_Y EQUALS size_top_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_TOP_X EQUALS texture_index_top_x = REGEX_STRING)
						| (ATTR_SIZE_ROOF_Y EQUALS size_roof_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_X EQUALS texture_index_roof_x = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_Y EQUALS texture_index_roof_y = REGEX_STRING)
						| (ATTR_TEXTURE_INDEX_ROOF_Z EQUALS texture_index_roof_z = REGEX_STRING);


library_object: OPEN TAG_LIBRARY_OBJECT library_object_attr* SLASH CLOSE;

library_object_attr: (ATTR_NAME EQUALS name = REGEX_STRING)
			| (ATTR_SCALE EQUALS scale = REGEX_STRING);

trigger: OPEN TAG_TRIGGER trigger_attr* CLOSE trigger_descendant* OPEN SLASH TAG_TRIGGER CLOSE;

trigger_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
			| (ATTR_TRIGGER_HEIGHT EQUALS trigget_height = REGEX_STRING);

trigger_descendant: fuel | vertex | trigger_weather_data;

trigger_weather_data: OPEN TAG_TRIGGER_WEATHER_DATA trigger_weather_data_attr* SLASH CLOSE;

trigger_weather_data_attr: (ATTR_TYPE EQUALS type = REGEX_STRING)
							| (ATTR_HEADING EQUALS heading = REGEX_STRING)
							| (ATTR_SCALAR EQUALS scalar = REGEX_STRING);


windsock: OPEN TAG_WINDSOCK windsock_attr* CLOSE windsock_descendant* OPEN SLASH TAG_WINDSOCK CLOSE;
windsock_attr: (ATTR_POLE_HEIGHT EQUALS pole_height = REGEX_STRING)
				| (ATTR_SOCK_LENGTH EQUALS sock_length = REGEX_STRING)
				| (ATTR_LIGHTED EQUALS lighted = REGEX_STRING);


windsock_descendant: pole_color | sock_color;
pole_color: OPEN TAG_POLE_COLOR pole_color_attr* SLASH CLOSE;

pole_color_attr: (ATTR_RED EQUALS red = REGEX_STRING)
				| (ATTR_GREEN EQUALS green = REGEX_STRING)
				| (ATTR_BLUE EQUALS blue = REGEX_STRING);

sock_color: OPEN TAG_SOCK_COLOR sock_color_attr* SLASH CLOSE;

sock_color_attr: (ATTR_RED EQUALS red = REGEX_STRING)
				| (ATTR_GREEN EQUALS green = REGEX_STRING)
				| (ATTR_BLUE EQUALS blue = REGEX_STRING);

attached_object: OPEN TAG_ATTACHED_OBJECT attached_object_attr* CLOSE attached_object_descendant* OPEN SLASH TAG_ATTACHED_OBJECT CLOSE;

attached_object_attr: (ATTR_ATTACHPOINT_NAME EQUALS attachpoint_name = REGEX_STRING)
				| (ATTR_INSTANCE_ID EQUALS instance_id = REGEX_STRING)
				| (ATTR_PITCH EQUALS pitch = REGEX_STRING)
				| (ATTR_BANK EQUALS bank = REGEX_STRING)
				| (ATTR_HEADING EQUALS heading = REGEX_STRING);

attached_object_descendant: random_attach | bias_xyz | beacon | effect | library_object;

random_attach: OPEN TAG_RANDOM_ATTACH random_attach_attr* SLASH CLOSE;

random_attach_attr: (ATTR_RANDOMNESS EQUALS randomness = REGEX_STRING)
				| (ATTR_PROBABILITY EQUALS probability = REGEX_STRING);

////////////////////////////////
///// DESCENDANT BLAST FENCE
////////////////////////////////// 

blast_fence: OPEN TAG_BLAST_FENCE blast_fence_attr* CLOSE blast_fence_descendant* OPEN SLASH TAG_BLAST_FENCE CLOSE;

blast_fence_attr: (ATTR_INSTANCE_ID EQUALS REGEX_STRING)
				| (ATTR_PROFILE EQUALS REGEX_STRING);

blast_fence_descendant: vertex vertex vertex*;

////////////////////////////////
///// DESCENDANT BOUNDARY FENCE
//////////////////////////////////

boundary_fence: OPEN TAG_BOUNDARY_FENCE boundary_fence_attr* CLOSE boundary_fence_descendant* OPEN SLASH TAG_BOUNDARY_FENCE CLOSE;

boundary_fence_attr: (ATTR_INSTANCE_ID EQUALS REGEX_STRING)
				| (ATTR_PROFILE EQUALS REGEX_STRING);

boundary_fence_descendant: vertex vertex vertex*;

////////////////////////////////
///// DESCENDANT DELETE AIRPORT
////////////////////////////////// 

delete_airport: OPEN TAG_DELETE_AIRPORT delete_airport_attr* SLASH CLOSE;

delete_airport_attr: (ATTR_DELETE_ALL_APPROACHES EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_APRON_LIGHTS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_APRONS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_FREQUENCIES EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_HELIPADS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_RUNWAYS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_STARTS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_TAXIWAYS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_BLAST_FENCES EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_BOUNDARY_FENCES EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_CONTROL_TOWERS EQUALS REGEX_STRING)
				| (ATTR_DELETE_ALL_JETWAYS EQUALS REGEX_STRING);
