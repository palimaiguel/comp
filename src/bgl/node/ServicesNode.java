package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class ServicesNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public ServicesNode()
    {
        super("Services", mandatory(), optionals());
    }
}

