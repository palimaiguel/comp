package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class ApproachNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("type", AttributeType.approachType),
                        new Attribute("fixType", AttributeType.approachFixType),
                        new Attribute("fixRegion", AttributeType.approachFixRegion),
                        new Attribute("fixIdent", AttributeType.approachFixIdent),
                        new Attribute("altitude", AttributeType.approachAltitude),
                        new Attribute("heading", AttributeType.approachHeading),
                        new Attribute("missedAltitude", AttributeType.approachMissedAltitude)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("runway", AttributeType.approachRunway),
                        new Attribute("designator", AttributeType.approachDesignator),
                        new Attribute("suffix", AttributeType.approachSuffix),
                        new Attribute("gpsOverlay", AttributeType.approachGPSOverlay)
                };
    }

    public ApproachNode()
    {
        super("Approach", mandatory(), optionals());
    }
}

