package bgl.node;

        import bgl.attribute.Attribute;
        import bgl.attribute.AttributeType;

public class HelipadNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("alt", AttributeType.helipadAltitude),
                        new Attribute("surface", AttributeType.helipadSurface),
                        new Attribute("heading", AttributeType.helipadHeading),
                        new Attribute("length", AttributeType.helipadLength),
                        new Attribute("width", AttributeType.helipadWidth),
                        new Attribute("type", AttributeType.helipadType)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("closed", AttributeType.helipadClosed),
                        new Attribute("transparent", AttributeType.helipadTransparent),
                        new Attribute("red", AttributeType.helipadRed),
                        new Attribute("green", AttributeType.helipadGreen),
                        new Attribute("blue", AttributeType.helipadBlue)
                };
    }

    public HelipadNode()
    {
        super("Helipad", mandatory(), optionals());
    }
}

