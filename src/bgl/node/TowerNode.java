package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class TowerNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("alt", AttributeType.towerAltitude)
                };
    }
    public static final Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public TowerNode()
    {
        super("Tower", mandatory(), optionals());
    }
}

