package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class ComNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("frequency", AttributeType.comFrequency),
                        new Attribute("type", AttributeType.comType),
                        new Attribute("name", AttributeType.comName)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public ComNode()
    {
        super("Com", mandatory(), optionals());
    }
}

