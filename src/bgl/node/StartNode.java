package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class StartNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.startLatitude),
                        new Attribute("lon", AttributeType.startLongitude),
                        new Attribute("alt", AttributeType.startAltitude),
                        new Attribute("heading", AttributeType.startHeading)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("type", AttributeType.startType),
                        new Attribute("number", AttributeType.startNumber),
                        new Attribute("designator", AttributeType.startDesignator)
                };
    }

    public StartNode()
    {
        super("Start", mandatory(), optionals());
    }
}

