package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;
import org.antlr.v4.runtime.ParserRuleContext;

import java.util.HashMap;

public class AirportNode extends Node
{
    private HashMap<Integer, ParserRuleContext> m_taxiwayPointIdentifiers = new HashMap<>(); // todos os taxiwaypoint e parking
    private HashMap<Integer, ParserRuleContext> m_taxiwayPointReferencedIdentifiers = new HashMap<>(); // todos os taxiwaypoint em 'start' e 'end'

    private HashMap<Integer, ParserRuleContext> m_taxiwayParkingIdentifiers = new HashMap<>(); // todos os taxiwayparking
    private HashMap<Integer, ParserRuleContext> m_taxiwayParkingReferencedIdentifiers = new HashMap<>(); // todos os taxiwayparking em 'start' e 'end'

    private HashMap<Integer, ParserRuleContext> m_taxiNameIdentifiers = new HashMap<>(); // todos os identificadores em taxiName
    private HashMap<Integer, ParserRuleContext> m_taxiNameReferencedIdentifiers = new HashMap<>(); // todos os referenciados no campo 'name' do taxipath

    public HashMap<Integer, ParserRuleContext> getTaxiwayPointIdentifiers()
    {
        return m_taxiwayPointIdentifiers;
    }

    public HashMap<Integer, ParserRuleContext> getTaxiwayPointReferencedIdentifiers()
    {
        return m_taxiwayPointReferencedIdentifiers;
    }

    public HashMap<Integer, ParserRuleContext> getTaxiwayParkingIdentifiers()
    {
        return m_taxiwayParkingIdentifiers;
    }

    public HashMap<Integer, ParserRuleContext> getTaxiwayParkingReferencedIdentifiers()
    {
        return m_taxiwayParkingReferencedIdentifiers;
    }

    public HashMap<Integer, ParserRuleContext> getTaxiNameIdentifiers()
    {
        return m_taxiNameIdentifiers;
    }

    public HashMap<Integer, ParserRuleContext> getTaxiNameReferencedIdentifiers()
    {
        return m_taxiNameReferencedIdentifiers;
    }

    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("alt", AttributeType.airportAlt),
                        new Attribute("ident", AttributeType.airportIdent),
                        new Attribute("airportTestRadius", AttributeType.airportAirportTestRadius),
                        new Attribute("trafficScalar", AttributeType.airportTrafficScalar)

                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("region", AttributeType.airportRegion),
                        new Attribute("country", AttributeType.airportCountry),
                        new Attribute("state", AttributeType.airportState),
                        new Attribute("city", AttributeType.airportCity),
                        new Attribute("name", AttributeType.airportName),
                        new Attribute("magvar", AttributeType.airportMagvar)
                };
    }

    public AirportNode()
    {
        super("Airport", mandatory(), optionals());
    }
}
