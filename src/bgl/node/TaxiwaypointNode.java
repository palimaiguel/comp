package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class TaxiwaypointNode extends AbstractTaxiwayPositionNode
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("index", AttributeType.taxipointIndex),
                        new Attribute("type", AttributeType.taxipointType)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("orientation", AttributeType.taxipointOrientation),
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("biasX", AttributeType.taxipointBiasX),
                        new Attribute("biasZ", AttributeType.taxipointBiasZ),
                };
    }

    public TaxiwaypointNode()
    {
        super("TaxiwayPoint", mandatory(), optionals());
    }
}

