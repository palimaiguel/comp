package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class LegNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("type", AttributeType.legType),
                        new Attribute("fixType", AttributeType.LegFixType),
                        new Attribute("fixRegion", AttributeType.LegFixRegion),
                        new Attribute("fixIdent", AttributeType.LegFixIdent),
                        new Attribute("flyOver", AttributeType.legFlyOver),
                        new Attribute("turnDirection", AttributeType.legTurnDirection),
                        new Attribute("recommendedType", AttributeType.legRecommendedType),
                        new Attribute("recommendedRegion", AttributeType.legRecommendedRegion),
                        new Attribute("recommendedIdent", AttributeType.legRecommendedIdent),
                        new Attribute("theta", AttributeType.legTheta),
                        new Attribute("rho", AttributeType.legRho),
                        new Attribute("trueCourse", AttributeType.legTrueCourse),
                        new Attribute("altitudeDescriptor", AttributeType.legAltitudeDescriptor),
                        new Attribute("altitude1", AttributeType.legAltitude1),

                        new Attribute("magneticCourse", AttributeType.legMagneticCourse),
                        new Attribute("time", AttributeType.legTime),
                        new Attribute("distance", AttributeType.legDistance),
                        new Attribute("altitude2", AttributeType.legAltitude2)
                };
    }

    public LegNode()
    {
        super("Leg", mandatory(), optionals());
    }
}

