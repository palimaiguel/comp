package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;
import compiler.ErrorPrinter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;

public class TaxiwaypathNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("type", AttributeType.taxiwayPathType),
                        new Attribute("start", AttributeType.taxiwayPathStart),
                        new Attribute("end", AttributeType.taxiwayPathEnd),
                        new Attribute("width", AttributeType.taxiwayPathWidth),
                        new Attribute("weightLimit", AttributeType.taxiwayPathWeightLimit),
                        new Attribute("surface", AttributeType.taxiwayPathSurface)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("centerLine", AttributeType.taxiwayPathCenterLine),
                        new Attribute("centerLineLighted", AttributeType.taxiwayPathCenterLineLighted),
                        new Attribute("leftEdge", AttributeType.taxiwayPathLeftEdge),
                        new Attribute("leftEdgeLighted", AttributeType.taxiwayPathLeftEdgeLighted),
                        new Attribute("rightEdge", AttributeType.taxiwayPathRightEdge),
                        new Attribute("rightEdgeLighted", AttributeType.taxiwayPathRightEdgeLighted),
                        new Attribute("designator", AttributeType.taxiwayPathDesignator),
                        new Attribute("drawDetail", AttributeType.taxiwayPathDrawDetail),
                        new Attribute("number", AttributeType.taxiwayPathNumber),
                        new Attribute("drawSurface", AttributeType.taxiwayPathDrawSurface),
                        new Attribute("name", AttributeType.taxiwayPathName)
                };
    }

    public TaxiwaypathNode()
    {
        super("TaxiwayPath", mandatory(), optionals());
    }

    @Override
    protected void checkMandatoryAttributes(@NotNull ParserRuleContext ctx)
    {
        super.checkMandatoryAttributes(ctx);

        String typeValue = super.mandatoryAttributes.get("type").getValue();

        if(typeValue != null)
        {
            if(typeValue.equals("RUNWAY"))
            {
                if(super.optionalAttributes.get("number").getValue() == null)
                {
                    ErrorPrinter.printTaxiwayPathError(ctx, true, "number", true);
                }

                if(super.optionalAttributes.get("designator").getValue() == null)
                {
                    ErrorPrinter.printTaxiwayPathError(ctx, true, "designator", true);
                }

                if(super.optionalAttributes.get("name").getValue() != null)
                {
                    ErrorPrinter.printTaxiwayPathError(ctx, true, "name", false);
                }
            }
            else
            {
                if(super.optionalAttributes.get("number").getValue() != null)
                {
                    ErrorPrinter.printTaxiwayPathError(ctx, false, "number", false);
                }

                if(super.optionalAttributes.get("designator").getValue() != null)
                {
                    ErrorPrinter.printTaxiwayPathError(ctx, false, "designator", false);
                }

                if(super.optionalAttributes.get("name").getValue() == null)
                {
                    ErrorPrinter.printTaxiwayPathError(ctx, false, "name", true);
                }
            }
        }
    }
}

