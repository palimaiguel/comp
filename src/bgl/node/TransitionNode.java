package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class TransitionNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("transitionType", AttributeType.transitionType),
                        new Attribute("fixType", AttributeType.transitionFixType),
                        new Attribute("fixRegion", AttributeType.transitionFixRegion),
                        new Attribute("fixIdent", AttributeType.transitionFixIdent),
                        new Attribute("altitude", AttributeType.transitionAltitude)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public TransitionNode()
    {
        super("Transition", mandatory(), optionals());
    }
}

