package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class RouteNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("routeType", AttributeType.routeType),
                        new Attribute("name", AttributeType.routeName),
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public RouteNode()
    {
        super("Route", mandatory(), optionals());
    }
}

