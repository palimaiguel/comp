package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class RunwayStartNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.runwayStartLat),
                        new Attribute("lon", AttributeType.runwayStartLon),
                        new Attribute("alt", AttributeType.runwayStartAlt),
                        new Attribute("heading", AttributeType.runwayStartHeading)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("type", AttributeType.runwayStartType),
                        new Attribute("end", AttributeType.runwayStartEnd)
                };
    }

    public RunwayStartNode()
    {
        super("RunwayStart", mandatory(), optionals());
    }
}

