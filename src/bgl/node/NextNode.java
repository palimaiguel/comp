package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class NextNode extends Node
{
    public static final Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("waypointType", AttributeType.nextType),
                        new Attribute("waypointRegion", AttributeType.nextRegion),
                        new Attribute("waypointIdent", AttributeType.nextIdent),
                        new Attribute("altitudeMinimum", AttributeType.nextAltitudeMinimum)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public NextNode()
    {
        super("Next", mandatory(), optionals());
    }
}

