package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class TransitionlegsNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public TransitionlegsNode()
    {
        super("TransitionLegs", mandatory(), optionals());
    }
}

