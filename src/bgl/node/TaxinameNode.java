package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class TaxinameNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("index", AttributeType.taxinameIndex),
                        new Attribute("name", AttributeType.taxinameName)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public TaxinameNode()
    {
        super("TaxiName", mandatory(), optionals());
    }
}

