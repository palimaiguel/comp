package bgl.node;

import bgl.attribute.Attribute;
import compiler.ErrorPrinter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * Created by Jo�o on 09/05/2015.
 */
public abstract class AbstractTaxiwayPositionNode extends Node
{
    public AbstractTaxiwayPositionNode(String name, Attribute[] mandatoryAttributes, Attribute[] optionalAttributes)
    {
        super(name, mandatoryAttributes, optionalAttributes);
    }

    @Override
    public void validateAttributes(@NotNull ParserRuleContext ctx)
    {
        super.validateAttributes(ctx);

        boolean error = false;
        if(super.optionalAttributes.get("biasX").getValue() != null && super.optionalAttributes.get("biasZ").getValue() != null)
        {
            if(super.optionalAttributes.get("lat").getValue() != null || super.optionalAttributes.get("lon").getValue() != null)
            {
                error = true;
            }
        }
        else if(super.optionalAttributes.get("lat").getValue() != null && super.optionalAttributes.get("lon").getValue() != null)
        {
            if(super.optionalAttributes.get("biasX").getValue() != null || super.optionalAttributes.get("biasZ").getValue() != null)
            {
                error = true;
            }
        }
        else
        {
            error = true;
        }

        if(error)
            ErrorPrinter.printInvalidTaxiwayLocation(ctx);
    }
}
