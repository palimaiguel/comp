package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import sdl.graph.TaxiPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaxiwayparkingNode extends AbstractTaxiwayPositionNode
{
    private HashMap<String, TaxiPoint> m_taxiwayConnections = new HashMap<>();

    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("index", AttributeType.taxiparkingIndex),
                        new Attribute("heading", AttributeType.taxiparkingHeading),
                        new Attribute("radius", AttributeType.taxiparkingRadius),
                        new Attribute("type", AttributeType.taxiparkingType),
                        new Attribute("name", AttributeType.taxiparkingName),
                        new Attribute("number", AttributeType.taxiparkingNumber),
                        new Attribute("pushBack", AttributeType.taxiparkingPushBack),
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("biasX", AttributeType.taxiparkingBiasX),
                        new Attribute("biasZ", AttributeType.taxiparkingBiasZ),
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("airlineCodes", AttributeType.taxiparkingAirlineCodes),
                        new Attribute("teeOffset1", AttributeType.taxiparkingteeOffset1),
                        new Attribute("teeOffset2", AttributeType.taxiparkingteeOffset2),
                        new Attribute("teeOffset3", AttributeType.taxiparkingteeOffset3),
                        new Attribute("teeOffset4", AttributeType.taxiparkingteeOffset4)
                };
    }

    public TaxiwayparkingNode()
    {
        super("TaxiwayParking", mandatory(), optionals());
    }

    public void addConnectToTaxiway(String id, TaxiPoint point)
    {
        m_taxiwayConnections.put(id, point);
    }

    public HashMap<String, TaxiPoint> getConnectsToTaxiways()
    {
        return m_taxiwayConnections;
    }


}

