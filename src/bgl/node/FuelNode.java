package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class FuelNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("type", AttributeType.fuelType),
                        new Attribute("availability", AttributeType.fuelAvailability)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public FuelNode()
    {
        super("Fuel", mandatory(), optionals());
    }
}

