package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;
import compiler.ErrorPrinter;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;

public class RunwayNode extends Node
{
    public static final Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("alt", AttributeType.runwayAlt),
                        new Attribute("surface", AttributeType.runwaySurface),
                        new Attribute("heading", AttributeType.runwayHeading),
                        new Attribute("length", AttributeType.runwayLength),
                        new Attribute("width", AttributeType.runwayWidth),
                        new Attribute("number", AttributeType.runwayNumber)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                        new Attribute("designator", AttributeType.runwayDesignator),
                        new Attribute("primaryDesignator", AttributeType.runwayPrimaryDesignator),
                        new Attribute("secondaryDesignator", AttributeType.runwaySecondaryDesignator),
                        new Attribute("patternAltitude", AttributeType.runwayPatternAltitude),
                        new Attribute("primaryTakeoff", AttributeType.runwayPrimaryTakeoff),
                        new Attribute("primaryLanding", AttributeType.runwayPrimaryLanding),
                        new Attribute("primaryPattern", AttributeType.runwayPrimaryPattern),
                        new Attribute("secondaryTakeoff", AttributeType.runwaySecondaryTakeoff),
                        new Attribute("secondaryLanding", AttributeType.runwaySecondaryLanding),
                        new Attribute("secondaryPattern", AttributeType.runwaySecondaryPattern),
                        new Attribute("primaryMarkingBias", AttributeType.runwayPrimaryMarkingBias),
                        new Attribute("secondaryMarkingBias", AttributeType.runwaySecondaryMarkingBias)
                };
    }

    private static String SECONDARY_DESIGNATOR_ERROR = "Attribute <secondaryDesignator> must be opposite to attribute <primaryDesignator>";

    public RunwayNode()
    {
        super("Runway", mandatory(), optionals());
    }

    @Override
    public void validateAttributes(@NotNull ParserRuleContext ctx)
    {
        super.validateAttributes(ctx);

        String primaryDesignatorValue = super.optionalAttributes.get("primaryDesignator").getValue();
        String secondaryDesignatorValue = super.optionalAttributes.get("secondaryDesignator").getValue();

        // Check if secondaryDesignator is opposite to primaryDesignator
        if(primaryDesignatorValue != null && secondaryDesignatorValue != null)
        {
            if(primaryDesignatorValue.equals("L") || primaryDesignatorValue.equals("LEFT"))
            {
                if(!secondaryDesignatorValue.equals("R") && !secondaryDesignatorValue.equals("RIGHT"))
                {
                    ErrorPrinter.printRunwayDesignatorError(ctx, SECONDARY_DESIGNATOR_ERROR);
                }
            }
            else if(primaryDesignatorValue.equals("R") || primaryDesignatorValue.equals("RIGHT"))
            {
                if(!secondaryDesignatorValue.equals("L") && !secondaryDesignatorValue.equals("LEFT"))
                {
                    ErrorPrinter.printRunwayDesignatorError(ctx, SECONDARY_DESIGNATOR_ERROR);
                }
            }
        }
    }

    @Override
    protected void checkMandatoryAttributes(@NotNull ParserRuleContext ctx)
    {
        super.checkMandatoryAttributes(ctx);

        if(super.optionalAttributes.get("designator").getValue() != null)
        {
            if(super.optionalAttributes.get("primaryDesignator").getValue() != null || super.optionalAttributes.get("secondaryDesignator").getValue() != null)
            {
                ErrorPrinter.printRunwayDesignatorError(ctx, "Since attribute <designator> is present then <primaryDesignator> and/or <secondaryDesignator> must not be present");
            }
        }
        else
        {
            String primaryDesignatorValue = super.optionalAttributes.get("primaryDesignator").getValue();
            String secondaryDesignatorValue = super.optionalAttributes.get("secondaryDesignator").getValue();

            if(primaryDesignatorValue != null && secondaryDesignatorValue == null)
            {
                ErrorPrinter.printRunwayDesignatorError(ctx, "Since attribute <primaryDesignator> is present then <secondaryDesignator> must be present");
            }
            else if(primaryDesignatorValue == null && secondaryDesignatorValue != null)
            {
                ErrorPrinter.printRunwayDesignatorError(ctx, "Since attribute <secondaryDesignator> is present then <primaryDesignator> must be present");
            }
        }
    }
}

