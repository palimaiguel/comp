package bgl.node;

import bgl.attribute.Attribute;
import compiler.ErrorPrinter;
import compiler.RegexValidator;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Node
{
    protected String name;
    protected List<Node> children;
    protected HashMap<String, Attribute> mandatoryAttributes;
    protected HashMap<String, Attribute> optionalAttributes;

    public Node(String name, Attribute[] mandatoryAttributes, Attribute[] optionalAttributes)
    {
        this.children = new ArrayList<>();
        this.mandatoryAttributes = new HashMap<>();
        this.optionalAttributes = new HashMap<>();

        this.name = name;
        initializeMandatoryAttributes(mandatoryAttributes);
        initializeOptionalAttributes(optionalAttributes);
    }

    protected void initializeMandatoryAttributes(Attribute[] attributes)
    {
        for (Attribute attribute : attributes)
        {
            this.mandatoryAttributes.put(attribute.getName(), attribute);
        }
    }

    protected void initializeOptionalAttributes(Attribute[] attributes)
    {
        for (Attribute attribute : attributes)
        {
            this.optionalAttributes.put(attribute.getName(), attribute);
        }
    }

    public void addChild(Node node)
    {
        this.children.add(node);
    }

    public void validateAttributes(@NotNull ParserRuleContext ctx)
    {
        checkMandatoryAttributes(ctx);
    }

    protected void checkMandatoryAttributes(@NotNull ParserRuleContext ctx)
    {
        // Check if mandatory attributes values are not null:
        this.mandatoryAttributes.forEach((name, attribute) ->
        {
            if (attribute.getValue() == null)
            {
                ErrorPrinter.printNullMandatoryAttribute(ctx, attribute.getName());
            }
        });
    }

    public void addAttribute(@NotNull ParserRuleContext ctx)
    {
        String key = ctx.getChild(0).getText();
        String value = ctx.getChild(2).getText();

        // Remove quotes:
        value = value.substring(1, value.length() - 1);

        if (this.mandatoryAttributes.containsKey(key))
        {
            Attribute attribute = this.mandatoryAttributes.get(key);
            validateAndSetAttributeValue(ctx, attribute, value);
        }
        else if (this.optionalAttributes.containsKey(key))
        {
            Attribute attribute = this.optionalAttributes.get(key);
            validateAndSetAttributeValue(ctx, attribute, value);
        }
        /*
        else
            ErrorPrinter.printInvalidAttribute(ctx, key); */
    }

    private void validateAndSetAttributeValue(@NotNull ParserRuleContext ctx, Attribute attribute, String value)
    {
        // Give warning if the attribute is already defined with a value:
        if (attribute.getValue() != null)
            ErrorPrinter.printAttributeRepeated(ctx, attribute.getName(), value);

        // Validate value:
        validateAttributeValue(ctx, attribute, value);

        // Set value of attribute:
        attribute.setValue(value);
    }

    private void validateAttributeValue(@NotNull ParserRuleContext ctx, Attribute attribute, String value)
    {
        boolean valid = true;
        switch (attribute.getType())
        {
            case latitude:
                valid = RegexValidator.latitude(value);
                break;
            case longitude:
                valid = RegexValidator.longitude(value);
                break;

            case airportAlt:
                valid = RegexValidator.airportAltitude(value);
                break;
            case airportIdent:
                valid = RegexValidator.airportIdent(value);
                break;
            case airportAirportTestRadius:
                valid = RegexValidator.airportTestRadius(value);
                break;
            case airportTrafficScalar:
                valid = RegexValidator.airportTrafficScalar(value);
                break;
            case airportRegion:
                valid = RegexValidator.airportRegion(value);
                break;
            case airportCountry:
                valid = RegexValidator.airportCountry(value);
                break;
            case airportState:
                valid = RegexValidator.airportState(value);
                break;
            case airportCity:
                valid = RegexValidator.airportCity(value);
                break;
            case airportName:
                valid = RegexValidator.airportName(value);
                break;
            case airportMagvar:
                valid = RegexValidator.airportMagvar(value);
                break;

            case taxipointBiasX:
                valid = RegexValidator.taxiwayPointBiasX(value);
                break;
            case taxipointBiasZ:
                valid = RegexValidator.taxiwayPointBiasZ(value);
                break;
            case taxipointIndex:
                valid = RegexValidator.taxiwayPointIndex(value);
                break;
            case taxipointType:
                valid = RegexValidator.taxiwayPointType(value);
                break;
            case taxipointOrientation:
                valid = RegexValidator.taxiwayPointOrientation(value);
                break;

            case taxiparkingIndex:
                valid = RegexValidator.taxiwayParkingIndex(value);
                break;
            case taxiparkingBiasX:
                valid = RegexValidator.taxiwayParkingBiasX(value);
                break;
            case taxiparkingHeading:
                valid = RegexValidator.taxiwayParkingHeading(value);
                break;
            case taxiparkingBiasZ:
                valid = RegexValidator.taxiwayParkingBiasZ(value);
                break;
            case taxiparkingRadius:
                valid = RegexValidator.taxiwayParkingRadius(value);
                break;
            case taxiparkingType:
                valid = RegexValidator.taxiwayParkingType(value);
                break;
            case taxiparkingName:
                valid = RegexValidator.taxiwayParkingName(value);
                break;
            case taxiparkingNumber:
                valid = RegexValidator.taxiwayParkingNumber(value);
                break;
            case taxiparkingPushBack:
                valid = RegexValidator.taxiwayParkingPushBack(value);
                break;
            case taxiparkingteeOffset1:
                valid = RegexValidator.taxiwayParkingteeOffset1(value);
                break;
            case taxiparkingteeOffset2:
                valid = RegexValidator.taxiwayParkingteeOffset2(value);
                break;
            case taxiparkingteeOffset3:
                valid = RegexValidator.taxiwayParkingteeOffset3(value);
                break;
            case taxiparkingteeOffset4:
                valid = RegexValidator.taxiwayParkingteeOffset4(value);
                break;
            case taxiparkingAirlineCodes:
                valid = RegexValidator.taxiwayParkingAirlineCodes(value);
                break;

            case taxinameIndex:
                valid = RegexValidator.taxinameIndex(value);
                break;
            case taxinameName:
                valid = RegexValidator.taxinameName(value);
                break;

            case taxiwayPathType:
                valid = RegexValidator.taxiwayPathType(value);
                break;
            case taxiwayPathStart:
                valid = RegexValidator.taxiwayPathStart(value);
                break;
            case taxiwayPathEnd:
                valid = RegexValidator.taxiwayPathEnd(value);
                break;
            case taxiwayPathWidth:
                valid = RegexValidator.taxiwayPathWidth(value);
                break;
            case taxiwayPathWeightLimit:
                valid = RegexValidator.taxiwayPathWeightLimit(value);
                break;
            case taxiwayPathSurface:
                valid = RegexValidator.taxiwayPathSurface(value);
                break;
            case taxiwayPathCenterLine:
                valid = RegexValidator.taxiwayPathCenterLine(value);
                break;
            case taxiwayPathCenterLineLighted:
                valid = RegexValidator.taxiwayPathCenterLineLighted(value);
                break;
            case taxiwayPathLeftEdge:
                valid = RegexValidator.taxiwayPathLeftEdge(value);
                break;
            case taxiwayPathLeftEdgeLighted:
                valid = RegexValidator.taxiwayPathLeftEdgeLighted(value);
                break;
            case taxiwayPathRightEdge:
                valid = RegexValidator.taxiwayPathRightEdge(value);
                break;
            case taxiwayPathRightEdgeLighted:
                valid = RegexValidator.taxiwayPathRightEdgeLighted(value);
                break;
            case taxiwayPathDesignator:
                valid = RegexValidator.taxiwayPathDesignator(value);
                break;
            case taxiwayPathDrawDetail:
                valid = RegexValidator.taxiwayPathDrawDetail(value);
                break;
            case taxiwayPathNumber:
                valid = RegexValidator.taxiwayPathNumber(value);
                break;
            case taxiwayPathDrawSurface:
                valid = RegexValidator.taxiwayPathDrawSurface(value);
                break;
            case taxiwayPathName:
                valid = RegexValidator.taxiwayPathName(value);
                break;

            case towerAltitude:
                valid = RegexValidator.towerAltitude(value);
                break;

            case fuelType:
                valid = RegexValidator.fuelType(value);
                break;
            case fuelAvailability:
                valid = RegexValidator.fuelAvailability(value);
                break;

            case comFrequency:
                valid = RegexValidator.comFrequency(value);
                break;
            case comType:
                valid = RegexValidator.comType(value);
                break;
            case comName:
                valid = RegexValidator.comName(value);
                break;

            case runwayAlt:
                valid = RegexValidator.runwayAltitude(value);
                break;
            case runwaySurface:
                valid = RegexValidator.runwaySurface(value);
                break;
            case runwayHeading:
                valid = RegexValidator.runwayHeading(value);
                break;
            case runwayLength:
                valid = RegexValidator.runwayLength(value);
                break;
            case runwayWidth:
                valid = RegexValidator.runwayWidth(value);
                break;
            case runwayNumber:
                valid = RegexValidator.runwayNumber(value);
                break;
            case runwayDesignator:
                valid = RegexValidator.runwayDesignator(value);
                break;
            case runwayPrimaryDesignator:
                valid = RegexValidator.runwayPrimaryDesignator(value);
                break;
            case runwaySecondaryDesignator:
                valid = RegexValidator.runwaySecondaryDesignator(value);
                break;
            case runwayPatternAltitude:
                valid = RegexValidator.runwayPatternAltitude(value);
                break;
            case runwayPrimaryTakeoff:
                valid = RegexValidator.runwayPrimaryTakeoff(value);
                break;
            case runwayPrimaryLanding:
                valid = RegexValidator.runwayPrimaryLanding(value);
                break;
            case runwayPrimaryPattern:
                valid = RegexValidator.runwayPrimaryPattern(value);
                break;
            case runwaySecondaryTakeoff:
                valid = RegexValidator.runwaySecondaryTakeoff(value);
                break;
            case runwaySecondaryLanding:
                valid = RegexValidator.runwaySecondaryLanding(value);
                break;
            case runwaySecondaryPattern:
                valid = RegexValidator.runwaySecondaryPattern(value);
                break;
            case runwayPrimaryMarkingBias:
                valid = RegexValidator.runwayPrimaryMarkingBias(value);
                break;
            case runwaySecondaryMarkingBias:
                valid = RegexValidator.runwaySecondaryMarkingBias(value);
                break;
            case runwayStartLat:
                valid = RegexValidator.runwayStartLatitude(value);
                break;
            case runwayStartLon:
                valid = RegexValidator.runwayStartLongitude(value);
                break;
            case runwayStartAlt:
                valid = RegexValidator.runwayStartAltitude(value);
                break;
            case runwayStartHeading:
                valid = RegexValidator.runwayStartHeading(value);
                break;
            case runwayStartType:
                valid = RegexValidator.runwayStartType(value);
                break;
            case runwayStartEnd:
                valid = RegexValidator.runwayStartEnd(value);
                break;

            case waypointType:
                valid = RegexValidator.waypointType(value);
                break;
            case waypointMagvar:
                valid = RegexValidator.waypointMagvar(value);
                break;
            case waypointRegion:
                valid = RegexValidator.waypointRegion(value);
                break;
            case waypointIdent:
                valid = RegexValidator.waypointIdent(value);
                break;

            case routeType:
                valid = RegexValidator.routeType(value);
                break;
            case routeName:
                valid = RegexValidator.routeName(value);
                break;

            case previousType:
                valid = RegexValidator.previousType(value);
                break;
            case previousRegion:
                valid = RegexValidator.previousRegion(value);
                break;
            case previousIdent:
                valid = RegexValidator.previousIdent(value);
                break;
            case previousAltitudeMinimum:
                valid = RegexValidator.previousAltitudeMinimum(value);
                break;

            case nextType:
                valid = RegexValidator.nextType(value);
                break;
            case nextRegion:
                valid = RegexValidator.nextRegion(value);
                break;
            case nextIdent:
                valid = RegexValidator.nextIdent(value);
                break;
            case nextAltitudeMinimum:
                valid = RegexValidator.nextAltitudeMinimum(value);
                break;

            case approachType:
                valid = RegexValidator.approachType(value);
                break;
            case approachFixType:
                valid = RegexValidator.approachFixType(value);
                break;
            case approachFixRegion:
                valid = RegexValidator.approachFixRegion(value);
                break;
            case approachFixIdent:
                valid = RegexValidator.approachFixIdent(value);
                break;
            case approachAltitude:
                valid = RegexValidator.approachAltitude(value);
                break;
            case approachHeading:
                valid = RegexValidator.approachHeading(value);
                break;
            case approachMissedAltitude:
                valid = RegexValidator.approachMissedAltitude(value);
                break;
            case approachRunway:
                valid = RegexValidator.approachRunway(value);
                break;
            case approachDesignator:
                valid = RegexValidator.approachDesignator(value);
                break;
            case approachSuffix:
                valid = RegexValidator.approachSuffix(value);
                break;
            case approachGPSOverlay:
                valid = RegexValidator.approachGPSOverlay(value);
                break;

            case legType:
                valid = RegexValidator.legType(value);
                break;
            case LegFixType:
                valid = RegexValidator.legFixType(value);
                break;
            case LegFixRegion:
                valid = RegexValidator.legFixRegion(value);
                break;
            case LegFixIdent:
                valid = RegexValidator.legFixIdent(value);
                break;
            case legFlyOver:
                valid = RegexValidator.legFlyOver(value);
                break;
            case legTurnDirection:
                valid = RegexValidator.legTurnDirection(value);
                break;
            case legRecommendedType:
                valid = RegexValidator.legRecommendedType(value);
                break;
            case legRecommendedRegion:
                valid = RegexValidator.legRecommendedRegion(value);
                break;
            case legRecommendedIdent:
                valid = RegexValidator.legRecommendedIdent(value);
                break;
            case legTheta:
                valid = RegexValidator.legTheta(value);
                break;
            case legRho:
                valid = RegexValidator.legRho(value);
                break;
            case legTrueCourse:
                valid = RegexValidator.legTrueCourse(value);
                break;
            case legMagneticCourse:
                valid = RegexValidator.legMagneticCourse(value);
                break;
            case legDistance:
                valid = RegexValidator.legDistance(value);
                break;
            case legTime:
                valid = RegexValidator.legTime(value);
                break;
            case legAltitudeDescriptor:
                valid = RegexValidator.legAltitudeDescriptor(value);
                break;
            case legAltitude1:
                valid = RegexValidator.legAltitude1(value);
                break;
            case legAltitude2:
                valid = RegexValidator.legAltitude2(value);
                break;

            case transitionType:
                valid = RegexValidator.transitionType(value);
                break;
            case transitionFixType:
                valid = RegexValidator.transitionFixType(value);
                break;
            case transitionFixRegion:
                valid = RegexValidator.transitionFixRegion(value);
                break;
            case transitionFixIdent:
                valid = RegexValidator.transitionFixIdent(value);
                break;
            case transitionAltitude:
                valid = RegexValidator.transitionAltitude(value);
                break;

            case helipadAltitude:
                valid = RegexValidator.helipadAltitude(value);
                break;
            case helipadSurface:
                valid = RegexValidator.helipadSurface(value);
                break;
            case helipadHeading:
                valid = RegexValidator.helipadHeading(value);
                break;
            case helipadLength:
                valid = RegexValidator.helipadLength(value);
                break;
            case helipadWidth:
                valid = RegexValidator.helipadWidth(value);
                break;
            case helipadType:
                valid = RegexValidator.helipadType(value);
                break;
            case helipadClosed:
                valid = RegexValidator.helipadClosed(value);
                break;
            case helipadTransparent:
                valid = RegexValidator.helipadTransparent(value);
                break;
            case helipadRed:
                valid = RegexValidator.helipadRed(value);
                break;
            case helipadGreen:
                valid = RegexValidator.helipadGreen(value);
                break;
            case helipadBlue:
                valid = RegexValidator.helipadBlue(value);
                break;

            case startLatitude:
                valid = RegexValidator.startLatitude(value);
                break;
            case startLongitude:
                valid = RegexValidator.startLongitude(value);
                break;
            case startAltitude:
                valid = RegexValidator.startAltitude(value);
                break;
            case startHeading:
                valid = RegexValidator.startHeading(value);
                break;
            case startType:
                valid = RegexValidator.startType(value);
                break;
            case startNumber:
                valid = RegexValidator.startNumber(value);
                break;
            case startDesignator:
                valid = RegexValidator.startDesignator(value);
                break;
        }

        if(!valid)
            ErrorPrinter.printAttributeValueNotValid(ctx, attribute.getName(), value);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Node> getChildren()
    {
        return children;
    }

    public List<Node> getChildren(String name)
    {
        List<Node> output = new ArrayList<>();

        for (Node child : this.children)
        {
            if(child.getName().equals(name))
                output.add(child);
        }

        return output;
    }

    public void setChildren(List<Node> children)
    {
        this.children = children;
    }

    public HashMap<String, Attribute> getMandatoryAttributes()
    {
        return mandatoryAttributes;
    }

    public void setMandatoryAttributes(HashMap<String, Attribute> mandatoryAttributes)
    {
        this.mandatoryAttributes = mandatoryAttributes;
    }

    public HashMap<String, Attribute> getOptionalAttributes()
    {
        return optionalAttributes;
    }

    public void setOptionalAttributes(HashMap<String, Attribute> optionalAttributes)
    {
        this.optionalAttributes = optionalAttributes;
    }

    public String getAttribute(String attributeName){
        Attribute attribute = mandatoryAttributes.get(attributeName);

        if(attribute == null){
            attribute = optionalAttributes.get(attributeName);
        }

        if(attribute == null)
            return "";

        return attribute.getValue();
    }


}
