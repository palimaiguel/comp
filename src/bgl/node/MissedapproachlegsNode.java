package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class MissedapproachlegsNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public MissedapproachlegsNode()
    {
        super("MissedApproachLegs", mandatory(), optionals());
    }
}

