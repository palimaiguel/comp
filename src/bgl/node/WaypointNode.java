package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class WaypointNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("lat", AttributeType.latitude),
                        new Attribute("lon", AttributeType.longitude),
                        new Attribute("waypointType", AttributeType.waypointType),
                        new Attribute("magvar", AttributeType.waypointMagvar),
                        new Attribute("waypointRegion", AttributeType.waypointRegion),
                        new Attribute("waypointIdent", AttributeType.waypointIdent)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public WaypointNode()
    {
        super("Waypoint", mandatory(), optionals());
    }
}

