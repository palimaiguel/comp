package bgl.node;

import bgl.attribute.Attribute;
import bgl.attribute.AttributeType;

public class PreviousNode extends Node
{
    public static Attribute[] mandatory()
    {
        return new Attribute[]
                {
                        new Attribute("waypointType", AttributeType.previousType),
                        new Attribute("waypointRegion", AttributeType.previousRegion),
                        new Attribute("waypointIdent", AttributeType.previousIdent),
                        new Attribute("altitudeMinimum", AttributeType.previousAltitudeMinimum)
                };
    }
    public static Attribute[] optionals()
    {
        return new Attribute[]
                {
                };
    }

    public PreviousNode()
    {
        super("Previous", mandatory(), optionals());
    }
}

