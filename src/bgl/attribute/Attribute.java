package bgl.attribute;

public class Attribute
{
    private String name;
    private String value;
    private AttributeType type;

    public Attribute(String name, AttributeType type)
    {
        this.name = name;
        this.type = type;
        this.value = null;
    }

    public String getName()
    {
        return name;
    }

    public AttributeType getType()
    {
        return type;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
